
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  >
  <xsl:template match="/">
  
  <xsl:for-each select="ReturnDataSet">
  <xsl:for-each select="Table">
  <table border="0" width="60%" class="HTable"><tr><td class="summarytitle">Asset Details</td></tr>
  <tr><td>
  <table border="0" align="center" width="100%">
  <tr class="normal">
	<td >Name</td><td><xsl:value-of select="Description"/></td>
  </tr>
    <tr class="normal">
  <td>Category</td><td><xsl:value-of select="Category"/></td>
  </tr>
    <tr class="normal">
	<td >Reference</td><td><xsl:value-of select="EXTREF"/></td>
  </tr>
  <tr class="normal">
  <td>Manufacturer</td><td><xsl:value-of select="Manufacturer"/></td>
  </tr>
  <tr class="normal">
  <td>Model</td><td><xsl:value-of select="ModelNo"/></td>
  </tr>
  <tr class="normal">
  <td>Value</td>
  <td><xsl:if test="AssetValue != ''"> 
				 <xsl:value-of select="format-number(AssetValue, '###,###,##0.00')"/>
  </xsl:if>	
  </td>
  </tr>
  <tr class="normal">
  <td>Warrenty Expires</td><td>
 <xsl:if test="Warrenty">
  <xsl:call-template name="format-date">
                         <xsl:with-param name="date" select="WarrentyExpires" />
                         <xsl:with-param name="format" select="5" />
                    </xsl:call-template>
        </xsl:if>	      
 </td>
  </tr>
  <tr class="normal">
  <td>Supplier</td><td><xsl:value-of select="SupplierName"/></td>
  </tr>
   </table>
  </td></tr></table>
</xsl:for-each>
  </xsl:for-each>
  </xsl:template>
  
  <xsl:template name="format-date">
  <xsl:param name="date" />
  <xsl:param name="format" select="0" />
<xsl:variable name="justDate" select="substring-before($date, 'T')" />
  <xsl:variable name="day" select="substring-before(substring-after($justDate, '-'), '-')" />
  <xsl:variable name="monthName" select="substring-before(substring-after(substring-after($justDate, '-'), '-'), '-')" />
  <xsl:variable name="year" select="substring-before(substring-after(substring-after(substring-after($justDate, '-'), '-'), '-'), '-')" />

  <xsl:variable name="month" select="substring(substring-after('Jan01Feb02Mar03Apr04May05Jun06Jul07Aug08Sep09Oct10Nov11Dec12', $monthName), 1, 2)" />
  <xsl:variable name="day2" select="concat(translate(substring($day,1,1), '0', ''), substring($day,2,1))" />
  <xsl:variable name="month2" select="concat(translate(substring($month,1,1), '0', ''), substring($month,2,1))" />

  <xsl:choose>
    <xsl:when test="$format = 1">
     <xsl:value-of select="concat($month2, '/', $day2, '/', substring($year, 3))" />
    </xsl:when>
    <xsl:when test="$format = 2">
     <xsl:value-of select="concat($month, '.', $day, '.', substring($year, 3))" />
    </xsl:when>
    <xsl:when test="$format = 3">
     <xsl:value-of select="concat($monthName, ' ', $day2, ' ', $year)" />
    </xsl:when>
<xsl:when test="$format = 4">
     <xsl:value-of select="concat($day2, '/', $month2, '/', substring($year, 3))" />
    </xsl:when>

    <xsl:otherwise>
     <xsl:value-of select="$justDate" />
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

  
 </xsl:stylesheet>
