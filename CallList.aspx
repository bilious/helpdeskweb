<%@ Page Language="vb" AutoEventWireup="false" Codebehind="CallList.aspx.vb" Inherits="HelpdeskWeb.CallList"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CallList</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="hexagon.css">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<IMG src="Hexagon.gif">
			<TABLE id="Table1" align="center" cellSpacing="1" cellPadding="1" width="800" height="600"
				border="0">
				<TR>
					<TD style="HEIGHT: 524px"></TD>
					<TD align="center" valign="top" style="BACKGROUND-POSITION:center 50%; BACKGROUND-IMAGE:url(HexagonTable.jpg); BACKGROUND-REPEAT:no-repeat; HEIGHT:524px">
						<asp:Xml id="xmlProducts" runat="server" DocumentSource="data.xml" TransformSource="datatable.xsl"></asp:Xml>
						<asp:Label id="lblWarning" runat="server">Label</asp:Label></TD>
					<TD style="HEIGHT: 524px"></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD></TD>
					<TD></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
