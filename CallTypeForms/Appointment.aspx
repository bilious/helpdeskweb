<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Appointment.aspx.vb" Inherits="HelpdeskWeb.Appointment"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Appointment</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body MS_POSITIONING="GridLayout" bgColor="activeborder">
		<form id="Form1" method="post" runat="server">
			<asp:Label id="Label1" style="Z-INDEX: 101; LEFT: 40px; POSITION: absolute; TOP: 56px" runat="server"
				Font-Names="Verdana" Font-Size="0.7em">Requestor's Name</asp:Label>
			<asp:button id="Button1" style="Z-INDEX: 111; LEFT: 224px; POSITION: absolute; TOP: 216px" runat="server"
				Text="Submit Data"></asp:button>
			<asp:Label id="lblError" style="Z-INDEX: 110; LEFT: 40px; POSITION: absolute; TOP: 8px" runat="server"
				Font-Size="0.7em" Font-Names="Verdana" Width="304px" ForeColor="Red" Height="32px"></asp:Label>
			<asp:Label id="Label4" style="Z-INDEX: 109; LEFT: 40px; POSITION: absolute; TOP: 176px" runat="server"
				Font-Names="Verdana" Font-Size="0.7em">Person Informed</asp:Label>
			<asp:DropDownList id="DropDownList2" style="Z-INDEX: 108; LEFT: 184px; POSITION: absolute; TOP: 176px"
				runat="server" Width="152px">
				<asp:ListItem Value="Gordon Foster">Gordon Foster</asp:ListItem>
				<asp:ListItem Value="NickyLewis">NickyLewis</asp:ListItem>
				<asp:ListItem Value="Carla O'Brien">Carla O'Brien</asp:ListItem>
			</asp:DropDownList>
			<asp:Label id="Label3" style="Z-INDEX: 106; LEFT: 40px; POSITION: absolute; TOP: 144px" runat="server"
				Font-Names="Verdana" Font-Size="0.7em">Type Of Accomodation</asp:Label>
			<asp:TextBox id="TextBox3" style="Z-INDEX: 105; LEFT: 184px; POSITION: absolute; TOP: 112px"
				runat="server"></asp:TextBox>
			<asp:TextBox id="TextBox2" style="Z-INDEX: 104; LEFT: 184px; POSITION: absolute; TOP: 80px" runat="server"></asp:TextBox>
			<asp:Label id="Label2" style="Z-INDEX: 103; LEFT: 40px; POSITION: absolute; TOP: 88px" runat="server"
				Font-Names="Verdana" Font-Size="0.7em">Contact Details</asp:Label>
			<asp:TextBox id="TextBox1" style="Z-INDEX: 102; LEFT: 184px; POSITION: absolute; TOP: 48px" runat="server"></asp:TextBox>
			<asp:DropDownList id="DropDownList1" style="Z-INDEX: 107; LEFT: 184px; POSITION: absolute; TOP: 144px"
				runat="server" Width="152px">
				<asp:ListItem Value="Commercial">Commercial</asp:ListItem>
				<asp:ListItem Value="Residential">Residential</asp:ListItem>
				<asp:ListItem Value="Student">Student</asp:ListItem>
			</asp:DropDownList><INPUT style="Z-INDEX: 112; LEFT: 208px; POSITION: absolute; TOP: 264px" type="submit"
				value="Submit">
		</form>
	</body>
</HTML>
