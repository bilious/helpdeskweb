Imports System.Data
Imports System.Data.SqlClient
Public Class Appointment
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents TextBox1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    Protected WithEvents TextBox2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents TextBox3 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Label3 As System.Web.UI.WebControls.Label
    Protected WithEvents DropDownList1 As System.Web.UI.WebControls.DropDownList
    Protected WithEvents DropDownList2 As System.Web.UI.WebControls.DropDownList
    Protected WithEvents Label4 As System.Web.UI.WebControls.Label
    Protected WithEvents lblError As System.Web.UI.WebControls.Label
    Protected WithEvents Button1 As System.Web.UI.WebControls.Button

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region


    Private IncidentID As Int32 = -1
    Private oDataAccessor As New DataAccessor

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        IncidentID = Request.QueryString("CallID")
        If Not Page.IsPostBack Then

            If IncidentID > 0 Then
                Dim oDataSet As DataSet = oDataAccessor.SelectQuery("Select Field1, Field2, Field3,  Field4,  Field5, Field6,  Field7 From tbl_Web_Fields Where CallID = " & IncidentID)
                If oDataSet.Tables.Count = 1 Then
                    If oDataSet.Tables(0).Rows.Count > 0 Then
                        TextBox1.Text = oDataSet.Tables(0).Rows(0).Item(0)
                        TextBox1.Text = oDataSet.Tables(0).Rows(0).Item(1)
                        TextBox1.Text = oDataSet.Tables(0).Rows(0).Item(2)
                        If oDataSet.Tables(0).Rows(0).Item(3) <> "" Then
                            DropDownList1.SelectedValue = oDataSet.Tables(0).Rows(0).Item(3)
                        End If
                        If oDataSet.Tables(0).Rows(0).Item(4) <> "" Then
                            DropDownList2.SelectedValue = oDataSet.Tables(0).Rows(0).Item(4)
                        End If
                    End If

                End If
            End If

        End If

    End Sub
    Function GetCheckBoxValue(ByVal CheckBox As WebControls.CheckBox) As String
        If CheckBox.Checked Then
            Return "Y"
        Else
            Return "N"
        End If
    End Function
   

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim WebLineID As Int32 = -1
        Dim strSQL = "Insert Into tbl_Web_Fields (CallID, Field1, Field2, Field3,  Field4,  Field5) Values (" & _
                IncidentID & ",'" & TextBox1.Text & "', '" & TextBox2.Text & "', '" & TextBox3.Text & "', '" & DropDownList1.SelectedValue & "','" & DropDownList1.SelectedValue & "')"
        Try
            WebLineID = oDataAccessor.ScalarQuery(strSQL & ";select @@identity")
        Catch ex As Exception
            lblError.Text = "Error ! " & ex.Message
        Finally
            If WebLineID > 0 Then
                lblError.Text = "Data Saved !"
            End If
        End Try
    End Sub
End Class
