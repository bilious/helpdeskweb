<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Maintenance.aspx.vb" Inherits="HelpdeskWeb.Maintenance"%>
<%@ Register TagPrefix="igsch" Namespace="Infragistics.WebUI.WebSchedule" Assembly="Infragistics.WebUI.WebDateChooser.v5.3, Version=5.3.20053.50, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Maintenance</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body bgColor="gainsboro" MS_POSITIONING="GridLayout">
		<form id="Form1" action="Maintenance.aspx" method="post" runat="server">
			<asp:label id="Label5" style="Z-INDEX: 113; LEFT: 24px; POSITION: absolute; TOP: 160px" runat="server"
				Font-Names="Verdana" Font-Size="0.7em">Number For Contact</asp:label><asp:textbox id="TextBox1" style="Z-INDEX: 117; LEFT: 328px; POSITION: absolute; TOP: 24px" runat="server"></asp:textbox><asp:label id="Label6" style="Z-INDEX: 114; LEFT: 24px; POSITION: absolute; TOP: 192px" runat="server"
				Font-Names="Verdana" Font-Size="0.7em">Scheduled Access Date</asp:label><asp:radiobutton id="RadioButton6" style="Z-INDEX: 112; LEFT: 392px; POSITION: absolute; TOP: 128px"
				runat="server" Font-Names="Verdana" Font-Size="0.7em" Text="No" GroupName="G3"></asp:radiobutton><asp:radiobutton id="RadioButton4" style="Z-INDEX: 111; LEFT: 392px; POSITION: absolute; TOP: 96px"
				runat="server" Font-Names="Verdana" Font-Size="0.7em" Text="No" GroupName="G2"></asp:radiobutton><asp:radiobutton id="RadioButton5" style="Z-INDEX: 110; LEFT: 328px; POSITION: absolute; TOP: 128px"
				runat="server" Font-Names="Verdana" Font-Size="0.7em" Text="Yes" GroupName="G3"></asp:radiobutton><asp:label id="Label4" style="Z-INDEX: 109; LEFT: 24px; POSITION: absolute; TOP: 128px" runat="server"
				Font-Names="Verdana" Font-Size="0.7em">Can We Gain Access Without Anyone Being There ?</asp:label><asp:radiobutton id="RadioButton3" style="Z-INDEX: 108; LEFT: 328px; POSITION: absolute; TOP: 96px"
				runat="server" Font-Names="Verdana" Font-Size="0.7em" Text="Yes" GroupName="G2"></asp:radiobutton><asp:textbox id="TextBox2" style="Z-INDEX: 101; LEFT: 328px; POSITION: absolute; TOP: 160px"
				runat="server"></asp:textbox><asp:textbox id="TextBox3" style="Z-INDEX: 102; LEFT: 328px; POSITION: absolute; TOP: 184px"
				runat="server"></asp:textbox><asp:label id="Label1" style="Z-INDEX: 103; LEFT: 24px; POSITION: absolute; TOP: 96px" runat="server"
				Font-Names="Verdana" Font-Size="0.7em">Has The Fault Been Reported Before ?</asp:label><asp:label id="Label2" style="Z-INDEX: 104; LEFT: 24px; POSITION: absolute; TOP: 64px" runat="server"
				Font-Names="Verdana" Font-Size="0.7em">Has The Fault Occurred Before ?</asp:label><asp:label id="Label3" style="Z-INDEX: 105; LEFT: 24px; POSITION: absolute; TOP: 32px" runat="server"
				Font-Names="Verdana" Font-Size="0.7em">How Long Has The Fault Been Known About ?</asp:label><asp:radiobutton id="RadioButton1" style="Z-INDEX: 106; LEFT: 328px; POSITION: absolute; TOP: 64px"
				runat="server" Font-Names="Verdana" Font-Size="0.7em" Text="Yes" GroupName="G1"></asp:radiobutton><asp:radiobutton id="RadioButton2" style="Z-INDEX: 107; LEFT: 392px; POSITION: absolute; TOP: 64px"
				runat="server" Font-Names="Verdana" Font-Size="0.7em" Text="No" GroupName="G1"></asp:radiobutton><asp:checkbox id="CheckBox1" style="Z-INDEX: 115; LEFT: 328px; POSITION: absolute; TOP: 224px"
				runat="server" Font-Names="Verdana" Font-Size="0.7em" Text="Tenants Informed"></asp:checkbox><asp:label id="lblError" style="Z-INDEX: 118; LEFT: 504px; POSITION: absolute; TOP: 32px" runat="server"
				Font-Names="Verdana" Font-Size="0.7em" Height="200px" ForeColor="Red" Width="184px" Visible="False"></asp:label><asp:textbox id="WebID" style="Z-INDEX: 120; LEFT: 24px; POSITION: absolute; TOP: 368px" runat="server"
				Width="8px" BackColor="Gainsboro" Height="8px"></asp:textbox></form>
	</body>
</HTML>
