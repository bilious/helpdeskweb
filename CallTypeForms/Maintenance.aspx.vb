Imports System.Data
Imports System.Data.SqlClient
Public Class Maintenance
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents TextBox2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents TextBox3 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    Protected WithEvents Label3 As System.Web.UI.WebControls.Label
    Protected WithEvents RadioButton1 As System.Web.UI.WebControls.RadioButton
    Protected WithEvents RadioButton2 As System.Web.UI.WebControls.RadioButton
    Protected WithEvents RadioButton3 As System.Web.UI.WebControls.RadioButton
    Protected WithEvents Label4 As System.Web.UI.WebControls.Label
    Protected WithEvents RadioButton5 As System.Web.UI.WebControls.RadioButton
    Protected WithEvents RadioButton4 As System.Web.UI.WebControls.RadioButton
    Protected WithEvents RadioButton6 As System.Web.UI.WebControls.RadioButton
    Protected WithEvents Label5 As System.Web.UI.WebControls.Label
    Protected WithEvents Label6 As System.Web.UI.WebControls.Label
    Protected WithEvents CheckBox1 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents TextBox1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblError As System.Web.UI.WebControls.Label
    Protected WithEvents WebID As System.Web.UI.WebControls.TextBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private IncidentID As Int32 = -1
    Private oDataAccessor As New DataAccessor
    Dim intWebID As Int32 = 0

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Request.QueryString("CallID") Is Nothing Then
            IncidentID = -1
        Else
            IncidentID = Convert.ToInt32(Request.QueryString("CallID"))
        End If
        If Request.QueryString("WebID") Is Nothing Then
            intWebID = 0
        Else
            intWebID = Convert.ToInt32(Request.QueryString("WebID"))
        End If

        lblError.Text = Now.ToString
        If Not Page.IsPostBack Then

            If IncidentID > 0 Then
                intWebID = oDataAccessor.ScalarQuery("Select WebID From tbl_Incidence Where IncidentID = " & IncidentID)
                Dim oDataSet As DataSet = oDataAccessor.SelectQuery("Select Field1, Field2, Field3,  Field4,  Field5, Field6,  Field7 From tbl_Web_Fields Where WebID = " & intWebID)
                If oDataSet.Tables.Count = 1 Then
                    If oDataSet.Tables(0).Rows.Count > 0 Then
                        TextBox1.Text = oDataSet.Tables(0).Rows(0).Item(0)
                        If oDataSet.Tables(0).Rows(0).Item(1) = "Y" Then RadioButton1.Checked = True
                        If oDataSet.Tables(0).Rows(0).Item(1) = "N" Then RadioButton2.Checked = True
                        If oDataSet.Tables(0).Rows(0).Item(2) = "Y" Then RadioButton3.Checked = True
                        If oDataSet.Tables(0).Rows(0).Item(2) = "N" Then RadioButton3.Checked = True
                        If oDataSet.Tables(0).Rows(0).Item(3) = "Y" Then RadioButton1.Checked = True
                        If oDataSet.Tables(0).Rows(0).Item(3) = "N" Then RadioButton2.Checked = True
                        TextBox2.Text = oDataSet.Tables(0).Rows(0).Item(4)
                        TextBox3.Text = oDataSet.Tables(0).Rows(0).Item(5)
                        If oDataSet.Tables(0).Rows(0).Item(6) = "Y" Then CheckBox1.Checked = True
                    End If

                End If
            Else
                'Initial First Time Load Do Nothing
            End If
        Else
            'Should Be A Submit
            Dim w As String = Request.Params("WebID")

            Dim strSQL As String

            Select Case IncidentID
                Case -1
                    'We Have A Problem No Info Linking Us To Call
                Case 0
                    'New Incident Do An Insert
                    intWebID = Convert.ToInt32(w)
                    strSQL = "Insert Into tbl_Web_Fields (WebID, Field1, Field2, Field3,  Field4,  Field5, Field6,  Field7) Values (" & _
                        intWebID & ",'" & TextBox1.Text & "', '" & GetRadioValue("G1") & "', '" & GetRadioValue("G2") & "', '" & GetRadioValue("G3") & "','" & TextBox2.Text & "','" & TextBox3.Text & "','" & GetCheckBoxValue(CheckBox1) & "')"
                Case Else
                    'Normal Update
                    intWebID = oDataAccessor.ScalarQuery("Select WebID From tbl_Incidence Where IncidentID = " & IncidentID)
                    strSQL = "Update tbl_Web_Fields Set Field1= '" & TextBox1.Text & "', Field2=' " & GetRadioValue("G1") & "', Field3 = '" & GetRadioValue("G2") & "', Field4 = '" & GetRadioValue("G3") & "', Field5='" & TextBox2.Text & "', Field6='" & TextBox3.Text & "', Field7='" & GetCheckBoxValue(CheckBox1) & "' Where WebID = " & intWebID
            End Select
            Try
                oDataAccessor.NonQuery(strSQL)

            Catch ex As Exception
                lblError.Text = "Error ! " & ex.Message
                Exit Sub
            Finally
                lblError.Text = "Saved"
            End Try
        End If

    End Sub
    Function GetCheckBoxValue(ByVal CheckBox As WebControls.CheckBox) As String
        If CheckBox.Checked Then
            Return "Y"
        Else
            Return "N"
        End If
    End Function
    Function GetRadioValue(ByVal GroupName) As String
        Select Case GroupName
            Case "G1"
                If RadioButton1.Checked Then
                    Return "Y"
                ElseIf RadioButton2.Checked Then
                    Return "N"
                Else
                    Return ""
                End If
            Case "G2"
                If RadioButton3.Checked Then
                    Return "Y"
                ElseIf RadioButton4.Checked Then
                    Return "N"
                Else
                    Return ""
                End If
            Case "G3"
                If RadioButton5.Checked Then
                    Return "Y"
                ElseIf RadioButton6.Checked Then
                    Return "N"
                Else
                    Return ""
                End If
        End Select
    End Function

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim WebLineID As Int32 = -1
        Dim strSQL = "Insert Into tbl_Web_Fields (CallID, Field1, Field2, Field3,  Field4,  Field5, Field6,  Field7) Values (" & _
                IncidentID & ",'" & TextBox1.Text & "', '" & GetRadioValue("G1") & "', '" & GetRadioValue("G2") & "', '" & GetRadioValue("G3") & "','" & TextBox2.Text & "','" & TextBox3.Text & "','" & GetCheckBoxValue(CheckBox1) & "')"
        Try
            WebLineID = oDataAccessor.ScalarQuery(strSQL & ";select @@identity")
        Catch ex As Exception
            lblError.Text = "Error ! " & ex.Message
        Finally
            If WebLineID > 0 Then
                lblError.Text = "Data Saved !"
            End If
        End Try
    End Sub
End Class
