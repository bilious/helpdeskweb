<%@ Page Language="vb" AutoEventWireup="false" Codebehind="RespMaintenance.aspx.vb" Inherits="HelpdeskWeb.RespMaintenance"%>
<%@ Register TagPrefix="igsch" Namespace="Infragistics.WebUI.WebSchedule" Assembly="Infragistics.WebUI.WebDateChooser.v5.3, Version=5.3.20053.50, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>RespMaintenance</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body bgColor="#dcdcdc" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<asp:textbox id="TextBox1" style="Z-INDEX: 110; LEFT: 328px; POSITION: absolute; TOP: 24px" runat="server"></asp:textbox><asp:textbox id="WebID" style="Z-INDEX: 112; LEFT: 8px; POSITION: absolute; TOP: 440px" runat="server"
				Width="8px" Height="8px" BackColor="Gainsboro"></asp:textbox><asp:label id="lblError" style="Z-INDEX: 111; LEFT: 32px; POSITION: absolute; TOP: 312px" runat="server"
				Font-Names="Verdana" Font-Size="0.7em" Width="440px" Height="40px" Visible="False" ForeColor="Red"></asp:label><asp:label id="Label6" style="Z-INDEX: 109; LEFT: 24px; POSITION: absolute; TOP: 160px" runat="server"
				Font-Names="Verdana" Font-Size="0.7em">Other Access Information</asp:label><asp:radiobutton id="RadioButton2" style="Z-INDEX: 108; LEFT: 392px; POSITION: absolute; TOP: 128px"
				runat="server" Font-Names="Verdana" Font-Size="0.7em" Text="No" GroupName="G1"></asp:radiobutton><asp:radiobutton id="RadioButton1" style="Z-INDEX: 107; LEFT: 328px; POSITION: absolute; TOP: 128px"
				runat="server" Font-Names="Verdana" Font-Size="0.7em" Text="Yes" GroupName="G1"></asp:radiobutton><asp:label id="Label4" style="Z-INDEX: 106; LEFT: 24px; POSITION: absolute; TOP: 128px" runat="server"
				Font-Names="Verdana" Font-Size="0.7em">Can We Gain Access Without Anyone Being There ?</asp:label><asp:textbox id="TextBox2" style="Z-INDEX: 101; LEFT: 328px; POSITION: absolute; TOP: 56px" runat="server"></asp:textbox><asp:textbox id="TextBox3" style="Z-INDEX: 102; LEFT: 24px; POSITION: absolute; TOP: 176px" runat="server"
				Width="457px" TextMode="MultiLine" Height="120px"></asp:textbox><asp:label id="Label1" style="Z-INDEX: 103; LEFT: 24px; POSITION: absolute; TOP: 96px" runat="server"
				Font-Names="Verdana" Font-Size="0.7em">When Did You First Report It ?</asp:label><asp:label id="Label2" style="Z-INDEX: 104; LEFT: 24px; POSITION: absolute; TOP: 64px" runat="server"
				Font-Names="Verdana" Font-Size="0.7em">How Many Times Has The Fault Occurred Before ?</asp:label><asp:label id="Label3" style="Z-INDEX: 105; LEFT: 24px; POSITION: absolute; TOP: 32px" runat="server"
				Font-Names="Verdana" Font-Size="0.7em">How Long Has The Fault Been Known About ?</asp:label><asp:calendar id="Calendar1" style="Z-INDEX: 113; LEFT: 312px; POSITION: absolute; TOP: 112px"
				runat="server" Font-Names="Verdana" Font-Size="0.7em" Width="161px" Height="160px" Visible="False" BackColor="#E0E0E0"></asp:calendar><asp:textbox id="TextBox4" style="Z-INDEX: 114; LEFT: 328px; POSITION: absolute; TOP: 88px" runat="server"></asp:textbox><asp:button id="Button1" style="Z-INDEX: 115; LEFT: 496px; POSITION: absolute; TOP: 96px" runat="server"
				Width="16px" Height="16px"></asp:button></form>
	</body>
</HTML>
