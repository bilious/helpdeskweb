
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  >
 <xsl:key name="group-by-type" match="SummaryDetails" use="GroupName" />
 <xsl:template match="NewDataSet">
 <p>&#160;</p>
	<xsl:for-each select="SummaryDetails[count(. | key('group-by-groupname', GroupName)[1]) = 1]">
	<script>
	function doLog(){
	iD = <xsl:value-of select="ContactID" />;
	 window.location.href = 'LogCall@integra&amp;ContactID=' + iD;
	}
	</script>
	  <table width="100%" border="0" align="center" 
      cellpadding="1" cellspacing="0" class="normal">
  <tr>
	<td><strong>Contact</strong></td>
	<td><strong><xsl:value-of select="Title" />&#160; <xsl:value-of select="FirstName" />&#160;<xsl:value-of select="LastName" /></strong></td>
  </tr>
    <tr>
	<td>Company</td>
	<td><xsl:value-of select="Company" /></td>
  </tr>
            <tr>
	<td>Position</td>
	<td><xsl:value-of select="Position" /></td>
  </tr>  
      <tr>
	<td>Telephone</td>
	<td><xsl:value-of select="Telephone" /></td>
  </tr>
        <tr>
	<td>Fax</td>
	<td><xsl:value-of select="Fax" /></td>
  </tr>
        <tr>
	<td>Email</td>
	<td><xsl:value-of select="Email" /></td>
  </tr>
        <tr>
	<td>Web</td>
	<td><xsl:value-of select="Website" /></td>
  </tr>
     
   </table>
 <p>&#160;</p>
 </xsl:for-each> 
 </xsl:template>
 </xsl:stylesheet>
