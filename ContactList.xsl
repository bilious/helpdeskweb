
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  >
 <xsl:key name="group-by-type" match="SummaryDetails" use="GroupName" />
 <xsl:template match="NewDataSet">

    <table width="100%" border="0" align="center" 
      cellpadding="1" cellspacing="0" class="normal">
      <tr><td colspan="8" align="center"></td></tr>
      <tr class="normal">
      <td><strong>ID</strong></td>
      <td width="7%" ><strong>Title</strong></td>
      <td width="15%" ><strong>FirstName</strong></td>
      <td width="8%" ><strong>LastName</strong></td>
      <td width="13%" ><strong>Position</strong></td>
      <td width="13%" ><strong>Telephone</strong></td>
      <td width="10%" ><strong>Fax</strong></td>
      <td width="27%"><strong>EMail</strong></td>
      </tr>
	<xsl:for-each select="SummaryDetails[count(. | key('group-by-groupname', GroupName)[1]) = 1]">
  
     <xsl:variable name="IDValue">
		<xsl:value-of select="ContactID"/>
	</xsl:variable> 
   
      <tr>
      <td><a href="ContactDetails.aspx?ContactID={ContactID}"><xsl:value-of select="ContactID"/></a></td>
        <td><xsl:value-of select="Title"/></td>
        <td> <xsl:value-of select="FirstName" /> </td>
        <td>   <xsl:value-of select="LastName" /> </td>
        <td>   <xsl:value-of select="Position" /></td>
        <td>
          <xsl:value-of select="Telephone" /> 
        </td>

        <td>
          <xsl:value-of select="Fax" /> 
        </td>


        <td>
                <xsl:value-of select="Email" /> 
        </td>
      </tr> 
      </xsl:for-each>
      
   </table><p></p>
   <table width="95%" class="normal"  align="center">
   <tr><td><a href="javascript:history.go(-1)">Back</a></td></tr>
   </table>
   
</xsl:template>
 </xsl:stylesheet>

