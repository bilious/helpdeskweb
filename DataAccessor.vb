Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Diagnostics
Imports Common

Public Class DataAccessor

    Private strConnectionString As String = ""
    Public Shared ConnectionString As String = ""

    Public Sub New()
        strConnectionString = GlobalVariables.DBConnStr
    End Sub


    Public Function CheckConnection() As Boolean

        Try
            Dim oConnection As SqlConnection = Nothing
            TakeConnection(oConnection)
            ReleaseConnection(oConnection)
            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function

#Region "Connection"

    Public Sub TakeConnection(ByRef connection As SqlConnection)
        connection = New SqlConnection(strConnectionString)
        connection.Open()
    End Sub
    Public Sub ReleaseConnection(ByRef connection As SqlConnection)
        If Not connection Is Nothing Then
            connection.Close()
        End If
    End Sub

#End Region

#Region "NonQuery"
    Public Sub NonQuery(ByVal query As String)

        Dim oConnection As SqlConnection = Nothing

        Debug.WriteLine(query)
        Try
            TakeConnection(oConnection)

            Dim oSqlCommand As SqlCommand = oConnection.CreateCommand()
            oSqlCommand.CommandType = CommandType.Text
            oSqlCommand.CommandText = query
            oSqlCommand.ExecuteNonQuery()
            'Catch ex As SqlException
            '    MsgBox(ex.Message)
        Finally
            ReleaseConnection(oConnection)
        End Try

    End Sub
#End Region

#Region "SelectQuery"
    Public Function SelectQuery(ByVal query As String) As DataSet


        Dim oConnection As SqlConnection = Nothing
        Dim oReturnDataSet As New DataSet("ReturnDataSet")

        Debug.WriteLine(query)
        Try
            TakeConnection(oConnection)

            Dim oSqlCommand As SqlCommand = oConnection.CreateCommand()
            oSqlCommand.CommandType = CommandType.Text
            oSqlCommand.CommandText = query

            Dim oAdapter As New SqlDataAdapter(oSqlCommand)
            oAdapter.Fill(oReturnDataSet)
            'Catch ex As SqlException
            '    MsgBox(ex.Message)
        Finally
            ReleaseConnection(oConnection)
        End Try

        Return oReturnDataSet

    End Function

#End Region

#Region "ScalarQuery"
    Public Function ScalarQuery(ByVal query As String) As Object


        Dim oConnection As SqlConnection = Nothing
        Dim oReturnObject As Object = Nothing

        Debug.WriteLine(query)
        Try
            TakeConnection(oConnection)

            Dim oSqlCommand As SqlCommand = oConnection.CreateCommand()
            oSqlCommand.CommandType = CommandType.Text
            oSqlCommand.CommandText = query

            oReturnObject = oSqlCommand.ExecuteScalar()

            'Catch ex As SqlException
            '    MsgBox(ex.Message)
        Finally
            ReleaseConnection(oConnection)
        End Try

        Return oReturnObject

    End Function
#End Region

#Region "ReaderQuery"
    Public Function ReaderQuery(ByVal query As String) As IDataReader


        Dim oConnection As SqlConnection = Nothing
        Dim oDataReader As IDataReader = Nothing

        Debug.WriteLine(query)
        Try
            TakeConnection(oConnection)

            Dim oSqlCommand As SqlCommand = oConnection.CreateCommand()
            oSqlCommand.CommandType = CommandType.Text
            oSqlCommand.CommandText = query

            oDataReader = oSqlCommand.ExecuteReader(CommandBehavior.CloseConnection)

            'Catch ex As SqlException
            '    MsgBox(ex.Message)
        Finally

        End Try

        Return oDataReader

    End Function

#End Region

End Class