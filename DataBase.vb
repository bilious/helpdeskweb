Imports System.Data.SqlClient
Imports System.Configuration
Imports Common

Public Class _DataBase

    Public Function OpenConnection() As SqlConnection
        Dim objConn As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings.Get("ConnectionString"))

        Try
            objConn.Open()
        Catch ex As SqlException
            MsgBox(ex.Message)
        Finally
        End Try
        Return objConn
    End Function

    Public Function ExecuteNonQuery(ByVal objConn As SqlConnection, ByVal sSQL As String)
        Dim oCmd As SqlCommand
        objConn.CreateCommand()
        oCmd = New SqlCommand
        oCmd.Connection = objConn
        oCmd.CommandText = sSQL
        Try
            oCmd.ExecuteNonQuery()
        Catch ex As SqlException
            MsgBox(ex.Message)
        Finally
        End Try
    End Function

    Public Function ExecuteDataSet() As DataSet

    End Function

    Public Function ExecuteDataReader(ByVal objConn As SqlConnection, ByVal sSQL As String) As SqlClient.SqlDataReader
        Dim oCmd As SqlCommand
        Dim oDR As SqlDataReader
        Try
            oCmd = New SqlCommand
            With oCmd
                .Connection = objConn
                .CommandText = sSQL
                oDR = .ExecuteReader()
            End With

        Catch oExcept As Exception
        End Try
        Return oDR
    End Function

    Public Function ExecuteScalar()

    End Function

    Public Function ExecuteDataReader2(ByVal objConn As SqlConnection, ByVal oCmd As SqlCommand, ByVal sSQL As String) As SqlDataReader
        Dim oDR As SqlDataReader

        Try
            With oCmd
                .Connection = objConn
                .CommandText = sSQL
                oDR = .ExecuteReader()
            End With

        Catch oExcept As Exception
        End Try
        Return oDR
    End Function
End Class
