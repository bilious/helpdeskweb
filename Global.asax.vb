Imports System.Web
Imports System.Web.SessionState
Imports Common
Imports Microsoft.Win32
Imports MSFoundationClasses

Public Class [Global]
    Inherits System.Web.HttpApplication

#Region " Component Designer Generated Code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        components = New System.ComponentModel.Container()
    End Sub

#End Region

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application is started
        Dim dtExpires As DateTime
        Dim strLicencee As String = ""
        Dim tKey As String = ""
        Try
            If ConfigurationSettings.AppSettings.Get("ProductCode") Is Nothing Then
                Throw New Exception("1")
            End If
            If ConfigurationSettings.AppSettings.Get("LicenceKey") Is Nothing Then
                Throw New Exception("1")
            End If

            Dim oLix As New Licence("Hexagon", ConfigurationSettings.AppSettings.Get("ProductCode"), 421)
            If oLix.IsValid Then
                tKey = ConfigurationSettings.AppSettings.Get("LicenceKey")
                Dim plainText As String = MSFoundationClasses.KeyEncrypter.Decrypt(CType(tKey, String), "Hexagon", "s@1tValue", "SHA1", 2, "@1B2c3D4e5F6g7H8", 128)
                Dim licence() As String = plainText.Split("-")
                If licence(1) <> 421 Then
                    Throw New Exception("2")
                End If

                dtExpires = CDate(licence(2))
                Dim intyear = dtExpires.Year
                Dim intmonth = dtExpires.Month
                If intyear < Now.Year Then
                    Throw New Exception("3")
                End If
                If intyear = Now.Year Then
                    If intmonth < Now.Month Then
                        Throw New Exception("3")
                    End If
                End If
                strLicencee = licence(0)
            Else
                Throw New Exception("4")
            End If
            GlobalVariables.DBConnStr = ConfigurationSettings.AppSettings.Get("ConnectionString")
            GlobalVariables.HelpDeskExpiryDate = dtExpires
            GlobalVariables.ActiveUserName = strLicencee
            GlobalVariables.ProductCode = ConfigurationSettings.AppSettings.Get("ProductCode")
        Catch ex As Exception
            GlobalVariables.ErrorMessage = ex.Message
        End Try


    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session is started
    End Sub

    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires at the beginning of each request
    End Sub

    Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires upon attempting to authenticate the use
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when an error occurs
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session ends
    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application ends
    End Sub

End Class
