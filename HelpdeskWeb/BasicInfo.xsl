
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  >
  <xsl:template match="/">
  
  <xsl:for-each select="ReturnDataSet">
  <xsl:for-each select="Table">
  <table border="0" width="60%" class="HTable"><tr><td class="summarytitle">Details</td></tr>
  <tr><td>
  <table border="0" align="center" width="100%">
  <tr class="normal">
	<td >Client</td><td><xsl:value-of select="ClientName"/></td>
  </tr>
  <xsl:if test="PropertyName != ''"> 
		    <tr class="normal">
			<td >PropertyName</td><td><xsl:value-of select="PropertyName"/></td>
  </tr>		
  </xsl:if>	
  <xsl:if test="UnitName != ''"> 
		    <tr class="normal">
			<td >UnitName</td><td><xsl:value-of select="UnitName"/></td>
  </tr>		
  </xsl:if>	
    <tr class="normal">
	<td >Reference</td><td><xsl:value-of select="EXTREF"/></td>
  </tr>
<xsl:if test="Telephone != ''"> 
	<tr class="normal">
	<td>Telephone</td><td><xsl:value-of select="Telephone"/></td>
	</tr>
</xsl:if>	
<xsl:if test="Address1 != ''">   
	<tr class="normal">
	<td valign = "Top">Address</td>
	<td>
	<xsl:if test="Address1 != ''"> 
				 <xsl:value-of select="Address1"/>,&#160;
	</xsl:if>	
    <xsl:if test="Address2 != ''"> 
		<xsl:value-of select="Address2"/>,&#160;
    </xsl:if>	
    <xsl:if test="Address3 != ''"> 
		<xsl:value-of select="Address3"/>,&#160;<br />
	</xsl:if>	
    <xsl:if test="City != ''"> 
		<xsl:value-of select="City"/>,&#160;
	</xsl:if>	
    <xsl:if test="Postcode != ''"> 
		<xsl:value-of select="Postcode"/>,&#160;
	</xsl:if>	
	</td>
	</tr>
</xsl:if>	
<xsl:if test="TenantName != ''"> 
  <tr>
  <td valign="Top" class="normal">Tenant</td>
  <td class="normal">
   <xsl:if test="TenantName != ''"> 
				 <xsl:value-of select="TenantName"/>
			 				  <xsl:if test="TenantTelephone != ''"><br/>Tel
									<xsl:value-of select="TenantTelephone"/>
								</xsl:if>		
				  <xsl:if test="TenantEmail != ''"><br/>Email
				 <xsl:value-of select="TenantEmail"/>
				</xsl:if>	
  
	</xsl:if>	
    </td>
	</tr>
</xsl:if>		
   </table>
  </td></tr></table>
</xsl:for-each>
  </xsl:for-each>
 
  </xsl:template>
 </xsl:stylesheet>
