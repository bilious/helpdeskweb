Imports System.Xml
Imports System.Data.SqlClient
Imports System.Xml.Xsl
Imports System.Text
Imports System.Data
Imports Common


Public Class CallDetails
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents xmlProducts As System.Web.UI.WebControls.Xml

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private oDataAccessor As New DataAccessor(ConfigurationSettings.AppSettings.Get("ConnectionString"))
    Private oNewSummaryHelper As New NewSummaryHelper

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'If Not Page.Request("CallID") Is Nothing Then

        '    Dim intCallID As Int32 = Int32.Parse(Page.Request("CallID"))

        '    Dim strCompoundSql As String = (New StringBuilder).AppendFormat(oNewSummaryHelper.SqlForCall, intCallID).ToString
        '    Dim dstSummary As DataSet = oDataAccessor.SelectQuery(strCompoundSql)


        '    dstSummary.DataSetName = "SummaryDetails"
        '    xmlProducts.Document = New XmlDataDocument(dstSummary)
        '    xmlProducts.TransformSource = "CallSummary.xsl"

        'End If
        Try

            If Not Request("CallID") Is Nothing Then
                Dim intCallID = Int32.Parse(Request("CallID"))
                Dim CurrentTag As String = ""
                Dim dadCall As New SqlDataAdapter
                Dim dstSummary As New DataSet
                Dim cmdCall As SqlCommand
                'Dim DB As New DataBase
                Dim Conn As SqlConnection
                Dim strCompoundSql As String = ""
                oDataAccessor.TakeConnection(Conn)
                If intCallID = 0 Then
                    strCompoundSql = "SELECT [Description] As StatusDesc, Count(IncidentID) As OpenCalls FROM View_WebSummary_Totals  Where (StatusID != 5) GROUP BY [Description]; SELECT PriorityDesc, Count(IncidentID) As OpenCalls FROM View_WebSummary_Totals Inner Join Config_Priority On View_WebSummary_Totals.PriorityID= Config_Priority.PriorityID Where (StatusID != 5) GROUP BY PriorityDesc;" & _
                    "Select ClientName, Count(IncidentID) As OpenCalls From View_WebSummary_Totals Inner Join View_Client_EXT On View_WebSummary_Totals.ClientID= View_Client_EXT.ClientID Where (StatusID != 5)  Group By ClientName"
                    ' strCompoundSql = "EXEC sp_WebServer_CallDetails"
                    cmdCall = New SqlCommand(strCompoundSql, Conn)
                    dadCall.SelectCommand = cmdCall
                    dadCall.Fill(dstSummary, "SummaryDetails")

                    xmlProducts.Document = New XmlDataDocument(dstSummary)
                    xmlProducts.TransformSource = "CallRoot.xsl"
                Else
                    strCompoundSql = (New StringBuilder).AppendFormat(oNewSummaryHelper.SqlForCall, intCallID).ToString
                    'strCompoundSql = "EXEC sp_WebServer_CallDetails " & intCallID
                    cmdCall = New SqlCommand(strCompoundSql, Conn)
                    dadCall.SelectCommand = cmdCall
                    dadCall.Fill(dstSummary, "SummaryDetails")

                    xmlProducts.Document = New XmlDataDocument(dstSummary)
                    xmlProducts.TransformSource = "CallSummary.xsl"
                End If




            End If
        Catch ex As Exception
        End Try

    End Sub

End Class
