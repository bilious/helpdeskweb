Imports System.Xml
Imports System.Data.SqlClient
Imports System.Xml.Xsl
Imports System.IO

Public Class CallList
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents xmlProducts As System.Web.UI.WebControls.Xml
    Protected WithEvents lblWarning As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private SummaryID As Integer = 0
    Private SummType As Integer = 0
    Private ClientRefID As Long = 0
    Private PropertyID As Long = 0
    Private UnitID As Long = 0
    Private AssetID As Long = 0
    Private SupplierID As Long = 0
    Private BaseSQL, WhereSQL, FinalSQL As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        '--------------------------------------------------------------------------
        SummaryID = Request("SummaryID")
        SummType = Request("SummType")
        ClientRefID = Request("ClientID")
        PropertyID = Request("PropertyID")
        UnitID = Request("UnitID")
        SupplierID = Request("FromID")
        AssetID = Request("AssetID")
        BaseSQL = "SELECT *,'CALLDETAILS' AS GroupName FROM VIEW_INCIDENCE"

        '        BaseSQL = "select *,'CALLDETAILS' AS GroupName," & _
        '"(SELECT Sum(TotalCost) FROM View_Incidence_Costs WHERE IncidentID=Incident.IncidentID) 'CostTotal'," & _
        '"(SELECT Sum(QuantityOrdered * CostPerUnit) FROM View_Incidence_Parts WHERE IncidentID=Incident.IncidentID) 'PartsTotal'," & _
        '"(SELECT Sum(LineTotal) FROM View_Incidence_Charges WHERE IncidentID=Incident.IncidentID) 'ChargesTotal'," & _
        '"(SELECT Sum(TimeSpent) FROM View_Incidence_Actions WHERE IncidentID=Incident.IncidentID) 'TotalTime'" & _
        '"from view_incidence as Incident "


        WhereSQL = ""

        If ClientRefID > 0 Then
            WhereSQL = "ClientID=" & ClientRefID
        End If
        If PropertyID > 0 Then
            WhereSQL += " AND PropertyID=" & PropertyID
        End If
        If UnitID > 0 Then
            WhereSQL += " AND UnitID=" & UnitID
        End If
        If AssetID > 0 Then
            WhereSQL += " AND AssetID=" & AssetID
        End If

        If SupplierID > 0 Then
            WhereSQL = " SupplierID=" & SupplierID
            'If ClientRefID = 0 Then
            '    WhereSQL = " SupplierID=" & SupplierID
            'Else
            '    WhereSQL += " AND SupplierID=" & SupplierID
            'End If

        End If


        Select Case SummType
            Case 1      'OPEN CALLS
                If WhereSQL.Length > 0 Then
                    FinalSQL = BaseSQL & " WHERE StatusID=1 AND " & WhereSQL
                Else
                    FinalSQL = BaseSQL & " WHERE StatusID=1"
                End If

            Case 2      'Assigned To Engineer
                If WhereSQL.Length > 0 Then
                    FinalSQL = BaseSQL & " WHERE StatusID=2 AND EngineerID>0 AND " & WhereSQL
                Else
                    FinalSQL = BaseSQL & " WHERE StatusID=2 AND EngineerID>0"
                End If

            Case 3      'Awaiting Parts
                If WhereSQL.Length > 0 Then
                    FinalSQL = BaseSQL & " WHERE StatusID=3 AND " & WhereSQL
                Else
                    FinalSQL = BaseSQL & " WHERE StatusID=3"
                End If

            Case 4      'In Progress Status Calls
                If WhereSQL.Length > 0 Then
                    FinalSQL = BaseSQL & " WHERE StatusID=4 AND " & WhereSQL
                Else
                    FinalSQL = BaseSQL & " WHERE StatusID=4"
                End If

            Case 5      'CLOSED CALLS In Last 24 hrs
                If WhereSQL.Length > 0 Then
                    FinalSQL = BaseSQL & " WHERE StatusID=5 AND " & WhereSQL
                Else
                    FinalSQL = BaseSQL & " WHERE StatusID=5"
                End If

            Case 6      'Planned Calls
                If WhereSQL.Length > 0 Then
                    FinalSQL = BaseSQL & " WHERE StatusID=6 AND " & WhereSQL
                Else
                    FinalSQL = BaseSQL & " WHERE StatusID=6"
                End If
            Case 8     '...
                If WhereSQL.Length > 0 Then
                    FinalSQL = BaseSQL & " WHERE StatusID=8 AND " & WhereSQL
                Else
                    FinalSQL = BaseSQL & " WHERE StatusID=8"
                End If

            Case 9      '...
                If WhereSQL.Length > 0 Then
                    FinalSQL = BaseSQL & " WHERE StatusID=9 AND " & WhereSQL
                Else
                    FinalSQL = BaseSQL & " WHERE StatusID=9"
                End If


            Case 7      'To Be Recharged
                If WhereSQL.Length > 0 Then
                    FinalSQL = BaseSQL & " WHERE StatusID=7 AND " & WhereSQL
                Else
                    FinalSQL = BaseSQL & " WHERE StatusID=7"
                End If

            Case 10      'Scheduled Calls
                If WhereSQL.Length > 0 Then
                    FinalSQL = BaseSQL & " WHERE CallTypeID=2 AND " & WhereSQL
                Else
                    FinalSQL = BaseSQL & " WHERE CallTypeID=2"
                End If

            Case 23      'Total Calls
                If WhereSQL.Length > 0 Then
                    FinalSQL = BaseSQL & " WHERE " & WhereSQL
                Else
                    FinalSQL = BaseSQL
                End If
                GoTo JumpStep
        End Select

        If SupplierID > 0 Then
            FinalSQL = FinalSQL & " AND SupplierID = " & SupplierID
        End If
JumpStep:
        '--------------------------------------------------------------------
        Dim dadCall As New SqlDataAdapter
        Dim dstSummary As New DataSet
        Dim cmdCall As SqlCommand
        'Dim DB As New DataBase
        Dim Conn As SqlConnection
        Dim oDataAccessor As New DataAccessorLayer.DataAccessor
        oDataAccessor.TakeConnection(Conn)
        'Conn = DB.OpenConnection()
        FinalSQL += " ORDER BY OpenSince"

        cmdCall = New SqlCommand(FinalSQL, Conn)
        dadCall.SelectCommand = cmdCall
        dadCall.Fill(dstSummary, "SummaryDetails")

        ' Pass DataSet for XSL Transform
        'Dim str As New StringWriter

        'dstSummary.WriteXml(str, XmlWriteMode.IgnoreSchema)
        'Response.Write(FinalSQL)
        If dstSummary.Tables(0).Rows.Count = 0 Then
            lblWarning.Text = "There Are No Records To Display"
            xmlProducts.Visible = False
        Else
            lblWarning.Visible = False
            xmlProducts.Document = New XmlDataDocument(dstSummary)
            xmlProducts.TransformSource = "CallList.xsl"

        End If
        
    End Sub

End Class
