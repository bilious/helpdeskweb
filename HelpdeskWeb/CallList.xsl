
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  >
 <xsl:key name="group-by-type" match="SummaryDetails" use="GroupName" />
 <xsl:template match="NewDataSet">

    <table width="100%" border="0" align="center" 
      cellpadding="1" cellspacing="0" class="normal">
      <tr><td colspan="8" align="center"><span class="style1">Call List</span></td></tr>
      <tr class="normal">
      <td width="7%" ><strong>Call ID</strong></td>
      <td width="15%" ><strong>Call Type</strong></td>
      <td width="8%" ><strong>Status</strong></td>
      <td width="13%" ><strong>Contact</strong></td>
      <td width="13%" ><strong>Property Ref </strong></td>
      <td width="10%" ><strong>Asset</strong></td>
      <td width="27%"><strong>Subject</strong></td>
      </tr>
	<xsl:for-each select="SummaryDetails[count(. | key('group-by-groupname', GroupName)[1]) = 1]">
  
     <xsl:variable name="IDValue">
		<xsl:value-of select="IncidentID"/>
	</xsl:variable> 
   
      <tr>
        <td>
          <a href="CallDetails.aspx?CallID={$IDValue}"><xsl:value-of select="IncidentName"/></a>
        </td>

        <td> <xsl:value-of select="CallTypeDesc" /> </td>
        <td>   <xsl:value-of select="StatusDesc" /> </td>
        <td>   <xsl:value-of select="CallerName" /></td>
        <td>
          <xsl:value-of select="PropertyName" /> 
        </td>

        <td>
          <xsl:value-of select="Description" /> 
        </td>


        <td>
                <xsl:value-of select="Subject" /> 
        </td>
      </tr> 
      </xsl:for-each>
      
   </table>
   <table width="95%" class="normal"  align="center">
   <tr><td><a href="javascript:history.go(-1)">Back</a></td></tr>
   </table>
   
</xsl:template>
 </xsl:stylesheet>
