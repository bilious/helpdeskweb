
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  >
 <xsl:key name="group-by-type" match="SummaryDetails" use="GroupName" />
 <xsl:template match="NewDataSet">
 <p>&#160;</p>

    <table width="100%" border="0" align="center" 
      cellpadding="1" cellspacing="0" class="normal">
      
      <tr class="normal">
      <td width="7%" ><strong>Status</strong></td>
      <td width="7%" ><strong>Open Calls</strong></td>
      </tr>
	<xsl:for-each select="SummaryDetails[count(. | key('group-by-groupname', GroupName)[1]) = 1]">
  
           <tr>
        <td>
          <xsl:value-of select="StatusDesc" /> 
        </td>
        <td>
          <xsl:value-of select="OpenCalls" /> 
        </td>
</tr>
 </xsl:for-each>      
   </table>
   
    <table width="100%" border="0" align="center" 
      cellpadding="1" cellspacing="0" class="normal">
      
      <tr class="normal">
      <td width="7%" ><strong>Priority</strong></td>
      <td width="7%" ><strong></strong></td>
      </tr>
	<xsl:for-each select="SummaryDetails1[count(. | key('group-by-groupname', GroupName)[1]) = 1]">
  
       <tr>
        <td>
          <xsl:value-of select="PriorityDesc" /> 
        </td>
        <td>
          <xsl:value-of select="OpenCalls" /> 
        </td>
</tr>
 </xsl:for-each>      
   </table>

    <table width="100%" border="0" align="center" 
      cellpadding="1" cellspacing="0" class="normal">
      
      <tr class="normal">
      <td width="7%" ><strong>Client</strong></td>
      <td width="7%" ><strong></strong></td>
      </tr>
	<xsl:for-each select="SummaryDetails2[count(. | key('group-by-groupname', GroupName)[1]) = 1]">
   
      <tr>
        <td>
          <xsl:value-of select="ClientName" /> 
        </td>
        <td>
          <xsl:value-of select="OpenCalls" /> 
        </td>
</tr>
 </xsl:for-each>      
   </table>

   
   <table width="95%" class="normal"  align="center">
   <tr><td><a href="javascript:history.go(-1)">Back</a></td></tr>
   </table>
   
</xsl:template>
 </xsl:stylesheet>
