
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="SummaryDetails">
  <center>
  
    <table width="70%">
    <tr>
      <td align="center" class="summarytitle">Call Summary </td>
    </tr>
    </table>
    <br/>
    <table width="70%" 
      cellpadding="1" cellspacing="0" class="de_table">
      <tr>
		<TD WIDTH="30%">Call ID:</TD>
        <td width="70%" colspan="3"><b>
          <xsl:value-of select="IncidentName" /> </b>
        </td>
       </tr>
       
       <tr>
         <TD>Call Type: </TD>
         <td colspan="3"><xsl:value-of select="CallTypeDesc" /> </td>
       </tr>
       <tr>
         <TD>Priority:</TD>
         <td colspan="3"><xsl:value-of select="PriorityDesc" /> </td>
       </tr>
       <tr>
         <TD>Status:</TD>
         <td colspan="3"><xsl:value-of select="StatusDesc" /> </td>
       </tr>
       <tr>
         <TD> </TD>
         <td colspan="3"> </td>
       </tr>
       <tr>
		<TD WIDTH="30%">Client:</TD>
        <td width="70%" colspan="3">
          <xsl:value-of select="ClientName" /> 
        </td>
       </tr> 

       <tr>
		<TD WIDTH="30%">Property:</TD>
        <td width="70%" colspan="3">
          <xsl:value-of select="PropertyName" /> 
        </td>
       </tr> 

       <tr>
		<TD WIDTH="30%">Asset:</TD>
        <td width="70%" colspan="3">
          <xsl:value-of select="AssetDescription" /> 
        </td>
       </tr> 
     <tr>
         <TD valign="top">Engineer:</TD>
         <td><xsl:value-of select="SupplierName" />
         </td>
          </tr>
       
 
       <tr>
         <TD valign="top">Contact:</TD>
         <td>Tenant:<xsl:value-of select="TenantName" />(<xsl:value-of select="TenantTel" />)<br/>
         <xsl:value-of select="CallerName" />&#160;
          (<xsl:value-of select="CallerPosition" /> )<br/>
         Tel:<xsl:value-of select="CallerTel" />
         
         </td>
          </tr>
       <tr>
		<TD WIDTH="30%">Total Costs:</TD>
        <td width="70%" colspan="3">
        <xsl:if test="CostsTotal >0"> 
          <xsl:value-of select="format-number(CostsTotal, '###,###,##0.00')" /> 
         </xsl:if>				
        </td>
       </tr> 
       
     <tr>
		<TD WIDTH="30%">Total Parts:</TD>
        <td width="70%" colspan="3">
        <xsl:if test="PartsTotal >0"> 
          <xsl:value-of select="format-number(PartsTotal, '###,###,##0.00')" /> 
         </xsl:if>				
        </td>
       </tr> 
            <tr>
		<TD WIDTH="30%">Total Charges:</TD>
		 <td width="70%" colspan="3">
        <xsl:if test="ChargesTotal >0">   
             
 <xsl:value-of select="format-number(ChargesTotal, '###,###,##0.00')" /> 
         </xsl:if>				
        </td>
       </tr> 

        <tr>
		<TD WIDTH="30%">Total Invoiced:</TD>
		 <td width="70%" colspan="3">
		 <xsl:if test="TotalInvoiced >0"> 
          <xsl:value-of select="format-number(TotalInvoiced, '###,###,##0.00')" /> 			
           </xsl:if>	
        </td>
       </tr>

       <tr>
       		<TD WIDTH="30%"><u>Dream Budget:</u></TD>
        <td width="70%" colspan="3"><u>
         <xsl:choose>
         <xsl:when test="BudgetValue >0">  
         <xsl:value-of select="format-number(BudgetValue, '###,###,##0.00')" /> 
        </xsl:when>
        <xsl:otherwise>
         <xsl:text>0.00</xsl:text>
        </xsl:otherwise>
         </xsl:choose> 
          </u>
        </td>
       </tr>
          <tr>
       		<TD WIDTH="30%"><u>Dream Actual:</u></TD>
       		<td width="70%" colspan="3"><u>
         <xsl:choose>
         <xsl:when test="ActualValue >0">  
         <xsl:value-of select="format-number(ActualValue, '###,###,##0.00')" /> 
        </xsl:when>
        <xsl:otherwise>
         <xsl:text>0.00</xsl:text>
        </xsl:otherwise>
         </xsl:choose> 
          </u>
        </td>
       </tr>
       <tr>
       		<TD WIDTH="30%"><u>Dream Commitment:</u></TD>
        <td width="70%" colspan="3"><u>
                 <xsl:choose>
        <xsl:when test="CommitmentValue >0">
        <xsl:choose>
				<xsl:when test="CommitmentValue >0">
               <xsl:value-of select="format-number(CommitmentValue, '###,###,##0.00')" /> 
               </xsl:when>
				<xsl:otherwise>
				<xsl:value-of select="format-number(CommitmentValue, '###,###,##0.00')" /> 
				</xsl:otherwise>
        </xsl:choose>
               <xsl:variable name="P1"><xsl:value-of select="CommitmentValue"/></xsl:variable>
        </xsl:when>
        <xsl:otherwise>
			<xsl:choose>
				<xsl:when test="CostsTotal >0">
               <xsl:value-of select="format-number(CostsTotal, '###,###,##0.00')" /> 
               </xsl:when>
				<xsl:otherwise>
				<xsl:text>0.00</xsl:text>
				</xsl:otherwise>
        </xsl:choose>
        </xsl:otherwise>
        </xsl:choose>
       </u>
        </td>
       </tr>
       
   </table>
      <table width="70%" class="normal"  align="center">
   <tr><td><p>
   Opened On: 
   <xsl:value-of select="substring(OpenSince, 1, 10)"/>
    <br />
    Closed On:<xsl:value-of select="substring(ClosedAt, 1, 10)"/>
    <br />
   Called Logged By: <xsl:value-of select="UserName" />
   </p>
     <p align="right">
     <table>
     <tr><td><a href="javascript:history.go(-1)"><img src="backbutton.jpg" border="0"></img> </a></td>
     <td><a href="Command$EditCall@integra&amp;CallID={IncidentID}"><img src="editbutton.jpg" border="0"></img></a></td></tr>
     </table>
     </p></td></tr>
   </table>

  </center>	
  </xsl:template>
 </xsl:stylesheet>
