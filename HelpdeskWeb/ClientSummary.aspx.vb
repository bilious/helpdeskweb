Imports System.Xml
Imports System.Data.SqlClient
Imports System.Xml.Xsl
Imports System.IO

Public Class ClientSummary
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents xmlProducts As System.Web.UI.WebControls.Xml

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private ClientRefID As Long = 0

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        ClientRefID = Request("ClientRefID")
        Dim CurrentTag As String = ""

        '--------------------------------------------------------------------------
        '// Craete DataTable
        Dim TableSummarySchema(4) As String
        TableSummarySchema(0) = "GroupName"
        TableSummarySchema(1) = "Type"
        TableSummarySchema(2) = "Description"
        TableSummarySchema(3) = "Value"
        TableSummarySchema(4) = "CID"

        Dim DBTable_Summary As New DataTable("SummaryDetails")
        DBTable_Summary.Columns.Clear()
        Dim i As Integer = 0

        For i = 0 To 4                              'Create Data Table Columns
            Dim DBFD As New DataColumn(TableSummarySchema(i))
            DBFD.DefaultValue = ""
            DBFD.DataType = System.Type.GetType("System.String")
            DBTable_Summary.Columns.Add(DBFD)
        Next
        '--------------------------------------------------------------------------
        'Dim DB As New DataBase
        Dim DR As SqlDataReader
        Dim Conn As SqlConnection
        Dim dstSummary As New DataSet
        Dim SQL As String = ""
        Dim SQLWHERE As String = ""
        Dim oDataAccessor As New DataAccessorLayer.DataAccessor
        oDataAccessor.TakeConnection(Conn)
        'Conn = DB.OpenConnection()
        '--------------------------------------------------------------------------
        '// SOME QUERIES ARE YET TO BE CHANGED
        '----------- GET Client Name
        'SQL = "select 'Properties' as GroupName, 'Client' AS Type, ClientName AS Description,0 AS Value,ClientID as CID From tbl_Client where ClientID=" & ClientRefID & ";"

        ''----------- GET Assigned To Engineer
        'SQL += "Select 'Properties' as GroupName, 'Property' as Type,PropertyName as Description,0 AS VALUE,0 as CID from tbl_Property where clientid=" & ClientRefID & ";"

        ''----------- GET Awaiting Parts (QUERY TO UPDATE)
        'SQL += "SELECT 'Call' as GroupName,'Call - ' + CAST(IncidentID as varchar(12)) as Type,PropertyName as Description,0 as Value, IncidentID as CID From View_Incident where ClientID=" & ClientRefID
        '--------------------------------------------------------------------------
        ' Populate DataTable with Results
        'DR = DB.ExecuteDataReader(Conn, SQL)
        SQL = "Exec sp_WebServer_ClientDetails " & ClientRefID
        Dim oSqlCommand As New SqlCommand(SQL, Conn)
        DR = oSqlCommand.ExecuteReader

        i = 0
        While DR.Read()
            CurrentTag = DR.Item("GroupNAme")
            While CurrentTag = DR.Item("GroupNAme")
                DBTable_Summary.LoadDataRow(TableSummarySchema, True)
                DBTable_Summary.Rows(i)(0) = DR.Item("GroupName")
                DBTable_Summary.Rows(i)(1) = DR.Item("Type")
                DBTable_Summary.Rows(i)(2) = DR.Item("Description")
                If Not IsDBNull(DR.Item("Value")) Then
                    DBTable_Summary.Rows(i)(3) = DR.Item("Value")
                Else
                    DBTable_Summary.Rows(i)(3) = 0
                End If
                If Not IsDBNull(DR.Item("CID")) Then
                    DBTable_Summary.Rows(i)(4) = DR.Item("CID")
                Else
                    DBTable_Summary.Rows(i)(4) = 0
                End If
                i += 1
                If Not DR.Read() Then
                    Exit While
                End If
            End While
            DR.NextResult()
        End While
        DR.Close()
        Conn.Close()

        dstSummary.Tables.Add(DBTable_Summary)     'Convert To DataSet
        '--------------------------------------------------------------------------
        ' Pass DataSet for XSL Transform
        'Dim str As New StringWriter

        'dstSummary.WriteXml(str, XmlWriteMode.IgnoreSchema)
        'Response.Write(str.ToString)

        xmlProducts.Document = New XmlDataDocument(dstSummary)
        xmlProducts.TransformSource = "NewSummary.xsl"

    End Sub

End Class
