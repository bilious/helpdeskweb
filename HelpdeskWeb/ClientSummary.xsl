<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  >
  <xsl:template match="SummaryDetails">
  <center>
      <table width="70%" border="1"
      cellpadding="1" cellspacing="0" class="normal">
      <tr>
        <td width="30%">
         <xsl:choose>
			<xsl:when test="Type='Properties'">
				<a href=""></a><xsl:value-of select="Type" /><a/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="Type" />
			</xsl:otherwise>
		 </xsl:choose>
         </td>
        <td width="70%">
          <xsl:value-of select="Description" /> 
        </td>

      </tr>
      </table>
   </center>	
  </xsl:template>
 </xsl:stylesheet>
