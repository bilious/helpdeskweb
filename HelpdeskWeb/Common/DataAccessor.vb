﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports MSFoundationClasses


Public Class DataAccessor

    Private strConnectionString As String = ""


    Public Sub New()

        Me.New(Scope.GetParameter("ConnectionString"))

    End Sub
    Public Sub New(ByVal conn As String)

        Scope.SetParameter("ConnectionString", conn)
        strConnectionString = conn

    End Sub
    Function GetDBConnectionInfo() As String()
        Dim tpos As Int32 = 0
        Dim strConnectionInfo(3) As String
        Dim StrUser As String
        Dim strPass As String
        Dim ConnectionArray() As String = strConnectionString.Split(";")
        Dim strServer As String = ConnectionArray(0)
        tpos = InStr(strServer, "=")
        strServer = Mid$(strServer, (tpos + 1), 32000)
        Dim strDatabase As String = ConnectionArray(1)
        tpos = InStr(strDatabase, "=")
        strDatabase = Mid$(strDatabase, (tpos + 1), 32000)

        StrUser = ConnectionArray(2)
        tpos = InStr(StrUser, "=")
        StrUser = Mid$(StrUser, (tpos + 1), 32000)
        strPass = ConnectionArray(3)
        tpos = InStr(strPass, "=")
        strPass = Mid$(strPass, (tpos + 1), 32000)

        strConnectionInfo(0) = strServer
        strConnectionInfo(1) = strDatabase
        strConnectionInfo(2) = StrUser
        strConnectionInfo(3) = strPass
        Return strConnectionInfo
    End Function
    'Function GetConnectionInfo() As ConnectionInfo
    '    Dim tpos As Int32 = 0
    '    Dim oConnectionInfo As New ConnectionInfo
    '    Dim StrUser As String
    '    Dim strPass As String
    '    Dim ConnectionArray() As String = strConnectionString.Split(";")
    '    Dim strServer As String = ConnectionArray(0)
    '    tpos = InStr(strServer, "=")
    '    strServer = Mid$(strServer, (tpos + 1), 32000)
    '    Dim strDatabase As String = ConnectionArray(1)
    '    tpos = InStr(strDatabase, "=")
    '    strDatabase = Mid$(strDatabase, (tpos + 1), 32000)

    '    StrUser = ConnectionArray(2)
    '    tpos = InStr(StrUser, "=")
    '    StrUser = Mid$(StrUser, (tpos + 1), 32000)
    '    strPass = ConnectionArray(3)
    '    tpos = InStr(strPass, "=")
    '    strPass = Mid$(strPass, (tpos + 1), 32000)

    '    oConnectionInfo.ServerName = strServer
    '    oConnectionInfo.DatabaseName = strDatabase
    '    oConnectionInfo.UserID = StrUser
    '    oConnectionInfo.Password = strPass
    '    Return oConnectionInfo
    'End Function
    'Public Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
    '    Dim myTables As Tables = myReportDocument.Database.Tables
    '    Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
    '    For Each myTable In myTables
    '        'myTable.Location = myTable.Location
    '        Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
    '        myTableLogonInfo.ConnectionInfo = myConnectionInfo
    '        myTable.ApplyLogOnInfo(myTableLogonInfo)
    '        Dim strLoc As String
    '        strLoc = myTable.Location
    '        strLoc = myConnectionInfo.DatabaseName & Right(strLoc, Len(strLoc) - InStr(strLoc, ".") + 1)

    '    Next

    'End Sub
#Region "CheckConnection"
    Public Function CheckConnection() As Boolean

        Try
            Dim oConnection As SqlConnection = Nothing
            TakeConnection(oConnection)
            ReleaseConnection(oConnection)
            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function
#End Region

#Region "Connection"

    Public Sub TakeConnection(ByRef connection As SqlConnection)
        connection = New SqlConnection(strConnectionString)
        connection.Open()
    End Sub
    Public Sub ReleaseConnection(ByRef connection As SqlConnection)
        If Not connection Is Nothing Then
            connection.Close()
        End If
    End Sub

#End Region

#Region "CallProcedure"
    Public Sub CallProcedure(ByVal strProc As String, ByVal strValue As String)

    End Sub
#End Region

#Region "NonQuery"
    Public Sub NonQuery(ByVal query As String)


        'Debug.WriteLine(query)
        Dim oConnection As SqlConnection = Nothing

        Try
            TakeConnection(oConnection)

            Dim oSqlCommand As SqlCommand = oConnection.CreateCommand()
            oSqlCommand.CommandType = CommandType.Text
            oSqlCommand.CommandText = query
            oSqlCommand.ExecuteNonQuery()
            'Catch ex As SqlException
            '    MsgBox(ex.Message)
        Finally
            ReleaseConnection(oConnection)
        End Try

    End Sub
#End Region

#Region "ImplodeQuery"
    Public Function ImplodeQuery(ByVal query As String, ByVal strParam As String) As String

        'This Should Return A Single Column Dataset & ConCat Or Implode Them Into A Single String With A Seperator
        Dim oConnection As SqlConnection = Nothing
        Dim oReturnDataSet As New DataSet("ReturnDataSet")
        Dim strNewString As String = ""
        Try
            TakeConnection(oConnection)

            Dim oSqlCommand As SqlCommand = oConnection.CreateCommand()
            oSqlCommand.CommandType = CommandType.Text
            oSqlCommand.CommandText = query

            Dim oAdapter As New SqlDataAdapter(oSqlCommand)
            oAdapter.Fill(oReturnDataSet)
            'Catch ex As SqlException
            '    MsgBox(ex.Message)
        Finally
            ReleaseConnection(oConnection)
        End Try

        Dim IEnum As IEnumerator = oReturnDataSet.Tables(0).Rows.GetEnumerator
        While IEnum.MoveNext
            If strNewString = "" Then
                strNewString = IEnum.Current.ItemArray(0)
            Else
                strNewString = strNewString & strParam & IEnum.Current.ItemArray(0)
            End If

        End While

        Return strNewString

    End Function

#End Region
#Region "SelectQuery"
    Public Function SelectQuery(ByVal query As String) As DataSet

        'Debug.WriteLine(query)
        Dim oConnection As SqlConnection = Nothing
        Dim oReturnDataSet As New DataSet("ReturnDataSet")

        Try
            TakeConnection(oConnection)

            Dim oSqlCommand As SqlCommand = oConnection.CreateCommand()
            oSqlCommand.CommandType = CommandType.Text
            oSqlCommand.CommandText = query

            Dim oAdapter As New SqlDataAdapter(oSqlCommand)
            oAdapter.Fill(oReturnDataSet)
        Catch ex As SqlException
            WriteEventEntry("HHD SQL Error:" & Chr(10) & ex.Message)
            WriteEventEntry("HHD SQL :" & query, 2)
            Return Nothing
            '    MsgBox(ex.Message)
        Finally
            ReleaseConnection(oConnection)
        End Try

        Return oReturnDataSet

    End Function

#End Region

#Region "ScalarQuery"
    Public Function ScalarQuery(ByVal query As String) As Object

        'Debug.WriteLine(query)
        Dim oConnection As SqlConnection = Nothing
        Dim oReturnObject As Object = Nothing

        Try
            TakeConnection(oConnection)

            Dim oSqlCommand As SqlCommand = oConnection.CreateCommand()
            oSqlCommand.CommandType = CommandType.Text
            oSqlCommand.CommandText = query

            oReturnObject = oSqlCommand.ExecuteScalar()

        Catch ex As SqlException
            WriteEventEntry("HHD SQL Error:" & Chr(10) & ex.Message)
            WriteEventEntry("HHD SQL :" & query, 2)

            Return -1
        Finally
            ReleaseConnection(oConnection)
        End Try

        Return oReturnObject

    End Function
#End Region

#Region "ReaderQuery"
    Public Function ReaderQuery(ByVal query As String) As IDataReader

        'Debug.WriteLine(query)
        Dim oConnection As SqlConnection = Nothing
        Dim oDataReader As IDataReader = Nothing

        Try
            TakeConnection(oConnection)

            Dim oSqlCommand As SqlCommand = oConnection.CreateCommand()
            oSqlCommand.CommandType = CommandType.Text
            oSqlCommand.CommandText = query

            oDataReader = oSqlCommand.ExecuteReader(CommandBehavior.CloseConnection)

        Catch ex As SqlException
            WriteEventEntry("HHD SQL Error:" & Chr(10) & ex.Message)
            WriteEventEntry("HHD SQL :" & query, 2)
            Return Nothing
        Finally

        End Try

        Return oDataReader

    End Function

#End Region

#Region "CSVQuery"
    Function CSVQuery(ByVal strFileName As String) As DataSet
        Dim sR As String = StrReverse(strFileName)
        Dim sP As String = strFileName.Substring(0, Len(strFileName) - sR.IndexOf("\"))
        Dim strFolder As String = sP
        Dim strFile As String = StrReverse(sR.Substring(0, sR.IndexOf("\")))
        Dim ds As New DataSet
        Try
            Dim f As System.IO.File
            If f.Exists(strFolder & strFile) Then
                Dim ConStr As String = _
                "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & _
                strFolder & ";Extended Properties=""Text;HDR=No;FMT=Delimited\"""
                Dim conn As New OleDb.OleDbConnection(ConStr)
                Dim da As New OleDb.OleDbDataAdapter("Select * from " & _
                strFile, conn)
                da.Fill(ds, "TextFile")
                Return ds
            End If
        Catch ex As Exception
            Return Nothing
        End Try

    End Function
#End Region



#Region "Event Log Handler"
    Sub WriteEventEntry(ByVal strEventText As String, Optional ByVal Type As Int32 = 1)

        Dim objEventLog As New EventLog

        Try
            'Register the App as an Event Source
            If Not objEventLog.SourceExists("Hexagon Helpdesk") Then

                objEventLog.CreateEventSource("Hexagon Helpdesk", "Helpdesk Events")
            End If

            objEventLog.Source = "Hexagon Helpdesk"

            'WriteEntry is overloaded; this is one
            'of 10 ways to call it
            Select Case Type
                Case 1
                    objEventLog.WriteEntry(strEventText, EventLogEntryType.Error)
                Case Else
                    objEventLog.WriteEntry(strEventText, EventLogEntryType.Information)
            End Select


        Catch Ex As Exception

        End Try

    End Sub

    Public Function WriteToEventLog(ByVal Entry As String, _
       Optional ByVal AppName As String = "VB.NET Application", _
       Optional ByVal EventType As  _
       EventLogEntryType = EventLogEntryType.Information, _
       Optional ByVal LogName As String = "Application") As Boolean

        '*************************************************************
        'PURPOSE: Write Entry to Event Log using VB.NET
        'PARAMETERS: Entry - Value to Write
        '            AppName - Name of Client Application. Needed 
        '              because before writing to event log, you must 
        '              have a named EventLog source. 
        '            EventType - Entry Type, from EventLogEntryType 
        '              Structure e.g., EventLogEntryType.Warning, 
        '              EventLogEntryType.Error
        '            LogName: Name of Log (System, Application; 
        '              Security is read-only) If you 
        '              specify a non-existent log, the log will be
        '              created

        'RETURNS:   True if successful, false if not

        'EXAMPLES: 
        '1. Simple Example, Accepting All Defaults
        '    WriteToEventLog "Hello Event Log"

        '2.  Specify EventSource, EventType, and LogName
        '    WriteToEventLog("Danger, Danger, Danger", "MyVbApp", _
        '                      EventLogEntryType.Warning, "System")
        '
        'NOTE:     EventSources are tightly tied to their log. 
        '          So don't use the same source name for different 
        '          logs, and vice versa
        '******************************************************

        Dim objEventLog As New EventLog

        Try
            'Register the App as an Event Source
            If Not objEventLog.SourceExists(AppName) Then

                objEventLog.CreateEventSource(AppName, LogName)
            End If

            objEventLog.Source = AppName

            'WriteEntry is overloaded; this is one
            'of 10 ways to call it
            objEventLog.WriteEntry(Entry, EventType)
            Return True
        Catch Ex As Exception
            Return False

        End Try

    End Function
#End Region
End Class
