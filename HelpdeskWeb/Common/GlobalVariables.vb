Public Class GlobalVariables
    Private Shared _DBConnStr As String = ""
    Private Shared _SQL_DateFormat As String = "MM/dd/yyyy"
    Private Shared _SQL_DateTimeFormat As String = "MM/dd/yyyy hh:mm:ss"
    Private Shared _BaseURLPath As String = ""
    Private Shared _UserRoleLevel As Byte = 0
    Private Shared _ActiveUserID As Long = 0
    Private Shared _ActiveUserName As String = ""
    Private Shared _ApplicationNAme As String = "Hexagon - Purchase Orders"
    Private Shared _LandmarkConnStr As String = ""
    Public Shared HelpDeskActivationDate As DateTime = Nothing
    Public Shared HelpDeskExpiryDate As DateTime = Nothing
    Public Shared HelpDeskRegisteredMachine As String = ""
    Public Shared ErrorMessage As String = ""
    Public Shared ProductCode As String = ""

    Public Shared Property DBConnStr() As String
        Get
            Return _DBConnStr
        End Get
        Set(ByVal Value As String)
            _DBConnStr = Value
        End Set
    End Property
    Public Shared Property LandmarkConnStr() As String
        Get
            Return _LandmarkConnStr
        End Get
        Set(ByVal Value As String)
            _LandmarkConnStr = Value
        End Set
    End Property
    Public Shared Property UserRoleLevel() As Byte
        Get
            Return _UserRoleLevel
        End Get
        Set(ByVal Value As Byte)
            _UserRoleLevel = Value
        End Set
    End Property

    Public Shared Property BaseURLPath() As String
        Get
            Return _BaseURLPath
        End Get
        Set(ByVal Value As String)
            _BaseURLPath = Value
        End Set
    End Property

    Public Shared Property SQL_DateFormat() As String
        Get
            Return _SQL_DateFormat
        End Get
        Set(ByVal Value As String)
            _SQL_DateFormat = Value
        End Set
    End Property
    Public Shared Property SQL_DateTimeFormat() As String
        Get
            Return _SQL_DateTimeFormat
        End Get
        Set(ByVal Value As String)
            _SQL_DateTimeFormat = Value
        End Set
    End Property

    Public Shared Property ActiveUserName() As String
        Get
            Return _ActiveUserName
        End Get
        Set(ByVal Value As String)
            _ActiveUserName = Value
        End Set
    End Property

    Public Shared Property ActiveUserID() As Long
        Get
            Return _ActiveUserID
        End Get
        Set(ByVal Value As Long)
            _ActiveUserID = Value
        End Set
    End Property

    Public Shared ReadOnly Property ApplicationName() As String
        Get
            Return _ApplicationNAme
        End Get
    End Property

End Class
