Imports System.Xml
Imports System.Data.SqlClient
Imports System.Xml.Xsl
Imports System.Text
Imports System.Data

Public Class ContactDetails
    Inherits System.Web.UI.Page
    Private oDataAccessor As New DataAccessorLayer.DataAccessor
    Private oNewSummaryHelper As New NewSummaryHelper

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents xmlProducts As System.Web.UI.WebControls.Xml

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not Request("ContactID") Is Nothing Then
            Dim intContactID = Int32.Parse(Request("ContactID"))
            Dim CurrentTag As String = ""
            Dim dadCall As New SqlDataAdapter
            Dim dadCall2 As New SqlDataAdapter
            Dim dstSummary As New DataSet
            Dim dstSummary2 As New DataSet
            Dim cmdCall As SqlCommand
            Dim cmdCall2 As SqlCommand
            'Dim DB As New DataBase
            Dim Conn As SqlConnection
            Dim strCompoundSql As String = ""
            oDataAccessor.TakeConnection(Conn)
            If intContactID <> 0 Then
                Dim strDetailSQL As String = "Select * From View_Contact_EXT Where ContactID = " & intContactID
                cmdCall = New SqlCommand(strDetailSQL, Conn)
                dadCall.SelectCommand = cmdCall
                dadCall.Fill(dstSummary, "SummaryDetails")

                xmlProducts.Document = New XmlDataDocument(dstSummary)
                xmlProducts.TransformSource = "Contact.xsl"

                '                strCompoundSql = "Select Contact.*," & _
                '" (SELECT Count(CallID) FROM tbl_Incidence_Call WHERE LoggedBY={0}) 'TotalCalls'" & _
                ' " FROM View_Contact_Ext As Contact Where ContactID={0}"
                '                strCompoundSql = (New StringBuilder).AppendFormat(oNewSummaryHelper.SqlForCall, intContactID).ToString

                '                cmdCall2 = New SqlCommand(strCompoundSql, Conn)
                '                dadCall2.SelectCommand = cmdCall2
                '                dadCall2.Fill(dstSummary2, "SummaryDetails")

                '                Xml1.Document = New XmlDataDocument(dstSummary2)
                '                Xml1.TransformSource = "Contact.xsl"
            Else
                strCompoundSql = (New StringBuilder).AppendFormat(oNewSummaryHelper.SqlForCall, intContactID).ToString
                cmdCall = New SqlCommand(strCompoundSql, Conn)
                dadCall.SelectCommand = cmdCall
                dadCall.Fill(dstSummary, "SummaryDetails")

                xmlProducts.Document = New XmlDataDocument(dstSummary)
                xmlProducts.TransformSource = "CallSummary.xsl"
            End If




        End If
    End Sub

End Class
