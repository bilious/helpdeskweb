Imports System.Xml
Imports System.Data.SqlClient
Imports System.Xml.Xsl
Imports System.IO

Public Class ContactList
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents xmlProducts As System.Web.UI.WebControls.Xml

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Private SummaryID As Integer = 0
    Private SummType As Integer = 0
    Private ClientRefID As Long = 0
    Private PropertyID As Long = 0
    Private UnitID As Long = 0
    Private SupplierID As Long = 0
    Private BaseSQL, WhereSQL, FinalSQL As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        SummaryID = Request("SummaryID")
        SummType = Request("SummType")
        ClientRefID = Request("ClientID")
        PropertyID = Request("PropertyID")
        UnitID = Request("UnitID")
        SupplierID = Request("SupplierID")
        'Put user code to initialize the page here

        '--------------------------------------------------------------------
        Dim dadCall As New SqlDataAdapter
        Dim dstSummary As New DataSet
        Dim cmdCall As SqlCommand
        'Dim DB As New DataBase
        Dim Conn As SqlConnection
        Dim oDataAccessor As New DataAccessorLayer.DataAccessor
        oDataAccessor.TakeConnection(Conn)

        BaseSQL = "SELECT Title, FirstName, LastName, [Position], Telephone, Fax, Email , tbl_Contact.ContactID FROM tbl_Contact INNER JOIN tbl_ContactLink ON tbl_Contact.ContactID = tbl_ContactLink.ContactID Where "
        WhereSQL = ""

        If ClientRefID > 0 Then
            WhereSQL = "ClientID=" & ClientRefID
        End If
        If PropertyID > 0 Then
            WhereSQL += " AND PropertyID=" & PropertyID
        End If
        If UnitID > 0 Then
            WhereSQL += " AND UnitID=" & UnitID
        End If



        'Conn = DB.OpenConnection()

        cmdCall = New SqlCommand(BaseSQL & WhereSQL, Conn)
        dadCall.SelectCommand = cmdCall
        dadCall.Fill(dstSummary, "SummaryDetails")

        ' Pass DataSet for XSL Transform
        'Dim str As New StringWriter

        'dstSummary.WriteXml(str, XmlWriteMode.IgnoreSchema)
        'Response.Write(str.ToString)

        xmlProducts.Document = New XmlDataDocument(dstSummary)
        xmlProducts.TransformSource = "ContactList.xsl"
    End Sub

End Class
