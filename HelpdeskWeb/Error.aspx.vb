Imports Common
Public Class _Error
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblError As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Request.Params("Error") <> "" Then
            lblError.Text = Request.Params("Error")
        Else
            Select Case GlobalVariables.ErrorMessage
                Case "1"
                    lblError.Text = "The Configuration File Did Not Contain Any Licence Information</br>Please Contact Your Hexagon Representative To Correctly Add This Information To Your Server WebConfig File."
                Case "2"
                    lblError.Text = "The Product Key In The Config File Is Not Valid Or Is Not For This Product.<br/>Please Contact Your Hexagon Representative."
                Case "3"
                    lblError.Text = "Your Licence For This Product Has Expired. Please Contact Hexagon Sales."
                Case "4"
                    lblError.Text = "The Licence Code In The Config File Is Not Valid Please Contact Hexagon Sales."
                Case Else
                    lblError.Text = "An Non-Specified Error Has Occured"
            End Select
        End If
    End Sub

End Class
