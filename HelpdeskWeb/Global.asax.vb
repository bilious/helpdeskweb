﻿Imports System.Web.SessionState
Imports MSFoundationClasses
Imports Common

Public Class Global_asax
    Inherits System.Web.HttpApplication

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application is started
        Dim dtExpires As DateTime
        Dim strLicencee As String = ""
        Dim tKey As String = ""
        Try
            If ConfigurationSettings.AppSettings.Get("ProductCode") Is Nothing Then
                Throw New Exception("1")
            End If
            If ConfigurationSettings.AppSettings.Get("LicenceKey") Is Nothing Then
                Throw New Exception("1")
            End If

            Dim oLix As New Licence("Hexagon", ConfigurationSettings.AppSettings.Get("ProductCode"), 421)
            If oLix.IsValid Then
                tKey = ConfigurationSettings.AppSettings.Get("LicenceKey")
                Dim plainText As String = MSFoundationClasses.KeyEncrypter.Decrypt(CType(tKey, String), "Hexagon", "s@1tValue", "SHA1", 2, "@1B2c3D4e5F6g7H8", 128)
                Dim licence() As String = plainText.Split("-")
                If licence(1) <> 421 Then
                    Throw New Exception("2")
                End If

                dtExpires = CDate(licence(2))
                Dim intyear = dtExpires.Year
                Dim intmonth = dtExpires.Month
                If intyear < Now.Year Then
                    Throw New Exception("3")
                End If
                If intyear = Now.Year Then
                    If intmonth < Now.Month Then
                        Throw New Exception("3")
                    End If
                End If
                strLicencee = licence(0)
            Else
                Throw New Exception("4")
            End If
            GlobalVariables.DBConnStr = ConfigurationSettings.AppSettings.Get("ConnectionString")
            Application.Set("ConnectionString", GlobalVariables.DBConnStr)
            GlobalVariables.HelpDeskExpiryDate = dtExpires
            GlobalVariables.ActiveUserName = strLicencee
            GlobalVariables.ProductCode = ConfigurationSettings.AppSettings.Get("ProductCode")
        Catch ex As Exception
            GlobalVariables.ErrorMessage = ex.Message
        End Try
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session is started
    End Sub

    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires at the beginning of each request
    End Sub

    Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires upon attempting to authenticate the use
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when an error occurs
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session ends
    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application ends
    End Sub

End Class