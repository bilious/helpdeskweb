Imports System.Xml
Imports System.Data.SqlClient
Imports System.Xml.Xsl
Imports System.IO
Imports System.Text

Public Class NewSystemSummary
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.DataSet2 = New System.Data.DataSet
        Me.DataTable1 = New System.Data.DataTable
        Me.DataColumn1 = New System.Data.DataColumn
        Me.DataColumn2 = New System.Data.DataColumn
        Me.DataColumn3 = New System.Data.DataColumn
        Me.DataColumn4 = New System.Data.DataColumn
        CType(Me.DataSet2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataTable1, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'DataSet2
        '
        Me.DataSet2.DataSetName = "NewDataSet"
        Me.DataSet2.Locale = New System.Globalization.CultureInfo("en-GB")
        Me.DataSet2.Tables.AddRange(New System.Data.DataTable() {Me.DataTable1})
        '
        'DataTable1
        '
        Me.DataTable1.Columns.AddRange(New System.Data.DataColumn() {Me.DataColumn1, Me.DataColumn2, Me.DataColumn3, Me.DataColumn4})
        Me.DataTable1.TableName = "Table1"
        '
        'DataColumn1
        '
        Me.DataColumn1.ColumnName = "ClientName"
        '
        'DataColumn2
        '
        Me.DataColumn2.ColumnName = "Title"
        '
        'DataColumn3
        '
        Me.DataColumn3.ColumnName = "FirstName"
        '
        'DataColumn4
        '
        Me.DataColumn4.ColumnName = "LastName"
        CType(Me.DataSet2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataTable1, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Protected WithEvents DataSet2 As System.Data.DataSet
    Protected WithEvents DataTable1 As System.Data.DataTable
    Protected WithEvents DataColumn1 As System.Data.DataColumn
    Protected WithEvents DataColumn2 As System.Data.DataColumn
    Protected WithEvents DataColumn3 As System.Data.DataColumn
    Protected WithEvents DataColumn4 As System.Data.DataColumn
    Protected WithEvents XMLINFO As System.Web.UI.WebControls.Xml
    Protected WithEvents xmlProducts As System.Web.UI.WebControls.Xml

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private oNewSummaryHelper As New NewSummaryHelper
    Dim DataSet1 As New DataSet

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not Page.Request("tag") Is Nothing Then

            Dim oTag As String = Page.Request("tag")
            If oTag = "COMP:0-0-0-0-0" Then
                oTag = "HEX:0-0-0-0-0" ' Crap Fix To Show Summaries Better
            End If
            Dim strSql As String = ""

            Dim schema() As String = Nothing
            Dim params() As Object = Nothing

            Dim oItemType As ItemType = oNewSummaryHelper.GetItemType(oTag, params)


            Dim DBTable_Summary As DataTable = oNewSummaryHelper.CreateSummaryTable(schema)

            If Not oItemType = ItemType.System And Not oItemType = ItemType.Call And Not oItemType = ItemType.Status Then
                Dim DBTable_Info As DataSet = oNewSummaryHelper.CreateInfoTable(oItemType, params)
                XMLINFO.Document = New XmlDataDocument(DBTable_Info)
                If oItemType = ItemType.Asset Then
                    XMLINFO.TransformSource = "AssetInfo.xsl"
                ElseIf oItemType = ItemType.Supplier Then
                    XMLINFO.TransformSource = "SupplierInfo.xsl"
                Else
                    XMLINFO.TransformSource = "BasicInfo.xsl"
                End If

            End If

            Select Case oItemType

                Case ItemType.System
                    strSql = oNewSummaryHelper.SqlForSystem
                Case ItemType.Supplier
                    If params(4) = 0 Then
                        strSql = oNewSummaryHelper.SqlForAllSuppliers
                    Else
                        strSql = oNewSummaryHelper.SqlForSupplier
                    End If

                Case ItemType.Client
                    strSql = oNewSummaryHelper.SqlForClient
                Case ItemType.Property
                    strSql = oNewSummaryHelper.SqlForProperty
                Case ItemType.Unit
                    strSql = oNewSummaryHelper.SqlForUnit
                Case ItemType.Asset
                    strSql = oNewSummaryHelper.SqlForAsset
                Case ItemType.Call
                    Response.Redirect("CallDetails.aspx?CallID=" & params(0))
                Case ItemType.Status
                    Response.Redirect("StatusDetails.aspx?StatusID=" & params(0))
                Case ItemType.Contact
                    Response.Redirect("ContactDetails.aspx?CallID=" & params(4))
            End Select

            If strSql <> "" Then
                Dim strCompoundSql As String = (New StringBuilder).AppendFormat(strSql, params).ToString
                oNewSummaryHelper.ExecuteSummarySql(DBTable_Summary, xmlProducts, schema, strCompoundSql)
            End If

        End If

    End Sub
End Class
