<%@ Page Language="vb" AutoEventWireup="false" Codebehind="SearchResults.aspx.vb" Inherits="HelpdeskWeb.SearchResults"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>SearchResults</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="hexagon.css" type="text/css" rel="stylesheet">
		<style type="text/css">.style1 {
	FONT-WEIGHT: bold; COLOR: #ffffff
}
		</style>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<FONT face="Arial"><IMG src="Hexagon.gif"> </FONT>
			<TABLE id="Table1" height="600" cellSpacing="1" cellPadding="1" width="600" align="center"
				border="0">
				<TR>
					<TD style="HEIGHT: 524px"><FONT face="Arial"></FONT></TD>
					<TD style="BACKGROUND-POSITION: center 50%; BACKGROUND-IMAGE: url(HexagonTable.jpg); BACKGROUND-REPEAT: no-repeat; HEIGHT: 524px"
						vAlign="top" align="center"><asp:xml id="xmlProducts" runat="server" DocumentSource="data.xml" TransformSource="datatable.xsl"></asp:xml>
						<P><FONT face="Arial"></FONT></P>
						<table width="80%" align="left">
							<TR>
								<TD><FONT face="Arial"><STRONG>Clients</STRONG></FONT></TD>
							</TR>
							<TR>
								<TD>
									<DIV align="center"><asp:datagrid id="DataGrid1" runat="server" AutoGenerateColumns="False" Width="500px" CssClass="normal">
											<HeaderStyle Font-Underline="True" Font-Bold="True"></HeaderStyle>
											<Columns>
												<asp:HyperLinkColumn DataNavigateUrlField="tag" DataTextField="ClientID" HeaderText="ClientID"></asp:HyperLinkColumn>
												<asp:BoundColumn DataField="ClientName" HeaderText="ClientName"></asp:BoundColumn>
												<asp:BoundColumn DataField="EXTREF" HeaderText="Reference"></asp:BoundColumn>
												<asp:BoundColumn DataField="Telephone" HeaderText="Telephone"></asp:BoundColumn>
												<asp:BoundColumn DataField="Contact" HeaderText="Contact"></asp:BoundColumn>
												<asp:HyperLinkColumn Text="Click To View" DataNavigateUrlField="tag" DataNavigateUrlFormatString="NewSystemSummary.aspx?tag={0}"></asp:HyperLinkColumn>
											</Columns>
										</asp:datagrid></DIV>
								</TD>
							</TR>
							<TR>
								<TD>&nbsp;</TD>
							</TR>
							<TR>
								<TD><FONT face="Arial"><STRONG>Properties</STRONG></FONT></TD>
							</TR>
							<TR>
								<TD>
									<DIV align="center"><asp:datagrid id="DataGrid2" runat="server" AutoGenerateColumns="False" Width="500px" CssClass="normal">
											<HeaderStyle Font-Underline="True" Font-Bold="True"></HeaderStyle>
											<Columns>
												<asp:HyperLinkColumn DataNavigateUrlField="tag" DataNavigateUrlFormatString="NewSystemSummary.aspx?tag={0}"
													DataTextField="PropertyID" HeaderText="PropertyID"></asp:HyperLinkColumn>
												<asp:BoundColumn DataField="PropertyName" HeaderText="PropertyName"></asp:BoundColumn>
												<asp:BoundColumn DataField="ClientName" HeaderText="Client"></asp:BoundColumn>
												<asp:BoundColumn DataField="Manager" HeaderText="Manager"></asp:BoundColumn>
												<asp:BoundColumn DataField="SectorReference" HeaderText="Sector"></asp:BoundColumn>
												<asp:HyperLinkColumn Text="Click To View" DataNavigateUrlField="tag" DataNavigateUrlFormatString="NewSystemSummary.aspx?tag={0}"></asp:HyperLinkColumn>
											</Columns>
										</asp:datagrid></DIV>
								</TD>
							</TR>
							<TR>
								<TD></TD>
							</TR>
							<TR>
								<TD><FONT face="Arial"><STRONG>Units</STRONG></FONT></TD>
							</TR>
							<TR>
								<TD><asp:datagrid id="Datagrid6" runat="server" AutoGenerateColumns="False" Width="500px" CssClass="normal">
										<HeaderStyle Font-Underline="True" Font-Bold="True"></HeaderStyle>
										<Columns>
											<asp:HyperLinkColumn DataNavigateUrlField="tag" DataNavigateUrlFormatString="NewSystemSummary.aspx?tag={0}"
												DataTextField="UnitID" HeaderText="UnitID"></asp:HyperLinkColumn>
											<asp:BoundColumn DataField="UnitName" HeaderText="UnitName"></asp:BoundColumn>
											<asp:BoundColumn DataField="ClientName" HeaderText="Client"></asp:BoundColumn>
											<asp:BoundColumn DataField="Tenantname" HeaderText="Tenant"></asp:BoundColumn>
											<asp:BoundColumn DataField="TenantTelephone" HeaderText="Phone"></asp:BoundColumn>
											<asp:HyperLinkColumn Text="Click To View" DataNavigateUrlField="tag" DataNavigateUrlFormatString="NewSystemSummary.aspx?tag={0}"></asp:HyperLinkColumn>
										</Columns>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD>&nbsp;</TD>
							</TR>
							<TR>
								<TD><FONT face="Arial"><STRONG>Assets</STRONG></FONT></TD>
							</TR>
							<TR>
								<TD>
									<DIV align="center"><asp:datagrid id="DataGrid3" runat="server" AutoGenerateColumns="False" Width="500px" CssClass="normal">
											<HeaderStyle Font-Underline="True" Font-Bold="True"></HeaderStyle>
											<Columns>
												<asp:HyperLinkColumn DataNavigateUrlField="tag" DataNavigateUrlFormatString="NewSystemSummary.aspx?tag={0}"
													DataTextField="AssetID" HeaderText="AssetID"></asp:HyperLinkColumn>
												<asp:BoundColumn DataField="PropertyName" HeaderText="Property"></asp:BoundColumn>
												<asp:BoundColumn DataField="Description" HeaderText="Description"></asp:BoundColumn>
												<asp:BoundColumn Visible="False" DataField="Manufacturer" HeaderText="Manufacturer"></asp:BoundColumn>
												<asp:BoundColumn DataField="EXTREF" HeaderText="Ref"></asp:BoundColumn>
												<asp:BoundColumn DataField="SupplierName" HeaderText="Supplier"></asp:BoundColumn>
												<asp:HyperLinkColumn Text="Click To View" DataNavigateUrlField="tag" DataNavigateUrlFormatString="NewSystemSummary.aspx?tag={0}"></asp:HyperLinkColumn>
											</Columns>
										</asp:datagrid></DIV>
								</TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 20px">&nbsp;</TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 20px"><span class="style1"><FONT face="Arial" color="#000000">Suppliers</FONT></span></TD>
							</TR>
							<TR>
								<TD>
									<DIV align="center"><asp:datagrid id="DataGrid4" runat="server" AutoGenerateColumns="False" Width="500px" CssClass="normal">
											<HeaderStyle Font-Underline="True" Font-Bold="True"></HeaderStyle>
											<Columns>
												<asp:HyperLinkColumn DataNavigateUrlField="SupplierID" DataNavigateUrlFormatString="NewSystemSummary.aspx?tag=SUP:{0}"
													DataTextField="SupplierID" HeaderText="SupplierID"></asp:HyperLinkColumn>
												<asp:BoundColumn DataField="SupplierName" HeaderText="SupplierName"></asp:BoundColumn>
												<asp:BoundColumn DataField="Category" HeaderText="Category"></asp:BoundColumn>
												<asp:BoundColumn DataField="Contact" HeaderText="Contact"></asp:BoundColumn>
												<asp:HyperLinkColumn Text="Click To View" DataNavigateUrlField="SupplierID" DataNavigateUrlFormatString="NewSystemSummary.aspx?tag=SUP:{0}"></asp:HyperLinkColumn>
											</Columns>
										</asp:datagrid></DIV>
									<FONT face="Arial"></FONT>
								</TD>
							</TR>
							<TR>
								<TD></TD>
							</TR>
							<TR>
								<TD><FONT face="Arial"><STRONG>Contacts</STRONG></FONT></TD>
							</TR>
							<TR>
								<TD>
									<asp:DataGrid id="DataGrid5" runat="server" AutoGenerateColumns="False" Width="500px" CssClass="normal">
										<HeaderStyle Font-Underline="True" Font-Bold="True"></HeaderStyle>
										<Columns>
											<asp:HyperLinkColumn DataNavigateUrlField="ContactID" DataNavigateUrlFormatString="ContactDetails.aspx?ContactID={0}"
												DataTextField="ContactName" HeaderText="Contact"></asp:HyperLinkColumn>
											<asp:BoundColumn DataField="Company" HeaderText="Company"></asp:BoundColumn>
											<asp:BoundColumn DataField="Position" HeaderText="Position"></asp:BoundColumn>
											<asp:BoundColumn DataField="PropertyName" HeaderText="Property"></asp:BoundColumn>
											<asp:HyperLinkColumn Text="Click To View" DataNavigateUrlField="ContactID" DataNavigateUrlFormatString="ContactDetails.aspx?ContactID={0}"></asp:HyperLinkColumn>
										</Columns>
									</asp:DataGrid></TD>
							</TR>
						</table>
					</TD>
					<TD style="HEIGHT: 524px"><FONT face="Arial"></FONT></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
