<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Setup.aspx.vb" Inherits="HelpdeskWeb.Setup"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Setup</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<IMG src="Hexagon.gif">
			<table>
				<TR>
					<TD style="HEIGHT: 21px">Stage 1</TD>
					<TD style="HEIGHT: 21px">(Database Creation)</TD>
					<TD style="HEIGHT: 21px"></TD>
					<TD style="HEIGHT: 21px"></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD>SQL SA UserName</TD>
					<TD><asp:textbox id="txtSAName" runat="server"></asp:textbox></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD>SQL SA Password</TD>
					<TD><asp:textbox id="txtSAPassword" runat="server" TextMode="Password"></asp:textbox></TD>
					<TD></TD>
				</TR>
				<tr>
					<td></td>
					<TD>Helpdesk DB Name:
					</TD>
					<TD><asp:textbox id="txtdbName" runat="server">HexagonHelpdesk</asp:textbox></TD>
					<td></td>
				</tr>
				<TR>
					<TD></TD>
					<TD>Helpdesk Account UserName</TD>
					<TD><asp:textbox id="txtUser" runat="server">Helpdesk</asp:textbox></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD>Helpdesk Account Password</TD>
					<TD><asp:textbox id="txtPassword" runat="server" TextMode="Password"></asp:textbox></TD>
					<TD><asp:imagebutton id="ImageButton1" runat="server" ImageUrl="images\arrow.gif"></asp:imagebutton></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD colSpan="2">
						<P><asp:label id="lblStatus" runat="server" ForeColor="Red"></asp:label>
							<asp:label id="lblInfo" runat="server" ForeColor="Blue"></asp:label></P>
					</TD>
					<TD></TD>
				</TR>
				<tr>
					<td>Stage 2</td>
					<TD>(Table&nbsp;&amp; View Creation)</TD>
					<TD></TD>
					<td><asp:imagebutton id="Imagebutton2" runat="server" ImageUrl="images\arrow.gif"></asp:imagebutton></td>
				</tr>
				<tr>
					<td>Stage 3</td>
					<TD></TD>
					<TD></TD>
					<td><asp:imagebutton id="Imagebutton3" runat="server" ImageUrl="images\arrow.gif"></asp:imagebutton></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
