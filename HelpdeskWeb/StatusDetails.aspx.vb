Imports System.Xml
Imports System.Data.SqlClient
Imports System.Xml.Xsl
Imports System.Text
Imports System.Data
Imports DataAccessor


Public Class StatusDetails
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents xmlProducts As System.Web.UI.WebControls.Xml

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private oDataAccessor As New DataAccessorLayer.DataAccessor
    Private oNewSummaryHelper As New NewSummaryHelper

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'If Not Page.Request("CallID") Is Nothing Then

        '    Dim intCallID As Int32 = Int32.Parse(Page.Request("CallID"))

        '    Dim strCompoundSql As String = (New StringBuilder).AppendFormat(oNewSummaryHelper.SqlForCall, intCallID).ToString
        '    Dim dstSummary As DataSet = oDataAccessor.SelectQuery(strCompoundSql)


        '    dstSummary.DataSetName = "SummaryDetails"
        '    xmlProducts.Document = New XmlDataDocument(dstSummary)
        '    xmlProducts.TransformSource = "CallSummary.xsl"

        'End If


        If Not Request("StatusID") Is Nothing Then
            Dim intStatusID = Int32.Parse(Request("StatusID"))
            Dim CurrentTag As String = ""
            Dim dadCall As New SqlDataAdapter
            Dim dstSummary As New DataSet
            Dim cmdCall As SqlCommand
            'Dim DB As New DataBase
            Dim Conn As SqlConnection
            Dim strCompoundSql As String = "SELECT "
            oDataAccessor.TakeConnection(Conn)
            Dim myAges() As String
            If Not Request("Age") Is Nothing Then
                myAges = Request("Age").Split(":")

                ' Dim statusData(1) As String
                ' statusData(0) = "Last 24 Hours|0|24"
                ' statusData(1) = "Last 2 Days|24|48"

                For i = 0 To myAges.GetUpperBound(0)
                    Dim tStatus = myAges(i).ToString.Split("|")
                    strCompoundSql = strCompoundSql & intStatusID & " As StatusID" & i & ", '" & tStatus(0) & "' as desc" & i & ",'" & myAges(i) & "' As Age" & i & ",(select COUNT(*) AS Value From tbl_incidence where statusid = " & intStatusID & " and DATEDIFF(hour, OpenSince, getdate()) < " & tStatus(2) & " And DATEDIFF(hour, OpenSince, getdate()) > " & tStatus(1) & " ) 'Count" & i & "',"
                    '"'Last 24 Hours' as desc1,(select COUNT(*) AS Value From tbl_incidence where DATEDIFF(hour, CloseDate, getdate()) < 24) 'Count1', " & _
                    '"'Last 2 Days' as desc2,(select COUNT(*) AS Value From tbl_incidence where DATEDIFF(hour, CloseDate, getdate()) < 48 And DATEDIFF(hour, CloseDate, getdate()) > 24 ) 'Count2'," & _
                Next
                strCompoundSql = strCompoundSql & "'BillField' As Pointless "

            Else
                'This Data May Be Available But Not Passed In So We Need To Try & Look It Up
                Dim strAgeRange As String = "Last 24 Hours|0|24 :Last 2 Days|24|48"
                Try
                    strAgeRange = oDataAccessor.ScalarQuery("Select AgeRangeConfig from Config_Status where StatusID = " & intStatusID & " And Affects = 'CALL'")
                Catch
                End Try
                If strAgeRange = "" Then
                    'Non-Supplied DataLookup Failed
                    strCompoundSql = strCompoundSql & intStatusID & " As StatusID0, 'All Calls ' as desc0 , '' As Age0,(select COUNT(*) AS Value From tbl_incidence where statusid = " & intStatusID & ") 'Count0'"
                Else
                    myAges = strAgeRange.Split(":")

                    ' Dim statusData(1) As String
                    ' statusData(0) = "Last 24 Hours|0|24"
                    ' statusData(1) = "Last 2 Days|24|48"

                    For i = 0 To myAges.GetUpperBound(0)
                        Dim tStatus = myAges(i).ToString.Split("|")
                        strCompoundSql = strCompoundSql & intStatusID & " As StatusID" & i & ", '" & tStatus(0) & "' as desc" & i & ",'" & myAges(i) & "' As Age" & i & ",(select COUNT(*) AS Value From tbl_incidence where statusid = " & intStatusID & " and DATEDIFF(hour, OpenSince, getdate()) < " & tStatus(2) & " And DATEDIFF(hour, OpenSince, getdate()) > " & tStatus(1) & " ) 'Count" & i & "',"
                        '"'Last 24 Hours' as desc1,(select COUNT(*) AS Value From tbl_incidence where DATEDIFF(hour, CloseDate, getdate()) < 24) 'Count1', " & _
                        '"'Last 2 Days' as desc2,(select COUNT(*) AS Value From tbl_incidence where DATEDIFF(hour, CloseDate, getdate()) < 48 And DATEDIFF(hour, CloseDate, getdate()) > 24 ) 'Count2'," & _
                    Next
                    strCompoundSql = strCompoundSql & "'BillField' As Pointless "

                End If
                End If
                cmdCall = New SqlCommand(strCompoundSql, Conn)
                dadCall.SelectCommand = cmdCall
                dadCall.Fill(dstSummary, "SummaryDetails")

                xmlProducts.Document = New XmlDataDocument(dstSummary)
                xmlProducts.TransformSource = "StatusSummary.xsl"


        End If

    End Sub

End Class
