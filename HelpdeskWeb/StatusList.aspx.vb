Imports System.Xml
Imports System.Data.SqlClient
Imports System.Xml.Xsl
Imports System.IO

Public Class StatusList
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents xmlProducts As System.Web.UI.WebControls.Xml
    Protected WithEvents lblWarning As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private strAge As String = ""
    Private intStatusID As Int32 = 0
    Private BaseSQL, WhereSQL, FinalSQL As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        '--------------------------------------------------------------------------
        strAge = Request("Age")
        intStatusID = Request("StatusID")

        Dim tStatus = strAge.Split("|")
        If strAge = "" Then
            BaseSQL = "select * From View_Incidence Where StatusID = " & intStatusID

        Else

            BaseSQL = "select * From View_Incidence Where StatusID = " & intStatusID & " And (DATEDIFF(hour, OpenSince, getdate()) < " & tStatus(2) & " And DATEDIFF(hour, OpenSince, getdate()) > " & tStatus(1) & " ) "

        End If

        '--------------------------------------------------------------------
        Dim dadCall As New SqlDataAdapter
        Dim dstSummary As New DataSet
        Dim cmdCall As SqlCommand
        'Dim DB As New DataBase
        Dim Conn As SqlConnection
        Dim oDataAccessor As New DataAccessorLayer.DataAccessor
        oDataAccessor.TakeConnection(Conn)
        'Conn = DB.OpenConnection()
        FinalSQL = BaseSQL + " ORDER BY OpenSince"

        cmdCall = New SqlCommand(FinalSQL, Conn)
        dadCall.SelectCommand = cmdCall
        dadCall.Fill(dstSummary, "SummaryDetails")

        ' Pass DataSet for XSL Transform
        'Dim str As New StringWriter

        'dstSummary.WriteXml(str, XmlWriteMode.IgnoreSchema)
        'Response.Write(FinalSQL)
        If dstSummary.Tables(0).Rows.Count = 0 Then
            lblWarning.Text = "There Are No Records To Display"
            xmlProducts.Visible = False
        Else
            lblWarning.Visible = False
            xmlProducts.Document = New XmlDataDocument(dstSummary)
            xmlProducts.TransformSource = "CallList.xsl"

        End If

    End Sub

End Class
