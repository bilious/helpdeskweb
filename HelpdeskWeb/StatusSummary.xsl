
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="SummaryDetails">
  <center>
    <xsl:variable name="Age0">
      <xsl:value-of select="Age0"/>
    </xsl:variable>
    <xsl:variable name="Status0">
      <xsl:value-of select="StatusID0"/>
    </xsl:variable>
    <xsl:variable name="Age1">
      <xsl:value-of select="Age1"/>
    </xsl:variable>
    <xsl:variable name="Status1">
      <xsl:value-of select="StatusID1"/>
    </xsl:variable>
    <xsl:variable name="Age2">
      <xsl:value-of select="Age2"/>
    </xsl:variable>
    <xsl:variable name="Status2">
      <xsl:value-of select="StatusID2"/>
    </xsl:variable>
    <xsl:variable name="Age3">
      <xsl:value-of select="Age3"/>
    </xsl:variable>
    <xsl:variable name="Status3">
      <xsl:value-of select="StatusID3"/>
    </xsl:variable>
    <xsl:variable name="Age4">
      <xsl:value-of select="Age4"/>
    </xsl:variable>
    <xsl:variable name="Status4">
      <xsl:value-of select="StatusID4"/>
    </xsl:variable>
    <xsl:variable name="Age5">
      <xsl:value-of select="Age5"/>
    </xsl:variable>
    <xsl:variable name="Status5">
      <xsl:value-of select="StatusID5"/>
    </xsl:variable>
    <table width="70%">
    <tr>
      <td align="center" class="summarytitle">Call Status Summary </td>
    </tr>
    </table>
    <br/>
    <table width="70%" 
      cellpadding="1" cellspacing="0" class="de_table">
     
      <tr>
        <TD>
          <xsl:value-of select="desc0" />
        </TD>
        <td colspan="2">
          <p><a href="StatusList.aspx?Age={$Age0}&amp;StatusID={$Status0}">
            <xsl:value-of select="Count0" />
          </a>
          <br/>
            <P></P>
          </p>
        </td>
        
      </tr>
       <tr>
         <TD>
           <xsl:value-of select="desc1" />
         </TD>
         <td colspan="2">
           <p><a href="StatusList.aspx?Age={$Age1}&amp;StatusID={$Status1}">
             <xsl:value-of select="Count1" />
           </a>
           <br/>
           <P></P>
           </p>
         </td>
        
       </tr>
       <tr>
         <TD>
           <xsl:value-of select="desc2" />
         </TD>
         <td colspan="2">
           <p> <a href="StatusList.aspx?Age={$Age2}&amp;StatusID={$Status2}">
             <xsl:value-of select="Count2" />
           </a>
           <br/>
           <P></P>
           </p>
         </td>
        
       </tr>
      <tr>
        <TD>
          <xsl:value-of select="desc3" />
        </TD>
        <td colspan="2">
          <p> <a href="StatusList.aspx?Age={$Age3}&amp;StatusID={$Status3}">
            <xsl:value-of select="Count3" />
          </a>
          <br/>
          <P></P>
          </p>
        </td>
       
      </tr>
      <tr>
        <TD>
          <xsl:value-of select="desc4" />
        </TD>
        <td colspan="2">
          <p><a href="StatusList.aspx?Age={$Age4}&amp;StatusID={$Status4}">
            <xsl:value-of select="Count4" />
          </a>
          <br/>
          <P></P>
          </p>
        </td>
       
      </tr>
      <tr>
        <TD>
          <xsl:value-of select="desc5" />
        </TD>
        <td colspan="2">
          <p> <a href="StatusList.aspx?Age={$Age5}&amp;StatusID={$Status5}">
            <xsl:value-of select="Count5" />
          </a>
          <br/>
          <P></P>
          </p>
        </td>
        
      </tr>
       
       
   </table>
      <table width="70%" class="normal"  align="center">
   <tr><td>
     <p align="right">
     <table>
     <tr><td><a href="javascript:history.go(-1)"><img src="backbutton.jpg" border="0"></img> </a></td>
     <td></td></tr>
     </table>
     </p></td></tr>
   </table>

  </center>	
  </xsl:template>
 </xsl:stylesheet>
