
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  >
  <xsl:template match="/">
  
  <xsl:for-each select="ReturnDataSet">
  <xsl:for-each select="Table">
  <table border="0" width="60%" class="HTable"><tr><td class="summarytitle">Supplier Details</td></tr>
  <tr><td>
  <table border="0" align="center" width="100%">
  <tr class="normal">
	<td >Name</td><td><xsl:value-of select="SupplierName"/></td>
  </tr>
    <tr class="normal">
  <td>Category</td><td><xsl:value-of select="Category"/></td>
  </tr>
    <tr class="normal">
	<td >Reference</td><td><xsl:value-of select="AccountCode"/></td>
  </tr>
  <tr class="normal">
  <td valign="Top">Address</td><td><xsl:if test="Address1 != ''"> 
				 <xsl:value-of select="Address1"/>,&#160;
  </xsl:if>	
    <xsl:if test="Address2 != ''"> 
    <xsl:value-of select="Address2"/>,&#160;
     </xsl:if>	
      <xsl:if test="Address3 != ''"> 
  <xsl:value-of select="Address3"/>,&#160;<br />
   </xsl:if>	
    <xsl:if test="City != ''"> 
  <xsl:value-of select="City"/>,&#160;
   </xsl:if>	
    <xsl:if test="Postcode != ''"> 
  <xsl:value-of select="Postcode"/>,&#160;
   </xsl:if>	</td>
  </tr>
  <tr class="normal">
  <td>Telephone</td><td><xsl:value-of select="SupplierTelephone"/></td>
  </tr>
   </table>
  </td></tr></table>
</xsl:for-each>
  </xsl:for-each>
 
  </xsl:template>
 </xsl:stylesheet>
