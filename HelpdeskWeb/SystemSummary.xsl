<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
 <xsl:key name="group-by-type" match="SummaryDetails" use="Type" />
 <xsl:template match="NewDataSet">
 
  <table border="0" align="center" width="60%" >
   <xsl:for-each select="SummaryDetails[count(. | key('group-by-type', Type)[1]) = 1]">
    <!-- <xsl:sort select="type" /> -->
    <tr><td colspan="2" class="summarytitle"><xsl:value-of select="Type" /></td></tr>
    <tr><td><table width="100%" align="center">
    <xsl:for-each select="key('group-by-type', Type)">
     <tr>
     <td class="normal" width="70%"><xsl:value-of select="Description"/></td>
     <td  class="normal" width="30%">
      <xsl:variable name="FromID">
		<xsl:value-of select="FromID"/>
			</xsl:variable>
     <xsl:variable name="SummType">
		<xsl:value-of select="SummType"/>
	</xsl:variable> 
     <xsl:variable name="ClientID">
		<xsl:value-of select="ClientID"/>
	</xsl:variable> 
     <xsl:variable name="PropertyID">
		<xsl:value-of select="PropertyID"/>
	</xsl:variable> 
     <xsl:variable name="UnitID">
		<xsl:value-of select="UnitID"/>
	</xsl:variable> 
     <xsl:variable name="AssetID">
		<xsl:value-of select="AssetID"/>
	</xsl:variable> 
	 <xsl:choose>
     	<xsl:when test="Description='New'">
     		<xsl:if test="Value &gt; 0">
				<a href="CallList.aspx?FromID={$FromID}&amp;SummaryID=1&amp;SummType={$SummType}&amp;ClientID={$ClientID}&amp;PropertyID={$PropertyID}&amp;UnitID={$UnitID}&amp;AssetID={AssetID}"><xsl:value-of select="Value"/></a>
			</xsl:if>				
			<xsl:if test="Value=0">
				<xsl:value-of select="Value"/> 
			</xsl:if>				
		</xsl:when>

     	<xsl:when test="Description='Awaiting Budget'">
     		<xsl:if test="Value &gt; 0">
				<a href="CallList.aspx?FromID={$FromID}&amp;SummaryID=2&amp;SummType={$SummType}&amp;ClientID={$ClientID}&amp;PropertyID={$PropertyID}&amp;UnitID={$UnitID}&amp;AssetID={AssetID}"><xsl:value-of select="Value"/></a>
			</xsl:if>				
			<xsl:if test="Value=0">
				<xsl:value-of select="Value"/> 
			</xsl:if>				
		</xsl:when>
		
     	<xsl:when test="Description='PO SignOff Required'">
     		<xsl:if test="Value &gt; 0">
				<a href="CallList.aspx?FromID={$FromID}&amp;SummaryID=3&amp;SummType={$SummType}&amp;ClientID={$ClientID}&amp;PropertyID={$PropertyID}&amp;UnitID={$UnitID}&amp;AssetID={AssetID}"><xsl:value-of select="Value"/></a>
			</xsl:if>				
			<xsl:if test="Value=0">
				<xsl:value-of select="Value"/> 
			</xsl:if>				
		</xsl:when>
		
		     	<xsl:when test="Description='Awaiting Parts'">
     		<xsl:if test="Value &gt; 0">
				<a href="CallList.aspx?FromID={$FromID}&amp;SummaryID=6&amp;SummType={$SummType}&amp;ClientID={$ClientID}&amp;PropertyID={$PropertyID}&amp;UnitID={$UnitID}&amp;AssetID={AssetID}"><xsl:value-of select="Value"/></a>
			</xsl:if>				
			<xsl:if test="Value=0">
				<xsl:value-of select="Value"/> 
			</xsl:if>				
		</xsl:when>
		
     	<xsl:when test="Description='Scheduled Calls'">
     		<xsl:if test="Value &gt; 0">
				<a href="CallList.aspx?FromID={$FromID}&amp;SummaryID=4&amp;SummType={$SummType}&amp;ClientID={$ClientID}&amp;PropertyID={$PropertyID}&amp;UnitID={$UnitID}&amp;AssetID={AssetID}"><xsl:value-of select="Value"/></a>
			</xsl:if>				
			<xsl:if test="Value=0">
				<xsl:value-of select="Value"/> 
			</xsl:if>				
		</xsl:when>
		
<xsl:when test="Description='In Progress'">
     		<xsl:if test="Value &gt; 0">
				<a href="CallList.aspx?FromID={$FromID}&amp;SummaryID=4&amp;SummType={$SummType}&amp;ClientID={$ClientID}&amp;PropertyID={$PropertyID}&amp;UnitID={$UnitID}&amp;AssetID={AssetID}"><xsl:value-of select="Value"/></a>
			</xsl:if>				
			<xsl:if test="Value=0">
				<xsl:value-of select="Value"/> 
			</xsl:if>				
		</xsl:when>
		
<xsl:when test="Description='Planned'">
     		<xsl:if test="Value &gt; 0">
				<a href="CallList.aspx?FromID={$FromID}&amp;SummaryID=7&amp;SummType={$SummType}&amp;ClientID={$ClientID}&amp;PropertyID={$PropertyID}&amp;UnitID={$UnitID}&amp;AssetID={AssetID}"><xsl:value-of select="Value"/></a>
			</xsl:if>				
			<xsl:if test="Value=0">
				<xsl:value-of select="Value"/> 
			</xsl:if>				
		</xsl:when>
		
     	<xsl:when test="Description='In Last 24 Hours'">
     		<xsl:if test="Value &gt; 0">
				<a href="CallList.aspx?FromID={$FromID}&amp;SummaryID=5&amp;SummType={$SummType}&amp;ClientID={$ClientID}&amp;PropertyID={$PropertyID}&amp;UnitID={$UnitID}&amp;AssetID={AssetID}"><xsl:value-of select="Value"/></a>
			</xsl:if>				
			<xsl:if test="Value=0">
				<xsl:value-of select="Value"/> 
			</xsl:if>				
		</xsl:when>
     	<xsl:when test="Description='Total Closed Calls'">
     		<xsl:if test="Value &gt; 0">
				<a href="CallList.aspx?FromID={$FromID}&amp;SummaryID=5&amp;SummType={$SummType}&amp;ClientID={$ClientID}&amp;PropertyID={$PropertyID}&amp;UnitID={$UnitID}&amp;AssetID={AssetID}"><xsl:value-of select="Value"/></a>
			</xsl:if>				
			<xsl:if test="Value=0">
				<xsl:value-of select="Value"/> 
			</xsl:if>				
		</xsl:when>


     	<xsl:when test="Description='Total Calls'">
  	     	<xsl:if test="Value &gt; 0">
				<a href="CallList.aspx?FromID={$FromID}&amp;SummaryID=7&amp;SummType={$SummType}&amp;ClientID={$ClientID}&amp;PropertyID={$PropertyID}&amp;UnitID={$UnitID}&amp;AssetID={AssetID}"><xsl:value-of select="Value"/></a>
			</xsl:if>				
			<xsl:if test="Value=0">
				<xsl:value-of select="Value"/> 
			</xsl:if>	
						
		</xsl:when>
		    	<xsl:when test="Description='Contacts'">
  	     	<xsl:if test="Value &gt; 0">
				<a href="ContactList.aspx?FromID={$FromID}&amp;SummaryID=7&amp;SummType={$SummType}&amp;ClientID={$ClientID}&amp;PropertyID={$PropertyID}&amp;UnitID={$UnitID}&amp;AssetID={AssetID}"><xsl:value-of select="Value"/></a>
			</xsl:if>				
			<xsl:if test="Value=0">
				<xsl:value-of select="Value"/> 
			</xsl:if>				
		</xsl:when>
				
		<xsl:otherwise>
			<xsl:value-of select="Value"/> 
		</xsl:otherwise>
		</xsl:choose>
		</td>
     </tr>
    </xsl:for-each>
    </table><br/></td></tr>
   </xsl:for-each>
  </table>
 </xsl:template>
</xsl:stylesheet>
 
