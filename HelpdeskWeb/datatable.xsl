<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/ClientSummary">
      <xsl:apply-templates select="SummaryType" />
  </xsl:template>

  <xsl:template match="SummaryType">
    <table width="50%" border="0" class="normal">
      <tr>
        <td width="70%">
          <xsl:value-of select="Name/CallType" />
        </td>
        <td width="30%" align="right">
          <xsl:value-of select="Name/TotalValue" />
        </td>
        
      </tr>
    </table>
  </xsl:template>

</xsl:stylesheet>

