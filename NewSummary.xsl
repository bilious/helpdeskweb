<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
 <xsl:key name="group-by-groupname" match="SummaryDetails" use="GroupName" />
 <xsl:template match="NewDataSet">
 
  <table border="0" align="center" width="70%">
   <xsl:for-each select="SummaryDetails[count(. | key('group-by-groupname', GroupName)[1]) = 1]">
    <!-- <xsl:sort select="GroupName" /> -->
    <tr><td colspan="2" class="summarytitle"><xsl:value-of select="GroupName" /></td></tr>
    <tr><td><table width="100%" CLASS="de_table">
    <xsl:for-each select="key('group-by-groupname', GroupName)">
     <tr>
     <td class="normal" width="30%"><xsl:value-of select="Type"/>: </td>
     <td class="normal" width="70%">
     <xsl:variable name="IDValue">
		<xsl:value-of select="CID"/>
	</xsl:variable> 
        <!-- <xsl:if test="GroupName='Properties'"> 
			<a href=""><xsl:value-of select="Description"/></a>
		</xsl:if> -->
		 <xsl:choose>
			<xsl:when test="Type='Client'">
				<a href="SystemSummary.aspx?SummaryID=2&amp;ClientRefID={$IDValue}"><xsl:value-of select="Description"/></a>
			</xsl:when>
			<xsl:when test="GroupName='Call'">
				<a href="CallDetails.aspx?CallID={$IDValue}"><xsl:value-of select="Description"/></a>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="Description"/>
			</xsl:otherwise>
		</xsl:choose>
		
     </td>
     </tr>
    </xsl:for-each>
    </table><br/></td></tr>
   </xsl:for-each>
  </table>
 </xsl:template>
</xsl:stylesheet>
 
