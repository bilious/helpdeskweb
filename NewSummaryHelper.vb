Imports System.Xml
Imports System.Data
Imports System.Data.SqlClient


Public Enum ItemType
    System
    Client
    [Property]
    Unit
    Asset
    File
    Supplier
    Contact
    [Call]
    Unknown
End Enum

Public Class NewSummaryHelper

    Private oDataAccessor As New DataAccessor

    'Public SqlForSystem As String = "" & _
    '"select 'OPEN CALLS' AS Type, 'New Calls' AS Description,COUNT(*) AS Value,1 as SummType,0 as ClientID,0 As PropertyID,0 as UnitID From tbl_incident where StatusID=1;" & _
    '"select 'OPEN CALLS' AS Type, 'Assigned To Engineer' AS Description,COUNT(*) AS Value,2 as SummType,0 as ClientID,0 As PropertyID,0 as UnitID From tbl_incident where StatusID=2 And SupplierID>0;" & _
    '"select 'OPEN CALLS' AS Type, 'Awaiting Parts' AS Description,COUNT(*) AS Value,3 as SummType,0 as ClientID,0 As PropertyID,0 as UnitID From tbl_incident where StatusID=3;" & _
    '"select 'OPEN CALLS' AS Type, 'Scheduled Calls' AS Description,COUNT(*) AS Value,4 as SummType,0 as ClientID,0 As PropertyID,0 as UnitID From tbl_incident where CallTypeID=2;" & _
    '"select 'CLOSED CALLS' AS Type, 'In Last 24 Hours' AS Description,COUNT(*) AS Value,5 as SummType,0 as ClientID,0 As PropertyID,0 as UnitID From tbl_incident where DATEDIFF(hour, CloseDate, getdate()) < 24;" & _
    '"select 'CLOSED CALLS' AS Type, 'Within SLAs' AS Description,COUNT(*) AS Value,6 as SummType,0 as ClientID,0 As PropertyID,0 as UnitID From tbl_incident where StatusID=1;" & _
    '"select 'CLOSED CALLS' AS Type, 'Outside SLAs' AS Description,COUNT(*) AS Value,7 as SummType,0 as ClientID,0 As PropertyID,0 as UnitID From tbl_incident where StatusID=1;" & _
    '"select 'PAYMENTS' AS Type, 'Total Value Parts On Order' AS Description,SUM(CostPrice) AS Value,8 as SummType,0 as ClientID,0 As PropertyID,0 as UnitID From tbl_POrder where OrderStatusID != 5;"

    'Public SqlForSystem As String = "" & _
'    "select 'OPEN CALLS' AS Type, 'New Calls' AS Description,COUNT(*) AS Value,0 As FromID,1 as SummType,0 as ClientID,0 As PropertyID,0 as UnitID, 0 as AssetID From tbl_incidence where StatusID=1;" & _
    '    "select 'OPEN CALLS' AS Type, 'Awaiting Budget' AS Description,COUNT(*) AS Value,0 As FromID,2 as SummType,0 as ClientID,0 As PropertyID,0 as UnitID , 0 as AssetID From tbl_incidence where StatusID=2 ;" & _
    '    "select 'OPEN CALLS' AS Type, 'PO Sign Off Required' AS Description,COUNT(*) AS Value,0 As FromID,3 as SummType,0 as ClientID,0 As PropertyID,0 as UnitID, 0 as AssetID From tbl_incidence where StatusID=3;" & _
    '    "select 'OPEN CALLS' AS Type, 'In Progress' AS Description,COUNT(*) AS Value,0 As FromID,4 as SummType,0 as ClientID,0 As PropertyID,0 as UnitID, 0 as AssetID From tbl_incidence where StatusID=4;" & _
    ' "select 'OPEN CALLS' AS Type, 'Scheduled Calls' AS Description,COUNT(*) AS Value,0 As FromID,10 as SummType,0 as ClientID,0 As PropertyID,0 as UnitID, 0 as AssetID From tbl_incidence where CallTypeID=2;" & _
    '"select 'OPEN CALLS' AS Type, 'Postponed Calls' AS Description,COUNT(*) AS Value,0 As FromID,6 as SummType,0 as ClientID,0 As PropertyID,0 as UnitID , 0 as AssetID From tbl_incidence where StatusID=6;" & _
    '"select 'OPEN CALLS' AS Type, 'Project' AS Description,COUNT(*) AS Value,0 As FromID,7 as SummType,0 as ClientID,0 As PropertyID,0 as UnitID, 0 as AssetID FRom tbl_incidence where StatusID=7;" & _
    '   "select 'CLOSED CALLS' AS Type, 'In Last 24 Hours' AS Description,COUNT(*) AS Value,0 As FromID,5 as SummType,0 as ClientID,0 As PropertyID,0 as UnitID, 0 as AssetID From tbl_incidence where DATEDIFF(hour, ClosedAt, getdate()) < 24;"

    Public SqlForSystem As String = "" & _
    "SELECT     Description, 'OPEN CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 1 AS SummType, 0 AS ClientID, 0 AS PropertyID, 0 AS UnitID, 0 AS AssetID " & _
    "FROM        View_WebSummary_Totals WHERE (StatusID = 1) GROUP BY Description;" & _
       "SELECT  Description,    'OPEN CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 2 AS SummType, 0 AS ClientID, 0 AS PropertyID, 0 AS UnitID, 0 AS AssetID " & _
    "FROM        View_WebSummary_Totals WHERE (StatusID = 2) GROUP BY Description;" & _
       "SELECT   Description,   'OPEN CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 3 AS SummType, 0 AS ClientID, 0 AS PropertyID, 0 AS UnitID, 0 AS AssetID " & _
    "FROM        View_WebSummary_Totals WHERE (StatusID = 3) GROUP BY Description;" & _
       "SELECT   Description,   'OPEN CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 4 AS SummType, 0 AS ClientID, 0 AS PropertyID, 0 AS UnitID, 0 AS AssetID " & _
    "FROM        View_WebSummary_Totals WHERE (StatusID = 4) GROUP BY Description;" & _
       "SELECT   Description,   'OPEN CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 6 AS SummType, 0 AS ClientID, 0 AS PropertyID, 0 AS UnitID, 0 AS AssetID " & _
    "FROM        View_WebSummary_Totals WHERE (StatusID = 6) GROUP BY Description;" & _
       "SELECT   Description,   'OPEN CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 7 AS SummType, 0 AS ClientID, 0 AS PropertyID, 0 AS UnitID, 0 AS AssetID " & _
    "FROM        View_WebSummary_Totals WHERE (StatusID = 7) GROUP BY Description;" & _
       "SELECT   Description,   'OPEN CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 8 AS SummType, 0 AS ClientID, 0 AS PropertyID, 0 AS UnitID, 0 AS AssetID " & _
    "FROM        View_WebSummary_Totals WHERE (StatusID = 8) GROUP BY Description;" & _
       "SELECT   Description,   'OPEN CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 9 AS SummType, 0 AS ClientID, 0 AS PropertyID, 0 AS UnitID, 0 AS AssetID " & _
    "FROM        View_WebSummary_Totals WHERE (StatusID = 9) GROUP BY Description;" & _
    "select 'CLOSED CALLS' AS Type, 'In Last 24 Hours' AS Description,COUNT(*) AS Value,0 As FromID,5 as SummType,0 as ClientID,0 As PropertyID,0 as UnitID, 0 as AssetID From tbl_incidence where DATEDIFF(hour, ClosedAt, getdate()) < 24; " & _
 "select 'CLOSED CALLS' AS Type, 'Total Closed Calls' AS Description,COUNT(*) AS Value,0 As FromID,5 as SummType,0 as ClientID,0 As PropertyID,0 as UnitID, 0 as AssetID From tbl_incidence Where StatusID=5"

    Public SqlForClient As String = "" & _
    "select 'Contacts' As Type, 'Contacts' As Description, Count(*) As Value, 0 As FromID,0 as SummType, {0} as ClientID, 0 as PropertyID, 0 As UnitID, 0 as AssetID From tbl_ContactLink Where ClientID={0};" & _
    "select 'Properties' AS Type, 'Properties' AS Description,COUNT(*) AS Value,0 As FromID,21 as SummType,{0} as ClientID,0 as PropertyID,0 as UnitID, 0 as AssetID From View_Property_Ext where ClientID={0} AND HelpdeskStatus != 0;" & _
    "select 'Properties' AS Type, 'Units' AS Description,COUNT(*) AS Value,0 As FromID,22 as SummType,{0} as ClientID,0 as PropertyID,0 as UnitID, 0 as AssetID From View_Unit_Ext where ClientID={0} And HelpdeskStatus != 0;" & _
 "SELECT     Description, 'OPEN CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 1 AS SummType, {0} AS ClientID, 0 AS PropertyID, 0 AS UnitID, 0 AS AssetID " & _
    "FROM        View_WebSummary_Totals WHERE (StatusID = 1) AND ClientID ={0} GROUP BY Description;" & _
       "SELECT  Description,    'OPEN CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 2 AS SummType, {0} AS ClientID, 0 AS PropertyID, 0 AS UnitID, 0 AS AssetID " & _
    "FROM        View_WebSummary_Totals WHERE (StatusID = 2) AND ClientID ={0} GROUP BY Description;" & _
       "SELECT   Description,   'OPEN CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 3 AS SummType, {0} AS ClientID, 0 AS PropertyID, 0 AS UnitID, 0 AS AssetID " & _
    "FROM        View_WebSummary_Totals WHERE (StatusID = 3) AND ClientID ={0} GROUP BY Description;" & _
       "SELECT   Description,   'OPEN CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 4 AS SummType, {0} AS ClientID, 0 AS PropertyID, 0 AS UnitID, 0 AS AssetID " & _
    "FROM        View_WebSummary_Totals WHERE (StatusID = 4) AND ClientID ={0} GROUP BY Description;" & _
       "SELECT   Description,   'OPEN CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 6 AS SummType, {0} AS ClientID, 0 AS PropertyID, 0 AS UnitID, 0 AS AssetID " & _
    "FROM        View_WebSummary_Totals WHERE (StatusID = 6) AND ClientID ={0} GROUP BY Description;" & _
       "SELECT   Description,   'OPEN CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 7 AS SummType, {0} AS ClientID, 0 AS PropertyID, 0 AS UnitID, 0 AS AssetID " & _
    "FROM        View_WebSummary_Totals WHERE (StatusID = 7) AND ClientID ={0} GROUP BY Description;" & _
       "SELECT   Description,   'OPEN CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 8 AS SummType, {0} AS ClientID, 0 AS PropertyID, 0 AS UnitID, 0 AS AssetID " & _
    "FROM        View_WebSummary_Totals WHERE (StatusID = 8) AND ClientID ={0} GROUP BY Description;" & _
       "SELECT   Description,   'OPEN CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 9 AS SummType, {0} AS ClientID, 0 AS PropertyID, 0 AS UnitID, 0 AS AssetID " & _
    "FROM        View_WebSummary_Totals WHERE (StatusID = 9) AND ClientID ={0} GROUP BY Description;" & _
    "select 'CLOSED CALLS' AS Type, 'In Last 24 Hours' AS Description,COUNT(*) AS Value,0 As FromID,5 as SummType,{0} as ClientID,0 As PropertyID,0 as UnitID, 0 as AssetID From View_WebSummary_Totals where ClientID ={0} AND DATEDIFF(hour, ClosedAt, getdate()) < 24; " & _
 "select 'CLOSED CALLS' AS Type, 'Total Closed Calls' AS Description,COUNT(*) AS Value,0 As FromID,5 as SummType,{0} as ClientID,0 As PropertyID,0 as UnitID, 0 as AssetID From View_WebSummary_Totals Where StatusID=5 AND ClientID ={0}  "

    Public SqlForProperty As String = "" & _
      "select 'Contacts' As Type, 'Contacts' As Description, Count(*) As Value, 0 As FromID,0 as SummType, {0} as ClientID, {1} as PropertyID, 0 As UnitID, 0 as AssetID From tbl_ContactLink Where ClientID={0} And PropertyID={1};" & _
    "select 'Properties' AS Type, 'Units' AS Description,COUNT(*) AS Value,0 As FromID,22 as SummType,{0} as ClientID, {1} as PropertyID,0 as UnitID, 0 as AssetID From View_Unit_Ext where ClientID={0} And PropertyID={1} And HelpdeskStatus != 0;" & _
        "select 'Properties' AS Type, 'Assets' AS Description,COUNT(*) AS Value,0 As FromID,21 as SummType,{0} as ClientID, {1} as PropertyID,0 as UnitID, 0 as AssetID From View_Asset_Ext where ClientID={0} And PropertyID={1} AND UnitID=0;" & _
 "SELECT     Description, 'OPEN CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 1 AS SummType, {0} AS ClientID, {1} AS PropertyID, 0 AS UnitID, 0 AS AssetID " & _
    "FROM        View_WebSummary_Totals WHERE (StatusID = 1) AND ClientID ={0} AND PropertyID={1} GROUP BY Description;" & _
       "SELECT  Description,    'OPEN CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 2 AS SummType, {0} AS ClientID, {1} AS PropertyID, 0 AS UnitID, 0 AS AssetID " & _
    "FROM        View_WebSummary_Totals WHERE (StatusID = 2) AND ClientID ={0} And PropertyID={1} GROUP BY Description;" & _
       "SELECT   Description,   'OPEN CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 3 AS SummType, {0} AS ClientID, {1} AS PropertyID, 0 AS UnitID, 0 AS AssetID " & _
    "FROM        View_WebSummary_Totals WHERE (StatusID = 3) AND ClientID ={0} And PropertyID={1} GROUP BY Description;" & _
       "SELECT   Description,   'OPEN CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 4 AS SummType, {0} AS ClientID, {1} AS PropertyID, 0 AS UnitID, 0 AS AssetID " & _
    "FROM        View_WebSummary_Totals WHERE (StatusID = 4) AND ClientID ={0} And PropertyID={1} GROUP BY Description;" & _
       "SELECT   Description,   'OPEN CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 6 AS SummType, {0} AS ClientID, {1} AS PropertyID, 0 AS UnitID, 0 AS AssetID " & _
    "FROM        View_WebSummary_Totals WHERE (StatusID = 6) AND ClientID ={0} And PropertyID={1} GROUP BY Description;" & _
       "SELECT   Description,   'OPEN CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 7 AS SummType, {0} AS ClientID, {1} AS PropertyID, 0 AS UnitID, 0 AS AssetID " & _
    "FROM        View_WebSummary_Totals WHERE (StatusID = 7) AND ClientID ={0} And PropertyID={1} GROUP BY Description;" & _
       "SELECT   Description,   'OPEN CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 8 AS SummType, {0} AS ClientID, {1} AS PropertyID, 0 AS UnitID, 0 AS AssetID " & _
    "FROM        View_WebSummary_Totals WHERE (StatusID = 8) AND ClientID ={0} And PropertyID={1} GROUP BY Description;" & _
       "SELECT   Description,   'OPEN CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 9 AS SummType, {0} AS ClientID, {1} AS PropertyID, 0 AS UnitID, 0 AS AssetID " & _
    "FROM        View_WebSummary_Totals WHERE (StatusID = 9) AND ClientID ={0} And PropertyID={1} GROUP BY Description;" & _
    "select 'CLOSED CALLS' AS Type, 'In Last 24 Hours' AS Description,COUNT(*) AS Value,0 As FromID,5 as SummType, {0} as ClientID,{1} As PropertyID,0 as UnitID, 0 as AssetID From View_WebSummary_Totals  where ClientID ={0} And PropertyID={1} AND DATEDIFF(hour, ClosedAt, getdate()) < 24; " & _
 "select 'CLOSED CALLS' AS Type, 'Total Closed Calls' AS Description,COUNT(*) AS Value,0 As FromID,5 as SummType, {0} as ClientID,{1} As PropertyID,0 as UnitID, 0 as AssetID From View_WebSummary_Totals  Where StatusID=5 AND PropertyID={1} And ClientID ={0}  "

    Public SqlForUnit As String = "" & _
      "select 'Contacts' As Type, 'Contacts' As Description, Count(*) As Value, 0 As FromID,0 as SummType, {0} as ClientID, {1} as PropertyID, {2} As UnitID, 0 as AssetID From tbl_ContactLink Where ClientID={0} And PropertyID={1} And UnitID={2};" & _
     "select 'Properties' AS Type, 'Units' AS Description,COUNT(*) AS Value,0 As FromID,22 as SummType,{0} as ClientID, {1} as PropertyID,{2} as UnitID, 0 as AssetID From View_Unit_Ext where ClientID={0} And PropertyID={1} And UnitID={2} And HelpdeskStatus != 0;" & _
       "select 'Properties' AS Type, 'Assets' AS Description,COUNT(*) AS Value,0 As FromID,21 as SummType,{0} as ClientID, {1} as PropertyID,{2} as UnitID, 0 as AssetID From View_Asset_Ext where ClientID={0} And PropertyID={1} And UnitID={2};" & _
     "SELECT     Description, 'OPEN CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 1 AS SummType, {0} AS ClientID, {1} AS PropertyID, {2} AS UnitID, 0 AS AssetID " & _
    "FROM        View_WebSummary_Totals WHERE (StatusID = 1) AND ClientID ={0} AND PropertyID={1} And UnitID={2} GROUP BY Description;" & _
       "SELECT  Description,    'OPEN CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 2 AS SummType, {0} AS ClientID, {1} AS PropertyID, {2} AS UnitID, 0 AS AssetID " & _
    "FROM        View_WebSummary_Totals WHERE (StatusID = 2) AND ClientID ={0} And PropertyID={1}  And UnitID={2} GROUP BY Description;" & _
       "SELECT   Description,   'OPEN CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 3 AS SummType, {0} AS ClientID, {1} AS PropertyID, {2} AS UnitID, 0 AS AssetID " & _
    "FROM        View_WebSummary_Totals WHERE (StatusID = 3) AND ClientID ={0} And PropertyID={1}  And UnitID={2} GROUP BY Description;" & _
       "SELECT   Description,   'OPEN CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 4 AS SummType, {0} AS ClientID, {1} AS PropertyID, {2} AS UnitID, 0 AS AssetID " & _
    "FROM        View_WebSummary_Totals WHERE (StatusID = 4) AND ClientID ={0} And PropertyID={1}  And UnitID={2} GROUP BY Description;" & _
       "SELECT   Description,   'OPEN CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 6 AS SummType, {0} AS ClientID, {1} AS PropertyID, {2} AS UnitID, 0 AS AssetID " & _
    "FROM        View_WebSummary_Totals WHERE (StatusID = 6) AND ClientID ={0} And PropertyID={1}  And UnitID={2} GROUP BY Description;" & _
       "SELECT   Description,   'OPEN CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 7 AS SummType, {0} AS ClientID, {1} AS PropertyID, {2} AS UnitID, 0 AS AssetID " & _
    "FROM        View_WebSummary_Totals WHERE (StatusID = 7) AND ClientID ={0} And PropertyID={1}  And UnitID={2} GROUP BY Description;" & _
       "SELECT   Description,   'OPEN CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 8 AS SummType, {0} AS ClientID, {1} AS PropertyID, {2} AS UnitID, 0 AS AssetID " & _
    "FROM        View_WebSummary_Totals WHERE (StatusID = 8) AND ClientID ={0} And PropertyID={1}  And UnitID={2} GROUP BY Description;" & _
       "SELECT   Description,   'OPEN CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 9 AS SummType, {0} AS ClientID, {1} AS PropertyID, {2} AS UnitID, 0 AS AssetID " & _
    "FROM        View_WebSummary_Totals WHERE (StatusID = 9) AND ClientID ={0} And PropertyID={1}  And UnitID={2} GROUP BY Description;" & _
    "select 'CLOSED CALLS' AS Type, 'In Last 24 Hours' AS Description,COUNT(*) AS Value,0 As FromID,5 as SummType, {0} as ClientID,{1} As PropertyID,{2} as UnitID, 0 as AssetID From View_WebSummary_Totals  where ClientID ={0} And PropertyID={1} And UnitID={2}AND DATEDIFF(hour, ClosedAt, getdate()) < 24; " & _
 "select 'CLOSED CALLS' AS Type, 'Total Closed Calls' AS Description,COUNT(*) AS Value,0 As FromID,5 as SummType, {0} as ClientID,{1} As PropertyID,{2} as UnitID, 0 as AssetID From View_WebSummary_Totals  Where StatusID=5 AND UnitID={2} AND PropertyID={1} And ClientID ={0}  "

    Public SqlForAsset As String = "" & _
     "select 'Contacts' As Type, 'Contacts' As Description, Count(*) As Value, 0 As FromID,0 as SummType, {0} as ClientID, {1} as PropertyID, {2} As UnitID, {3} as AssetID From tbl_ContactLink Where ClientID={0} And PropertyID={1} And UnitID={2} And AssetID={3};" & _
    "select 'Properties' AS Type, 'Properties' AS Description,COUNT(*) AS Value,0 As FromID,21 as SummType,{0} as ClientID, {1} as PropertyID,{2} as UnitID, {3} as AssetID From View_Property_Ext where ClientID={0} And PropertyID={1} And UnitID={2} And AssetID={3} AND HelpdeskStatus != 0;" & _
    "select 'Properties' AS Type, 'Units' AS Description,COUNT(*) AS Value,0 As FromID,22 as SummType,{0} as ClientID, {1} as PropertyID,{2} as UnitID, {3} as AssetID From View_Unit_Ext where ClientID={0} And PropertyID={1} And UnitID={2} And AssetID={3} And HelpdeskStatus != 0;" & _
 "SELECT     Description, 'OPEN CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 1 AS SummType, {0} AS ClientID, {1} AS PropertyID, {2} AS UnitID, {3} AS AssetID " & _
    "FROM        View_WebSummary_Totals WHERE (StatusID = 1) AND ClientID ={0} AND PropertyID={1} And UnitID={2}  And AssetID={3} GROUP BY Description;" & _
       "SELECT  Description,    'OPEN CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 2 AS SummType, {0} AS ClientID, {1} AS PropertyID, {2} AS UnitID, {3} AS AssetID " & _
    "FROM        View_WebSummary_Totals WHERE (StatusID = 2) AND ClientID ={0} And PropertyID={1}  And UnitID={2}  And AssetID={3} GROUP BY Description;" & _
       "SELECT   Description,   'OPEN CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 3 AS SummType, {0} AS ClientID, {1} AS PropertyID, {2} AS UnitID, {3} AS AssetID " & _
    "FROM        View_WebSummary_Totals WHERE (StatusID = 3) AND ClientID ={0} And PropertyID={1}  And UnitID={2} And AssetID={3}  GROUP BY Description;" & _
       "SELECT   Description,   'OPEN CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 4 AS SummType, {0} AS ClientID, {1} AS PropertyID, {2} AS UnitID, {3} AS AssetID " & _
    "FROM        View_WebSummary_Totals WHERE (StatusID = 4) AND ClientID ={0} And PropertyID={1}  And UnitID={2}  And AssetID={3} GROUP BY Description;" & _
       "SELECT   Description,   'OPEN CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 6 AS SummType, {0} AS ClientID, {1} AS PropertyID, {2} AS UnitID, {3} AS AssetID " & _
    "FROM        View_WebSummary_Totals WHERE (StatusID = 6) AND ClientID ={0} And PropertyID={1}  And UnitID={2}  And AssetID={3} GROUP BY Description;" & _
       "SELECT   Description,   'OPEN CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 7 AS SummType, {0} AS ClientID, {1} AS PropertyID, {2} AS UnitID, {3} AS AssetID " & _
    "FROM        View_WebSummary_Totals WHERE (StatusID = 7) AND ClientID ={0} And PropertyID={1}  And UnitID={2}  And AssetID={3} GROUP BY Description;" & _
       "SELECT   Description,   'OPEN CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 8 AS SummType, {0} AS ClientID, {1} AS PropertyID, {2} AS UnitID, {3} AS AssetID " & _
    "FROM        View_WebSummary_Totals WHERE (StatusID = 8) AND ClientID ={0} And PropertyID={1}  And UnitID={2}  And AssetID={3} GROUP BY Description;" & _
       "SELECT   Description,   'OPEN CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 9 AS SummType, {0} AS ClientID, {1} AS PropertyID, {2} AS UnitID, {3} AS AssetID " & _
    "FROM        View_WebSummary_Totals WHERE (StatusID = 9) AND ClientID ={0} And PropertyID={1}  And UnitID={2}  And AssetID={3} GROUP BY Description;" & _
    "select 'CLOSED CALLS' AS Type, 'In Last 24 Hours' AS Description,COUNT(*) AS Value,0 As FromID,5 as SummType, {0} as ClientID,{1} As PropertyID,{2} as UnitID, {3} as AssetID From View_WebSummary_Totals  where ClientID ={0} And PropertyID={1} And UnitID={2} And AssetID={3} AND DATEDIFF(hour, ClosedAt, getdate()) < 24; " & _
 "select 'CLOSED CALLS' AS Type, 'Total Closed Calls' AS Description,COUNT(*) AS Value,0 As FromID,5 as SummType, {0} as ClientID,{1} As PropertyID,{2} as UnitID, {3} as AssetID From View_WebSummary_Totals  Where StatusID=5 AND UnitID={2} AND PropertyID={1} And ClientID ={0}  And AssetID={3} " & _
    "select 'Finance' AS Type, 'Total Cost' AS Description,SUM(TotalAmount)AS Value,{3} As AssetID,46 as SummType,{0} as ClientID,{1} as PropertyID,{2} as UnitID,{3} As FromID From tbl_Cost INNER JOIN tbl_Incidence ON tbl_Cost.IncidentID = tbl_Incidence.IncidentID inner Join tbl_incidence_call on tbl_incidence_call.CallID = tbl_incidence.incidentid WHERE StatusID=1 And " & "ClientID={0} And PropertyID={1} And UnitID={2} AND AssetID={3};" & _
    "select 'Finance' AS Type, 'Total Hours' AS Description,SUM(tbl_Action.TimeTaken) AS Value,{3} As AssetID,46 as SummType,{0} as ClientID,{1} as PropertyID,{2} as UnitID,0 As FromID FROM tbl_Action INNER JOIN tbl_Incidence ON tbl_Action.IncidentID = tbl_Incidence.IncidentID inner Join tbl_incidence_call on tbl_incidence_call.CallID = tbl_incidence.incidentid where " & "ClientID={0} And PropertyID={1} And UnitID={2} AND AssetID={3};" & _
"select 'Finance' AS Type, 'Total Parts' AS Description,COUNT(*) AS Value,{3} As AssetID,46 as SummType,{0} as ClientID,{1} as PropertyID,{2} as UnitID,0 As FromID From tbl_POrder INNER JOIN tbl_Incidence ON tbl_POrder.IncidentID = tbl_Incidence.IncidentID inner Join tbl_incidence_call on tbl_incidence_call.CallID = tbl_incidence.incidentid where " & "ClientID={0} And PropertyID={1} And UnitID={2} AND AssetID={3};"
    '"select 'CALLS' AS Type, 'Calls In Last 12 Months' AS Description,COUNT(*) as Value,45 as SummType,{0} as ClientID,{1} as PropertyID,{2} as UnitID From tbl_incidence  inner Join tbl_incidence_call on tbl_incidence_call.CallID = tbl_incidence.incidentid  where StatusID=1 And " & "ClientID={0} And PropertyID={1} And UnitID={2} AND AssetID={3};" & _

    Public SqlForSupplier As String = "" & _
   "SELECT     'CALLS' AS Type, COUNT(*) AS Value, MAX(dbo.TBL_INCIDENCE_CALL.SupplierID) AS FromID, 0 AS AssetID, 1 AS SummType, 1 AS ClientID, " & _
   "                   1 AS PropertyID, 1 AS UnitID, dbo.Config_IncidentStatus.StatusDesc AS Description " & _
  "FROM         dbo.TBL_INCIDENCE INNER JOIN " & _
 "                    dbo.TBL_INCIDENCE_CALL ON dbo.TBL_INCIDENCE_CALL.CallID = dbo.TBL_INCIDENCE.CallID INNER JOIN " & _
  "                    dbo.Config_IncidentStatus ON dbo.TBL_INCIDENCE.StatusID = dbo.Config_IncidentStatus.StatusID " & _
"WHERE     (dbo.TBL_INCIDENCE.StatusID = 1) AND (dbo.TBL_INCIDENCE_CALL.SupplierID = {4}) " & _
"GROUP BY dbo.Config_IncidentStatus.StatusDesc; " & _
  "SELECT     'CALLS' AS Type, COUNT(*) AS Value, MAX(dbo.TBL_INCIDENCE_CALL.SupplierID) AS FromID, 0 AS AssetID, 2 AS SummType, 1 AS ClientID, " & _
   "                   1 AS PropertyID, 1 AS UnitID, dbo.Config_IncidentStatus.StatusDesc AS Description " & _
"FROM         dbo.TBL_INCIDENCE INNER JOIN " & _
 "                    dbo.TBL_INCIDENCE_CALL ON dbo.TBL_INCIDENCE_CALL.CallID = dbo.TBL_INCIDENCE.CallID INNER JOIN " & _
  "                    dbo.Config_IncidentStatus ON dbo.TBL_INCIDENCE.StatusID = dbo.Config_IncidentStatus.StatusID " & _
"WHERE     (dbo.TBL_INCIDENCE.StatusID = 2) AND (dbo.TBL_INCIDENCE_CALL.SupplierID = {4}) " & _
"GROUP BY dbo.Config_IncidentStatus.StatusDesc; " & _
  "SELECT     'CALLS' AS Type, COUNT(*) AS Value, MAX(dbo.TBL_INCIDENCE_CALL.SupplierID) AS FromID, 0 AS AssetID, 3 AS SummType, 1 AS ClientID, " & _
   "                   1 AS PropertyID, 1 AS UnitID, dbo.Config_IncidentStatus.StatusDesc AS Description " & _
"FROM         dbo.TBL_INCIDENCE INNER JOIN " & _
 "                    dbo.TBL_INCIDENCE_CALL ON dbo.TBL_INCIDENCE_CALL.CallID = dbo.TBL_INCIDENCE.CallID INNER JOIN " & _
  "                    dbo.Config_IncidentStatus ON dbo.TBL_INCIDENCE.StatusID = dbo.Config_IncidentStatus.StatusID " & _
"WHERE     (dbo.TBL_INCIDENCE.StatusID = 3) AND (dbo.TBL_INCIDENCE_CALL.SupplierID = {4}) " & _
"GROUP BY dbo.Config_IncidentStatus.StatusDesc; " & _
  "SELECT     'CALLS' AS Type, COUNT(*) AS Value, MAX(dbo.TBL_INCIDENCE_CALL.SupplierID) AS FromID, 0 AS AssetID, 4 AS SummType, 1 AS ClientID, " & _
   "                   1 AS PropertyID, 1 AS UnitID, dbo.Config_IncidentStatus.StatusDesc AS Description " & _
"FROM         dbo.TBL_INCIDENCE INNER JOIN " & _
 "                    dbo.TBL_INCIDENCE_CALL ON dbo.TBL_INCIDENCE_CALL.CallID = dbo.TBL_INCIDENCE.CallID INNER JOIN " & _
  "                    dbo.Config_IncidentStatus ON dbo.TBL_INCIDENCE.StatusID = dbo.Config_IncidentStatus.StatusID " & _
"WHERE     (dbo.TBL_INCIDENCE.StatusID = 4) AND (dbo.TBL_INCIDENCE_CALL.SupplierID = {4}) " & _
"GROUP BY dbo.Config_IncidentStatus.StatusDesc; " & _
  "SELECT     'CALLS' AS Type, COUNT(*) AS Value, MAX(dbo.TBL_INCIDENCE_CALL.SupplierID) AS FromID, 0 AS AssetID, 6 AS SummType, 1 AS ClientID, " & _
   "                   1 AS PropertyID, 1 AS UnitID, dbo.Config_IncidentStatus.StatusDesc AS Description " & _
"FROM         dbo.TBL_INCIDENCE INNER JOIN " & _
 "                    dbo.TBL_INCIDENCE_CALL ON dbo.TBL_INCIDENCE_CALL.CallID = dbo.TBL_INCIDENCE.CallID INNER JOIN " & _
  "                    dbo.Config_IncidentStatus ON dbo.TBL_INCIDENCE.StatusID = dbo.Config_IncidentStatus.StatusID " & _
"WHERE     (dbo.TBL_INCIDENCE.StatusID = 6) AND (dbo.TBL_INCIDENCE_CALL.SupplierID = {4}) " & _
"GROUP BY dbo.Config_IncidentStatus.StatusDesc; " & _
  "SELECT     'CALLS' AS Type, COUNT(*) AS Value, MAX(dbo.TBL_INCIDENCE_CALL.SupplierID) AS FromID, 0 AS AssetID, 7 AS SummType, 1 AS ClientID, " & _
   "                   1 AS PropertyID, 1 AS UnitID, dbo.Config_IncidentStatus.StatusDesc AS Description " & _
"FROM         dbo.TBL_INCIDENCE INNER JOIN " & _
 "                    dbo.TBL_INCIDENCE_CALL ON dbo.TBL_INCIDENCE_CALL.CallID = dbo.TBL_INCIDENCE.CallID INNER JOIN " & _
  "                    dbo.Config_IncidentStatus ON dbo.TBL_INCIDENCE.StatusID = dbo.Config_IncidentStatus.StatusID " & _
"WHERE     (dbo.TBL_INCIDENCE.StatusID = 7) AND (dbo.TBL_INCIDENCE_CALL.SupplierID = {4}) " & _
"GROUP BY dbo.Config_IncidentStatus.StatusDesc; " & _
  "SELECT     'CALLS' AS Type, COUNT(*) AS Value, MAX(dbo.TBL_INCIDENCE_CALL.SupplierID) AS FromID, 0 AS AssetID, 8 AS SummType, 1 AS ClientID, " & _
   "                   1 AS PropertyID, 1 AS UnitID, dbo.Config_IncidentStatus.StatusDesc AS Description " & _
"FROM         dbo.TBL_INCIDENCE INNER JOIN " & _
 "                    dbo.TBL_INCIDENCE_CALL ON dbo.TBL_INCIDENCE_CALL.CallID = dbo.TBL_INCIDENCE.CallID INNER JOIN " & _
  "                    dbo.Config_IncidentStatus ON dbo.TBL_INCIDENCE.StatusID = dbo.Config_IncidentStatus.StatusID " & _
"WHERE     (dbo.TBL_INCIDENCE.StatusID = 8) AND (dbo.TBL_INCIDENCE_CALL.SupplierID = {4}) " & _
"GROUP BY dbo.Config_IncidentStatus.StatusDesc; " & _
  "SELECT     'CALLS' AS Type, COUNT(*) AS Value, MAX(dbo.TBL_INCIDENCE_CALL.SupplierID) AS FromID, 0 AS AssetID, 9 AS SummType, 1 AS ClientID, " & _
   "                   1 AS PropertyID, 1 AS UnitID, dbo.Config_IncidentStatus.StatusDesc AS Description " & _
"FROM         dbo.TBL_INCIDENCE INNER JOIN " & _
 "                    dbo.TBL_INCIDENCE_CALL ON dbo.TBL_INCIDENCE_CALL.CallID = dbo.TBL_INCIDENCE.CallID INNER JOIN " & _
  "                    dbo.Config_IncidentStatus ON dbo.TBL_INCIDENCE.StatusID = dbo.Config_IncidentStatus.StatusID " & _
"WHERE     (dbo.TBL_INCIDENCE.StatusID = 9) AND (dbo.TBL_INCIDENCE_CALL.SupplierID = {4}) " & _
"GROUP BY dbo.Config_IncidentStatus.StatusDesc; " & _
"select 'CALLS' AS Type, 'Total Calls' AS Description,COUNT(*) as Value,Max(SupplierID) As FromID,23 as SummType, 0 As AssetID, {0} as ClientID,{1} as PropertyID,{2} as UnitID From tbl_incidence inner Join tbl_incidence_call on tbl_incidence_call.CallID = tbl_incidence.Callid where " & "SupplierID={4};" & _
    "select 'Finance' AS Type, 'Total Cost' AS Description,SUM(TotalCost) AS Value,Max(SupplierID) As FromID, 0 As AssetID, 46 as SummType,{0} as ClientID,{1} as PropertyID,{2} as UnitID From tbl_Incidence_CostItem WHERE SupplierID= {4};" & _
"select 'Finance' AS Type, 'Total Hours' AS Description,Sum(TimeSpent) AS Value,Max(AssignedSupplier) As FromID, 0 As AssetID, 46 as SummType,0 as ClientID,0 as PropertyID,0 as UnitID From tbl_Incidence_ActionItem WHERE AssignedSupplier= {4};"
    '"select 'CALLS' AS Type, 'Total Hours' AS Description,SUM(tbl_Action.TimeTaken) AS Value, 0 As AssetID, 1 As FromID,46 as SummType,{0} as ClientID,{1} as PropertyID,{2} as UnitID FROM tbl_Action INNER JOIN tbl_Incidence ON tbl_Action.IncidentID = tbl_Incidence.IncidentID inner Join tbl_incidence_call on tbl_incidence_call.CallID = tbl_incidence.incidentid WHERE SupplierID= {4};"

    '"select 'CALLS' AS Type, 'Calls In Last 12 Months' AS Description,COUNT(*) as Value,45 as SummType,{0} as ClientID,{1} as PropertyID,{2} as UnitID From tbl_incident where StatusID=1 And " & "EngineerID={4};" & _

    Public SqlForAllSuppliers As String = "" & _
"SELECT     'CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 0 AS AssetID, 1 AS SummType, 0 AS ClientID, " & _
"                   0 AS PropertyID, 0 AS UnitID, dbo.Config_IncidentStatus.StatusDesc AS Description " & _
"FROM         dbo.TBL_INCIDENCE INNER JOIN " & _
"                    dbo.TBL_INCIDENCE_CALL ON dbo.TBL_INCIDENCE_CALL.CallID = dbo.TBL_INCIDENCE.CallID INNER JOIN " & _
"                    dbo.Config_IncidentStatus ON dbo.TBL_INCIDENCE.StatusID = dbo.Config_IncidentStatus.StatusID " & _
"WHERE     (dbo.TBL_INCIDENCE.StatusID = 1)  " & _
"GROUP BY dbo.Config_IncidentStatus.StatusDesc; " & _
"SELECT     'CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 0 AS AssetID, 1 AS SummType, 0 AS ClientID, " & _
"                   0 AS PropertyID, 0 AS UnitID, dbo.Config_IncidentStatus.StatusDesc AS Description " & _
"FROM         dbo.TBL_INCIDENCE INNER JOIN " & _
"                    dbo.TBL_INCIDENCE_CALL ON dbo.TBL_INCIDENCE_CALL.CallID = dbo.TBL_INCIDENCE.CallID INNER JOIN " & _
"                    dbo.Config_IncidentStatus ON dbo.TBL_INCIDENCE.StatusID = dbo.Config_IncidentStatus.StatusID " & _
"WHERE     (dbo.TBL_INCIDENCE.StatusID = 2)  " & _
"GROUP BY dbo.Config_IncidentStatus.StatusDesc; " & _
"SELECT     'CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 0 AS AssetID, 1 AS SummType, 0 AS ClientID, " & _
"                   0 AS PropertyID, 0 AS UnitID, dbo.Config_IncidentStatus.StatusDesc AS Description " & _
"FROM         dbo.TBL_INCIDENCE INNER JOIN " & _
"                    dbo.TBL_INCIDENCE_CALL ON dbo.TBL_INCIDENCE_CALL.CallID = dbo.TBL_INCIDENCE.CallID INNER JOIN " & _
"                    dbo.Config_IncidentStatus ON dbo.TBL_INCIDENCE.StatusID = dbo.Config_IncidentStatus.StatusID " & _
"WHERE     (dbo.TBL_INCIDENCE.StatusID = 3)  " & _
"GROUP BY dbo.Config_IncidentStatus.StatusDesc; " & _
"SELECT     'CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 0 AS AssetID, 1 AS SummType, 0 AS ClientID, " & _
"                   0 AS PropertyID, 0 AS UnitID, dbo.Config_IncidentStatus.StatusDesc AS Description " & _
"FROM         dbo.TBL_INCIDENCE INNER JOIN " & _
"                    dbo.TBL_INCIDENCE_CALL ON dbo.TBL_INCIDENCE_CALL.CallID = dbo.TBL_INCIDENCE.CallID INNER JOIN " & _
"                    dbo.Config_IncidentStatus ON dbo.TBL_INCIDENCE.StatusID = dbo.Config_IncidentStatus.StatusID " & _
"WHERE     (dbo.TBL_INCIDENCE.StatusID = 4) " & _
"GROUP BY dbo.Config_IncidentStatus.StatusDesc; " & _
"SELECT     'CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 0 AS AssetID, 1 AS SummType, 0 AS ClientID, " & _
"                   0 AS PropertyID, 0 AS UnitID, dbo.Config_IncidentStatus.StatusDesc AS Description " & _
"FROM         dbo.TBL_INCIDENCE INNER JOIN " & _
"                    dbo.TBL_INCIDENCE_CALL ON dbo.TBL_INCIDENCE_CALL.CallID = dbo.TBL_INCIDENCE.CallID INNER JOIN " & _
"                    dbo.Config_IncidentStatus ON dbo.TBL_INCIDENCE.StatusID = dbo.Config_IncidentStatus.StatusID " & _
"WHERE     (dbo.TBL_INCIDENCE.StatusID = 6)  " & _
"GROUP BY dbo.Config_IncidentStatus.StatusDesc; " & _
"SELECT     'CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 0 AS AssetID, 1 AS SummType, 0 AS ClientID, " & _
"                   0 AS PropertyID, 0 AS UnitID, dbo.Config_IncidentStatus.StatusDesc AS Description " & _
"FROM         dbo.TBL_INCIDENCE INNER JOIN " & _
"                    dbo.TBL_INCIDENCE_CALL ON dbo.TBL_INCIDENCE_CALL.CallID = dbo.TBL_INCIDENCE.CallID INNER JOIN " & _
"                    dbo.Config_IncidentStatus ON dbo.TBL_INCIDENCE.StatusID = dbo.Config_IncidentStatus.StatusID " & _
"WHERE     (dbo.TBL_INCIDENCE.StatusID = 7) " & _
"GROUP BY dbo.Config_IncidentStatus.StatusDesc; " & _
"SELECT     'CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 0 AS AssetID, 1 AS SummType, 0 AS ClientID, " & _
"                   0 AS PropertyID, 0 AS UnitID, dbo.Config_IncidentStatus.StatusDesc AS Description " & _
"FROM         dbo.TBL_INCIDENCE INNER JOIN " & _
"                    dbo.TBL_INCIDENCE_CALL ON dbo.TBL_INCIDENCE_CALL.CallID = dbo.TBL_INCIDENCE.CallID INNER JOIN " & _
"                    dbo.Config_IncidentStatus ON dbo.TBL_INCIDENCE.StatusID = dbo.Config_IncidentStatus.StatusID " & _
"WHERE     (dbo.TBL_INCIDENCE.StatusID = 8)  " & _
"GROUP BY dbo.Config_IncidentStatus.StatusDesc; " & _
"SELECT     'CALLS' AS Type, COUNT(*) AS Value, 0 AS FromID, 0 AS AssetID, 1 AS SummType, 0 AS ClientID, " & _
"                   0 AS PropertyID, 0 AS UnitID, dbo.Config_IncidentStatus.StatusDesc AS Description " & _
"FROM         dbo.TBL_INCIDENCE INNER JOIN " & _
"                    dbo.TBL_INCIDENCE_CALL ON dbo.TBL_INCIDENCE_CALL.CallID = dbo.TBL_INCIDENCE.CallID INNER JOIN " & _
"                    dbo.Config_IncidentStatus ON dbo.TBL_INCIDENCE.StatusID = dbo.Config_IncidentStatus.StatusID " & _
"WHERE     (dbo.TBL_INCIDENCE.StatusID = 9)  " & _
"GROUP BY dbo.Config_IncidentStatus.StatusDesc; " & _
"select 'CALLS' AS Type, 'Total Calls' AS Description,COUNT(*) as Value,0 As FromID,23 as SummType, 0 As AssetID, {0} as ClientID,{1} as PropertyID,{2} as UnitID From tbl_incidence inner Join tbl_incidence_call on tbl_incidence_call.CallID = tbl_incidence.Callid ;" & _
 "select 'Finance' AS Type, 'Total Cost' AS Description,SUM(TotalCost) AS Value,Max(SupplierID) As FromID, 0 As AssetID, 46 as SummType,{0} as ClientID,{1} as PropertyID,{2} as UnitID From tbl_Incidence_CostItem ;" & _
"select 'Finance' AS Type, 'Total Hours' AS Description,Sum(TimeSpent) AS Value,Max(AssignedSupplier) As FromID, 0 As AssetID, 46 as SummType,0 as ClientID,0 as PropertyID,0 as UnitID From tbl_Incidence_ActionItem ;"
    '"select 'CALLS' AS Type, 'Total Hours' AS Description,SUM(tbl_Action.TimeTaken) AS Value, 0 As AssetID, 1 As FromID,46 as SummType,{0} as ClientID,{1} as PropertyID,{2} as UnitID FROM tbl_Action INNER JOIN tbl_Incidence ON tbl_Action.IncidentID = tbl_Incidence.IncidentID inner Join tbl_incidence_call on tbl_incidence_call.CallID = tbl_incidence.incidentid WHERE SupplierID= {4};"

    '"select 'CALLS' AS Type, 'Calls In Last 12 Months' AS Description,COUNT(*) as Value,45 as SummType,{0} as ClientID,{1} as PropertyID,{2} as UnitID From tbl_incident where StatusID=1 And " & "EngineerID={4};" & _


    Public SQLForContact As String = "" & _
    "Select Contact.*," & _
" (SELECT Count(CallID) FROM tbl_Incidence_Call WHERE LoggedBY={0}) 'TotalCalls'" & _
 " FROM View_Contact_Ext As Contact Where ContactID={0}"

    Public SqlForCall As String = "" & _
    "SELECT Incident.*," & _
    "(SELECT Sum(TotalCost) FROM View_Incidence_Costs WHERE IncidentID=Incident.IncidentID) 'CostsTotal'," & _
    "(SELECT Sum(CostPerUnit) FROM View_Incidence_Parts WHERE IncidentID=Incident.IncidentID) 'PartsTotal'," & _
    "(SELECT Sum(LineTotal) FROM View_Incidence_Charges WHERE IncidentID=Incident.IncidentID) 'ChargesTotal'," & _
    "(SELECT Sum(TimeSpent) FROM View_Incidence_Actions WHERE IncidentID=Incident.IncidentID) 'TotalTime'," & _
    "(Select Sum(InvoicedTotal) From View_Incidence_Invoices where CallId = Incident.IncidentID) 'TotalInvoiced'" & _
    ", View_Dream_JobSummary.BUDGETVALUE AS BudgetValue, View_Dream_JobSummary.ACTUALVALUE AS ActualValue, View_Dream_JobSummary.COMMITMENTVALUE AS CommitmentValue" & _
    " FROM View_Incidence Incident LEFT OUTER JOIN View_Dream_JobSummary ON Incident.IncidentID = View_Dream_JobSummary.IncidentID " & _
    "Where (Incident.IncidentID={0})"
    Public Function CreateInfoTable(ByVal oItemType As ItemType, ByRef params As Object) As DataSet
        Dim oDataSet As New DataSet
        Dim InfoTable As New DataTable("InfoTable")
        Dim InfoSQL As String = ""
        Select Case oItemType
            Case ItemType.Client
                InfoSQL = "SELECT EXTREF, ClientName As ClientName, tbl_Contact.Title, tbl_Contact.FirstName, tbl_Contact.LastName, tbl_Contact.Telephone, Address1, Address2, City, Postcode FROM View_Client_EXT LEFT OUTER JOIN tbl_Contact ON View_Client_EXT.ContactID = tbl_Contact.ContactID Where ClientID= " & params(0)
            Case ItemType.Property
                InfoSQL = "SELECT TenantName, TenantEmail, TenantTelephone, EXTREF, ClientName, PropertyName, tbl_Contact.Title, tbl_Contact.FirstName, tbl_Contact.LastName, tbl_Contact.Telephone, Address1, Address2,  City, Postcode FROM View_Property_EXT LEFT OUTER JOIN tbl_Contact ON view_Property_Ext.ContactID = tbl_Contact.ContactID Where PropertyID=" & params(1)
            Case ItemType.Unit
                InfoSQL = "SELECT TenantName, TenantEmail, TenantTelephone, EXTREF, ClientName, PropertyName, UnitName, tbl_Contact.FirstName, tbl_Contact.LastName, tbl_Contact.Telephone, Address1, Address2, Address3, City, Postcode, tbl_Contact.Title FROM View_Unit_EXT LEFT OUTER JOIN tbl_Contact ON View_Unit_EXT.ContactID = tbl_Contact.ContactID WHERE UnitID = " & params(2)
            Case ItemType.Asset
                InfoSQL = "SELECT * FROM View_Asset_EXT Where AssetID = " & params(3)
            Case ItemType.Supplier
                InfoSQL = "SELECT * FROM View_Supplier_EXT Where SupplierID=" & params(4)
            Case ItemType.Contact
                InfoSQL = "Select * FROM View_Contact_EXT Where ContactID=" & params(4)
        End Select
        oDataSet = oDataAccessor.SelectQuery(InfoSQL)
        InfoTable = oDataSet.Tables(0)
        Return oDataSet

    End Function

    Public Function CreateSummaryTable(ByRef schema() As String) As DataTable

        Dim TableSummarySchema(8) As String
        'Dim TableSummarySchema(6) As String
        TableSummarySchema(0) = "Type"
        TableSummarySchema(1) = "Description"
        TableSummarySchema(2) = "Value"
        TableSummarySchema(3) = "SummType"
        TableSummarySchema(4) = "ClientID"
        TableSummarySchema(5) = "PropertyID"
        TableSummarySchema(6) = "UnitID"
        TableSummarySchema(7) = "FromID"
        TableSummarySchema(8) = "AssetID"

        Dim DBTable_Summary As New DataTable("SummaryDetails")
        DBTable_Summary.Columns.Clear()
        Dim i As Integer = 0

        For i = 0 To 8                              'Create Data Table Columns
            Dim DBFD As New DataColumn(TableSummarySchema(i))
            DBFD.DefaultValue = ""
            DBFD.DataType = System.Type.GetType("System.String")
            DBTable_Summary.Columns.Add(DBFD)
        Next

        schema = TableSummarySchema

        Return DBTable_Summary

    End Function

    Public Function ExecuteSummarySql(ByVal summaryTable As DataTable, ByVal xmlProducts As System.Web.UI.WebControls.Xml, ByVal tableSchema() As String, ByVal sql As String)

        Dim dstSummary As New DataSet
        Dim DBTable_Summary As DataTable = summaryTable
        ' Dim DR As SqlDataReader = oDataAccessor.ReaderQuery(sql)
        Dim oDataSet As DataSet = oDataAccessor.SelectQuery(sql)
        Dim i As Int32 = 0
        Dim j As Int32 = 0
        For j = 0 To oDataSet.Tables.Count - 1
            With oDataSet.Tables(j)
                Dim oEnum As IEnumerator = oDataSet.Tables(j).Rows.GetEnumerator
                While oEnum.MoveNext
                    'While DR.Read()
                    DBTable_Summary.LoadDataRow(tableSchema, True)
                    DBTable_Summary.Rows(i)(0) = oEnum.Current.Item("Type")
                    DBTable_Summary.Rows(i)(1) = oEnum.Current.Item("Description")
                    If Not IsDBNull(oEnum.Current.Item("Value")) Then
                        DBTable_Summary.Rows(i)(2) = oEnum.Current.Item("Value")
                    Else
                        DBTable_Summary.Rows(i)(2) = 0
                    End If
                    DBTable_Summary.Rows(i)(3) = oEnum.Current.Item("SummType")
                    DBTable_Summary.Rows(i)(4) = oEnum.Current.Item("ClientID")
                    DBTable_Summary.Rows(i)(5) = oEnum.Current.Item("PropertyID")
                    DBTable_Summary.Rows(i)(6) = oEnum.Current.Item("UnitID")
                    If Not IsDBNull(oEnum.Current.Item("FromID")) Then
                        DBTable_Summary.Rows(i)(7) = oEnum.Current.Item("FromID")
                    Else
                        DBTable_Summary.Rows(i)(7) = 0
                    End If
                    DBTable_Summary.Rows(i)(8) = oEnum.Current.Item("AssetID")
                    i += 1
                    'DR.NextResult()
                    'End While
                    'DR.Close()
                End While
            End With
        Next
        dstSummary.Tables.Add(DBTable_Summary)     'Convert To DataSet
        xmlProducts.Document = New XmlDataDocument(dstSummary)
        xmlProducts.TransformSource = "SystemSummary.xsl"

    End Function

    Public Function GetCompanyItemType(ByVal compData As String) As ItemType

        Dim oItemType As ItemType = ItemType.Unknown

        Try

            Dim oTagData() As String = CType(compData, String).Split("-"c)
            Dim intClientID = oTagData(0)
            Dim intPropertyID = oTagData(1)
            Dim intUnitID = oTagData(2)
            Dim intAssetID = oTagData(3)
            Dim intFileID = oTagData(4)

            If intFileID = 0 And intAssetID <> 0 Then   'It is an Asset
                oItemType = ItemType.Asset
            ElseIf intFileID <> 0 Then                  'It is a File
                oItemType = ItemType.File
            ElseIf intUnitID <> 0 Then                  'It is a Unit
                oItemType = ItemType.Unit
            ElseIf intPropertyID <> 0 Then              'It is a Property
                oItemType = ItemType.Property
            ElseIf intClientID <> 0 Then                'It is a Client
                oItemType = ItemType.Client
            Else
                oItemType = ItemType.Unknown
            End If

        Catch ex As Exception

        End Try


        Return oItemType

    End Function


    Public Function GetItemType(ByVal tag As String, ByRef param As Object) As ItemType

        Dim oTagDatas() As String = CType(tag, String).Split(":"c)

        Select Case oTagDatas(0)
            Case "HEX"
                param = New Object() {0, 0, 0, 0}
                Return ItemType.System
            Case "CALL"
                param = New Object() {oTagDatas(1)}
                Return ItemType.Call
            Case "SUP"
                param = New Object() {0, 0, 0, 0, oTagDatas(1)}
                Return ItemType.Supplier
            Case "COMP"
                param = CType(oTagDatas(1), String).Split("-"c)
                Return GetCompanyItemType(oTagDatas(1))
            Case "ASSE"
                param = CType(oTagDatas(1), String).Split("-"c)
                Return ItemType.Asset
            Case "CON"
                param = New Object() {0, 0, 0, 0, oTagDatas(1)}
                Return ItemType.Contact
        End Select

    End Function

End Class
