<%@ Page Language="vb" AutoEventWireup="false" Codebehind="NewSystemSummary.aspx.vb" Inherits="HelpdeskWeb.NewSystemSummary"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
  <HEAD>
		<title>New System Summary</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK rel="stylesheet" type="text/css" href="hexagon.css">
  </HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<IMG src="Hexagon.gif">
			<TABLE id="Table1" align="center" cellSpacing="1" cellPadding="1" width="600" height="600"
				border="0">
				<TR>
					<TD style="HEIGHT: 524px"></TD>
					<TD align="center" style="BACKGROUND-POSITION:center 50%; BACKGROUND-IMAGE:url(HexagonTable.jpg); BACKGROUND-REPEAT:no-repeat; HEIGHT:524px">
						<asp:Xml id="XMLINFO" runat="server" TransformSource="BasicInfo.xsl" DocumentSource="data.xml"></asp:Xml>
					<br/>
						<asp:Xml id="xmlProducts" runat="server" DocumentSource="data.xml" TransformSource="datatable.xsl"></asp:Xml></TD>
					<TD style="HEIGHT: 524px"></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD></TD>
					<TD></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
