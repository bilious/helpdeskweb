Imports System.Xml
Imports System.Data.SqlClient
Imports System.Xml.Xsl
Imports System.IO

Public Class SearchResults
    Inherits System.Web.UI.Page
    Dim SearchText As String = ""
    Protected WithEvents DataGrid1 As System.Web.UI.WebControls.DataGrid
    Protected WithEvents DataGrid2 As System.Web.UI.WebControls.DataGrid
    Protected WithEvents DataGrid3 As System.Web.UI.WebControls.DataGrid
    Protected WithEvents DataGrid4 As System.Web.UI.WebControls.DataGrid
    Protected WithEvents DataGrid5 As System.Web.UI.WebControls.DataGrid
    Protected WithEvents Datagrid6 As System.Web.UI.WebControls.DataGrid
    Dim Sql As String = ""
    
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents xmlProducts As System.Web.UI.WebControls.Xml
    Protected WithEvents Form1 As System.Web.UI.HtmlControls.HtmlForm

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        SearchText = Request("SearchText")

        Sql = "SELECT tag, EXTREF, ClientID, ClientName, Telephone, Contact From View_Client_EXT Where ClientName Like '%" & SearchText & "%' OR EXTREF = '" & SearchText & "';" & _
        "SELECT tag, ExtRef, PropertyName, PropertyID,ClientID, ClientName, Manager, SectorReference From View_Property_EXT Where PropertyName Like '%" & SearchText & "%' OR EXTREF = '" & SearchText & "';" & _
        "SELECT tag, AssetID, Description, Category, EXTREF, Manufacturer, PropertyName, SupplierName From View_Asset_EXT Where Description Like '%" & SearchText & "%' Or ModelNo Like '%" & SearchText & "%'; " & _
        "SELECT SupplierID, SupplierName, Contact, SupplierTelephone, Category From View_Supplier_EXT Where SupplierName Like '%" & SearchText & "%';" & _
        "SELECT ContactID, Title+' '+FirstName+' '+LastName As ContactName, Company, [Position],ClientName, PropertyName,Telephone FROM View_Contact_EXT Where FirstName + ' ' + LastName Like '%" & SearchText & "%' or TenantName Like '%" & SearchText & "%';" & _
 "SELECT tag, ExtRef, UnitName, UnitID,ClientID, ClientName, TenantName, TenantTelephone, TenantEmail,UnitManager as Manager From View_Unit_EXT Where UnitName Like '%" & SearchText & "%' OR EXTREF = '" & SearchText & "';"

        Dim dadCall As New SqlDataAdapter
        Dim dstResults As New DataSet
        Dim cmdCall As SqlCommand
        'Dim DB As New DataBase
        Dim Conn As SqlConnection
        Dim oDataAccessor As New DataAccessor
        oDataAccessor.TakeConnection(Conn)
        'Conn = DB.OpenConnection()

        '---Check If This Is A Call ID First ---
        Dim intIncidence As Int32 = oDataAccessor.ScalarQuery("Select IncidentID from tbl_Incidence Where UPPER(IncidentName)= '" & UCase(SearchText) & "'") '"
        If Not IsDBNull(intIncidence) Then
            If intIncidence > 0 Then
                'Found a matching call
                Response.Redirect("CallDetails.aspx?CallID=" & intIncidence)
                Exit Sub
            End If
        End If

        cmdCall = New SqlCommand(Sql, Conn)
        dadCall.SelectCommand = cmdCall
        dadCall.Fill(dstResults)
        dstResults.Tables(0).TableName = "Clients"
        dstResults.Tables(1).TableName = "Properties"
        dstResults.Tables(2).TableName = "Assets"
        dstResults.Tables(3).TableName = "Suppliers"
        dstResults.Tables(4).TableName = "Contacts"
        dstResults.Tables(5).TableName = "Units"
        DataGrid1.DataSource = dstResults
        DataGrid1.DataMember = "Clients"
        DataGrid1.DataBind()
        DataGrid2.DataSource = dstResults
        DataGrid2.DataMember = "Properties"
        DataGrid2.DataBind()
        DataGrid3.DataSource = dstResults
        DataGrid3.DataMember = "Assets"
        DataGrid3.DataBind()
        DataGrid4.DataSource = dstResults
        DataGrid4.DataMember = "Suppliers"
        DataGrid4.DataBind()
        DataGrid5.DataSource = dstResults
        DataGrid5.DataMember = "Contacts"
        DataGrid5.DataBind()
        Datagrid6.DataSource = dstResults
        Datagrid6.DataMember = "Units"
        Datagrid6.DataBind()

        ' Pass DataSet for XSL Transform
        'Dim str As New StringWriter

        'dstSummary.WriteXml(str, XmlWriteMode.IgnoreSchema)
        'Response.Write(str.ToString)

        xmlProducts.Document = New XmlDataDocument(dstResults)
        xmlProducts.TransformSource = "SearchResults.xsl"

    End Sub

End Class
