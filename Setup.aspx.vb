Imports System.Xml
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.Xml.Xsl
Imports System.IO
Imports System.Text
Imports Common

Public Class Setup
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ImageButton1 As System.Web.UI.WebControls.ImageButton
    Protected WithEvents Imagebutton2 As System.Web.UI.WebControls.ImageButton
    Protected WithEvents Imagebutton3 As System.Web.UI.WebControls.ImageButton
    Protected WithEvents txtdbName As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtUser As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtPassword As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblStatus As System.Web.UI.WebControls.Label
    Protected WithEvents txtSAName As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtSAPassword As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblInfo As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Dim ApplicationServer As String = ""
    Dim DatabaseName As String = ""
    Dim Username As String = ""
    Dim Password As String = ""
    Private oDataAccessor As New DataAccessor

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not Page.IsPostBack Then
            If GlobalVariables.DBConnStr = "" Then
                Response.Redirect("Error.aspx")
            End If
        End If
    End Sub

    Private Sub ImageButton1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Dim strquery As String = ""
        If txtdbName.Text = "" Then
            lblStatus.Text = "Please Fill Out All Fields"
            Exit Sub
        End If
        If txtUser.Text = "" Then
            lblStatus.Text = "Please Fill Out All Fields"
            Exit Sub
        End If
        If txtPassword.Text = "" Then
            lblStatus.Text = "Please Fill Out All Fields"
            Exit Sub
        End If
        '========================
        Try
            DoSQLQuery("Create Database " & txtdbName.Text)
            DoSQLQuery("EXEC(sp_addlogin) '" & txtUser.Text & "','" & txtPassword.Text & "', '" & txtdbName.Text & "'")
            DoSQLQuery("Use " & txtdbName.Text)
            DoSQLQuery("EXEC(sp_adduser) '" & txtUser.Text & "'")
            DoSQLQuery("EXEC(sp_addrolemember) 'db_owner', '" & txtUser.Text & "'")
            DoSQLQuery("EXEC(sp_adduser) 'SQSDBA'; EXEC(sp_addrolemember) 'db_owner', 'SQSDBA'")
            lblInfo.Text = "Database Created"
        Catch ex As Exception
            lblStatus.Text = ex.Message
        End Try
    End Sub
    Sub DoSQLQuery(ByVal command As String, Optional ByVal strDB As String = "")
        Dim oConnection As SqlConnection = Nothing
        Try
            If strDB <> "" Then
                oConnection = New SqlConnection("data source=" & Environment.MachineName & ";user id=" & txtSAName.Text & ";password=" & txtSAPassword.Text & "; initial catalog= " & strDB)
            Else
                oConnection = New SqlConnection("data source=" & Environment.MachineName & ";user id=" & txtSAName.Text & ";password=" & txtSAPassword.Text)
            End If
            oConnection.Open()
            Dim oSqlCommand As SqlCommand = oConnection.CreateCommand()
            oSqlCommand.CommandType = CommandType.Text
            oSqlCommand.CommandText = command
            oSqlCommand.ExecuteNonQuery()

        Catch ex As Exception
            lblStatus.Text = ex.Message
        Finally
            oConnection.Close()
        End Try
    End Sub

    Private Sub Imagebutton2_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imagebutton2.Click
        Dim strTableSQL As String = ""
        Dim oRead As System.IO.StreamReader
        Dim oFile As System.IO.File

        oRead = oFile.OpenText("C:\HexagonHelpDesk\HelpdeskWeb\tables.sql")
        strTableSQL = oRead.ReadToEnd.ToString
        oRead.Close()
        ' DoSQLQuery("Use " & txtdbName.Text)
        DoSQLQuery(strTableSQL, txtdbName.Text)
        lblInfo.Text = "Tables Created"

    End Sub
End Class
