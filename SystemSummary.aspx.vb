Imports System.Xml
Imports System.Data.SqlClient
Imports System.Xml.Xsl
Imports System.IO

Public Class SystemSummary
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents xmlProducts As System.Web.UI.WebControls.Xml

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private SummaryID As Integer = 0
    Private ClientRefID As Long = 0
    Private PropertyID As Long = 0
    Private UnitID As Long = 0
    Private AssetID As Long = 0
    Private EngineerID As Long = 0
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        '--------------------------------------------------------------------------
        SummaryID = Request("SummaryID")
        ClientRefID = Request("ClientRefID")
        PropertyID = Request("PropertyID")
        UnitID = Request("UnitID")
        AssetID = Request("AssetID")
        EngineerID = Request("EngineerID")
        'SummaryID = 3
        'ClientRefID = 1
        'PropertyID = 1
        'UnitID = 1

        '--------------------------------------------------------------------------
        '// Craete DataTable
        Dim TableSummarySchema(7) As String
        TableSummarySchema(0) = "Type"
        TableSummarySchema(1) = "Description"
        TableSummarySchema(2) = "Value"
        TableSummarySchema(3) = "SummType"
        TableSummarySchema(4) = "ClientID"
        TableSummarySchema(5) = "PropertyID"
        TableSummarySchema(6) = "UnitID"
        TableSummarySchema(7) = "EngineerID"

        Dim DBTable_Summary As New DataTable("SummaryDetails")
        DBTable_Summary.Columns.Clear()
        Dim i As Integer = 0

        For i = 0 To 7                              'Create Data Table Columns
            Dim DBFD As New DataColumn(TableSummarySchema(i))
            DBFD.DefaultValue = ""
            DBFD.DataType = System.Type.GetType("System.String")
            DBTable_Summary.Columns.Add(DBFD)
        Next
        '--------------------------------------------------------------------------
        'Dim DB As New DataBase
        Dim DR As SqlDataReader
        Dim Conn As SqlConnection
        Dim dstSummary As New DataSet
        Dim SQL As String = ""
        Dim SQLWHERE As String = ""
        Dim oDataAccessor As New DataAccessor
        oDataAccessor.TakeConnection(Conn)
        'Conn = DB.OpenConnection()
        '--------------------------------------------------------------------------
        Select Case SummaryID

            Case 1          '   SYSTEM SUMMARY
                '-----------------------------------------------------------------------
                '// SQL QUERIES TO RETRIEVE SUMMARY DATA
                '// SOME QUERIES ARE YET TO BE CHANGED
                '----------- GET Open Calls, Incident.StatusID=1
                SQL = "select 'OPEN CALLS' AS Type, 'Open Calls' AS Description,COUNT(*) AS Value,1 as SummType,0 as ClientID,0 As PropertyID,0 as UnitID From tbl_incident where StatusID=1;"
                '----------- GET Assigned To Engineer
                SQL += "select 'OPEN CALLS' AS Type, 'Assigned To Engineer' AS Description,COUNT(*) AS Value,2 as SummType,0 as ClientID,0 As PropertyID,0 as UnitID From tbl_incident where StatusID=1 And SupplierID>0;"
                '----------- GET Awaiting Parts (QUERY TO UPDATE)
                SQL += "select 'OPEN CALLS' AS Type, 'Awaiting Parts' AS Description,COUNT(*) AS Value,3 as SummType,0 as ClientID,0 As PropertyID,0 as UnitID From tbl_incident where StatusID=1;"
                '----------- GET Scheduled Calls (QUERY TO UPDATE)
                SQL += "select 'OPEN CALLS' AS Type, 'Scheduled Calls' AS Description,COUNT(*) AS Value,4 as SummType,0 as ClientID,0 As PropertyID,0 as UnitID From tbl_incident where StatusID=1;"

                '----------- GET In Last 24 Hours (QUERY TO UPDATE)
                SQL += "select 'CLOSED CALLS' AS Type, 'In Last 24 Hours' AS Description,COUNT(*) AS Value,5 as SummType,0 as ClientID,0 As PropertyID,0 as UnitID From tbl_incident where StatusID=1;"
                '----------- GET Within SLAs (QUERY TO UPDATE)
                SQL += "select 'CLOSED CALLS' AS Type, 'Within SLAs' AS Description,COUNT(*) AS Value,6 as SummType,0 as ClientID,0 As PropertyID,0 as UnitID From tbl_incident where StatusID=1;"
                '----------- GET Outside SLAs (QUERY TO UPDATE)
                SQL += "select 'CLOSED CALLS' AS Type, 'Outside SLAs' AS Description,COUNT(*) AS Value,7 as SummType,0 as ClientID,0 As PropertyID,0 as UnitID From tbl_incident where StatusID=1;"

                '----------- GET Total Value Parts On Order (QUERY TO UPDATE)
                SQL += "select 'PAYMENTS' AS Type, 'Total Value Parts On Order' AS Description,COUNT(*) AS Value,8 as SummType,0 as ClientID,0 As PropertyID,0 as UnitID From tbl_incident where StatusID=1;"
                '--------------------------------------------------------------------------
                ' Populate DataTable with Results
                'DR = DB.ExecuteDataReader(Conn, SQL)
                Dim oSqlCommand As New SqlCommand(SQL, Conn)
                DR = oSqlCommand.ExecuteReader()

                i = 0
                While DR.Read()
                    DBTable_Summary.LoadDataRow(TableSummarySchema, True)
                    DBTable_Summary.Rows(i)(0) = DR.Item("Type")
                    DBTable_Summary.Rows(i)(1) = DR.Item("Description")
                    If Not IsDBNull(DR.Item("Value")) Then
                        DBTable_Summary.Rows(i)(2) = DR.Item("Value")
                    Else
                        DBTable_Summary.Rows(i)(2) = 0
                    End If
                    DBTable_Summary.Rows(i)(3) = DR.Item("SummType")
                    DBTable_Summary.Rows(i)(4) = DR.Item("ClientID")
                    DBTable_Summary.Rows(i)(5) = DR.Item("PropertyID")
                    DBTable_Summary.Rows(i)(6) = DR.Item("UnitID")

                    i += 1
                    DR.NextResult()
                End While
                DR.Close()
                Conn.Close()

                dstSummary.Tables.Add(DBTable_Summary)     'Convert To DataSet
                '--------------------------------------------------------------------------
                ' Pass DataSet for XSL Transform
                'Dim str As New StringWriter
                'dstSummary.WriteXml(str, XmlWriteMode.IgnoreSchema)
                'Response.Write(str.ToString)

                xmlProducts.Document = New XmlDataDocument(dstSummary)
                xmlProducts.TransformSource = "SystemSummary.xsl"

            Case 2          ' Client Summary
                '-----------------------------------------------------------------------
                '// SQL QUERIES TO RETRIEVE SUMMARY DATA
                '// SOME QUERIES ARE YET TO BE CHANGED
                '----------- GET Properties Summary
                SQL = "select 'Properties' AS Type, 'Properties' AS Description,COUNT(*) AS Value,21 as SummType," & ClientRefID & " as ClientID,0 as PropertyID,0 as UnitID From tbl_Property where ClientID=" & ClientRefID & ";"
                '----------- GET Units Summary
                SQL += "select 'Properties' AS Type, 'Units' AS Description,COUNT(*) AS Value,22 as SummType," & ClientRefID & " as ClientID,0 as PropertyID,0 as UnitID From tbl_Unit where ClientID=" & ClientRefID & ";"

                '----------- GET Open Call Summary
                SQL += "select 'CALLS' AS Type, 'Open Calls' AS Description,COUNT(*) AS Value,1 as SummType," & ClientRefID & " as ClientID,0 as PropertyID,0 as UnitID From tbl_incident where StatusID=1 And ClientID=" & ClientRefID & ";"
                '----------- GET In Last 24 Hours (QUERY TO UPDATE)
                SQL += "select 'CALLS' AS Type, 'In Last 24 Hours' AS Description,COUNT(*) AS Value,5 as SummType," & ClientRefID & " as ClientID,0 as PropertyID,0 as UnitID From tbl_incident where StatusID=1 And ClientID=" & ClientRefID & ";"
                '----------- GET Scheduled Calls (QUERY TO UPDATE)
                SQL += "select 'CALLS' AS Type, 'Scheduled Calls' AS Description,COUNT(*) AS Value,4 as SummType," & ClientRefID & " as ClientID,0 as PropertyID,0 as UnitID From tbl_incident where StatusID=1 And ClientID=" & ClientRefID & ";"
                '----------- GET Total Calls 
                SQL += "select 'CALLS' AS Type, 'Total Calls' AS Description,COUNT(*) AS Value,23 as SummType," & ClientRefID & " as ClientID,0 as PropertyID,0 as UnitID From tbl_incident where ClientID=" & ClientRefID

                '--------------------------------------------------------------------------
                ' Populate DataTable with Results
                'DR = DB.ExecuteDataReader(Conn, SQL)
                Dim oSqlCommand As New SqlCommand(SQL, Conn)
                DR = oSqlCommand.ExecuteReader()

                i = 0
                While DR.Read()
                    DBTable_Summary.LoadDataRow(TableSummarySchema, True)
                    DBTable_Summary.Rows(i)(0) = DR.Item("Type")
                    DBTable_Summary.Rows(i)(1) = DR.Item("Description")
                    If Not IsDBNull(DR.Item("Value")) Then
                        DBTable_Summary.Rows(i)(2) = DR.Item("Value")
                    Else
                        DBTable_Summary.Rows(i)(2) = 0
                    End If
                    DBTable_Summary.Rows(i)(3) = DR.Item("SummType")
                    DBTable_Summary.Rows(i)(4) = DR.Item("ClientID")
                    DBTable_Summary.Rows(i)(5) = DR.Item("PropertyID")
                    DBTable_Summary.Rows(i)(6) = DR.Item("UnitID")

                    i += 1
                    DR.NextResult()
                End While
                DR.Close()
                Conn.Close()

                dstSummary.Tables.Add(DBTable_Summary)     'Convert To DataSet
                '--------------------------------------------------------------------------
                ' Pass DataSet for XSL Transform
                xmlProducts.Document = New XmlDataDocument(dstSummary)
                xmlProducts.TransformSource = "SystemSummary.xsl"

            Case 3          ' Property Summary
                '-----------------------------------------------------------------------
                '// SQL QUERIES TO RETRIEVE SUMMARY DATA
                '// SOME QUERIES ARE YET TO BE CHANGED
                SQLWHERE = "ClientID=" & ClientRefID & " And PropertyID=" & PropertyID
                '----------- GET Units Summary
                SQL = "select 'Properties' AS Type, 'Units' AS Description,COUNT(*) AS Value,31 as SummType," & ClientRefID & " as ClientID," & PropertyID & " as PropertyID,0 as UnitID From tbl_Unit where " & SQLWHERE & ";"
                '----------- GET Assets Summary
                SQL += "select 'Properties' AS Type, 'Assets' AS Description,COUNT(*) AS Value,32 as SummType," & ClientRefID & " as ClientID," & PropertyID & " as PropertyID,0 as UnitID From tbl_Asset where " & SQLWHERE & ";"

                '----------- GET Open Call Summary
                SQL += "select 'CALLS' AS Type, 'Open Calls' AS Description,COUNT(*) AS Value,1 as SummType," & ClientRefID & " as ClientID," & PropertyID & " as PropertyID,0 as UnitID From tbl_incident where StatusID=1 And " & SQLWHERE & ";"
                '----------- GET In Last 24 Hours (QUERY TO UPDATE)
                SQL += "select 'CALLS' AS Type, 'In Last 24 Hours' AS Description,COUNT(*) AS Value,5 as SummType," & ClientRefID & " as ClientID," & PropertyID & "  as PropertyID,0 as UnitID From tbl_incident where StatusID=1 And " & SQLWHERE & ";"
                '----------- GET Scheduled Calls (QUERY TO UPDATE)
                SQL += "select 'CALLS' AS Type, 'Scheduled Calls' AS Description,COUNT(*) AS Value,4 as SummType," & ClientRefID & " as ClientID," & PropertyID & "  as PropertyID,0 as UnitID From tbl_incident where StatusID=1 And " & SQLWHERE & ";"
                '----------- GET Total Calls 
                SQL += "select 'CALLS' AS Type, 'Total Calls' AS Description,COUNT(*) AS Value,23 as SummType," & ClientRefID & " as ClientID," & PropertyID & "  as PropertyID,0 as UnitID From tbl_incident where " & SQLWHERE

                '--------------------------------------------------------------------------
                ' Populate DataTable with Results
                'DR = DB.ExecuteDataReader(Conn, SQL)
                Dim oSqlCommand As New SqlCommand(SQL, Conn)
                DR = oSqlCommand.ExecuteReader()

                i = 0
                While DR.Read()
                    DBTable_Summary.LoadDataRow(TableSummarySchema, True)
                    DBTable_Summary.Rows(i)(0) = DR.Item("Type")
                    DBTable_Summary.Rows(i)(1) = DR.Item("Description")
                    If Not IsDBNull(DR.Item("Value")) Then
                        DBTable_Summary.Rows(i)(2) = DR.Item("Value")
                    Else
                        DBTable_Summary.Rows(i)(2) = 0
                    End If
                    DBTable_Summary.Rows(i)(3) = DR.Item("SummType")
                    DBTable_Summary.Rows(i)(4) = DR.Item("ClientID")
                    DBTable_Summary.Rows(i)(5) = DR.Item("PropertyID")
                    DBTable_Summary.Rows(i)(6) = DR.Item("UnitID")

                    i += 1
                    DR.NextResult()
                End While
                DR.Close()
                Conn.Close()

                dstSummary.Tables.Add(DBTable_Summary)     'Convert To DataSet
                '--------------------------------------------------------------------------
                ' Pass DataSet for XSL Transform
                xmlProducts.Document = New XmlDataDocument(dstSummary)
                xmlProducts.TransformSource = "SystemSummary.xsl"

            Case 4          ' Unit Summary
                '-----------------------------------------------------------------------
                '// SQL QUERIES TO RETRIEVE SUMMARY DATA
                '// SOME QUERIES ARE YET TO BE CHANGED
                SQLWHERE = "ClientID=" & ClientRefID & " And PropertyID=" & PropertyID & " And UnitID=" & UnitID
                '----------- GET Open Call Summary
                SQL = "select 'CALLS' AS Type, 'Open Calls' AS Description,COUNT(*) AS Value,1 as SummType," & ClientRefID & " as ClientID," & PropertyID & " as PropertyID," & UnitID & " as UnitID From tbl_incident where StatusID=1 And " & SQLWHERE & ";"
                '----------- GET Scheduled Calls (QUERY TO UPDATE)
                SQL += "select 'CALLS' AS Type, 'Scheduled Calls' AS Description,COUNT(*) AS Value,4 as SummType," & ClientRefID & " as ClientID," & PropertyID & " as PropertyID," & UnitID & " as UnitID From tbl_incident where StatusID=1 And " & SQLWHERE & ";"

                '----------- GET In Last 12 Months (QUERY TO UPDATE)
                SQL += "select 'CALLS' AS Type, 'Calls In Last 12 Months' AS Description,COUNT(*) as Value,45 as SummType," & ClientRefID & " as ClientID," & PropertyID & " as PropertyID," & UnitID & " as UnitID From tbl_incident where StatusID=1 And " & SQLWHERE & ";"
                '----------- GET Total Calls 
                SQL += "select 'CALLS' AS Type, 'Total Calls' AS Description,COUNT(*) as Value,23 as SummType," & ClientRefID & " as ClientID," & PropertyID & " as PropertyID," & UnitID & " as UnitID From tbl_incident where " & SQLWHERE & ";"

                '----------- GET Total Cost This Year (QUERY TO UPDATE)
                SQL += "select 'CALLS' AS Type, 'Total Cost This Year' AS Description,COUNT(*) AS Value,46 as SummType," & ClientRefID & " as ClientID," & PropertyID & " as PropertyID," & UnitID & " as UnitID From tbl_incident where StatusID=1 And " & SQLWHERE & ";"
                '----------- GET Total No. of Hours This Year 
                SQL += "select 'CALLS' AS Type, 'Total No. of Hours This Year' AS Description,COUNT(*) AS Value,46 as SummType," & ClientRefID & " as ClientID," & PropertyID & " as PropertyID," & UnitID & " as UnitID From tbl_incident where " & SQLWHERE & ";"

                '--------------------------------------------------------------------------
                ' Populate DataTable with Results
                'DR = DB.ExecuteDataReader(Conn, SQL)
                Dim oSqlCommand As New SqlCommand(SQL, Conn)
                DR = oSqlCommand.ExecuteReader()

                i = 0
                While DR.Read()
                    DBTable_Summary.LoadDataRow(TableSummarySchema, True)
                    DBTable_Summary.Rows(i)(0) = DR.Item("Type")
                    DBTable_Summary.Rows(i)(1) = DR.Item("Description")
                    If Not IsDBNull(DR.Item("Value")) Then
                        DBTable_Summary.Rows(i)(2) = DR.Item("Value")
                    Else
                        DBTable_Summary.Rows(i)(2) = 0
                    End If
                    DBTable_Summary.Rows(i)(3) = DR.Item("SummType")
                    DBTable_Summary.Rows(i)(4) = DR.Item("ClientID")
                    DBTable_Summary.Rows(i)(5) = DR.Item("PropertyID")
                    DBTable_Summary.Rows(i)(6) = DR.Item("UnitID")

                    i += 1
                    DR.NextResult()
                End While
                DR.Close()
                Conn.Close()

                dstSummary.Tables.Add(DBTable_Summary)     'Convert To DataSet
                '--------------------------------------------------------------------------
                ' Pass DataSet for XSL Transform
                xmlProducts.Document = New XmlDataDocument(dstSummary)
                xmlProducts.TransformSource = "SystemSummary.xsl"
            Case 5 ' Asset Summary
                '-----------------------------------------------------------------------
                '// SQL QUERIES TO RETRIEVE SUMMARY DATA
                '// SOME QUERIES ARE YET TO BE CHANGED
                SQLWHERE = "ClientID=" & ClientRefID & " And PropertyID=" & PropertyID & " And UnitID=" & UnitID & " AND AssetID=" & AssetID
                '----------- GET Open Call Summary
                SQL = "select 'CALLS' AS Type, 'Open Calls' AS Description,COUNT(*) AS Value,1 as SummType," & ClientRefID & " as ClientID," & PropertyID & " as PropertyID," & UnitID & " as UnitID From tbl_incident where StatusID=1 And " & SQLWHERE & ";"
                '----------- GET Scheduled Calls (QUERY TO UPDATE)
                SQL += "select 'CALLS' AS Type, 'Scheduled Calls' AS Description,COUNT(*) AS Value,4 as SummType," & ClientRefID & " as ClientID," & PropertyID & " as PropertyID," & UnitID & " as UnitID From tbl_incident where StatusID=1 And " & SQLWHERE & ";"

                '----------- GET In Last 12 Months (QUERY TO UPDATE)
                SQL += "select 'CALLS' AS Type, 'Calls In Last 12 Months' AS Description,COUNT(*) as Value,45 as SummType," & ClientRefID & " as ClientID," & PropertyID & " as PropertyID," & UnitID & " as UnitID From tbl_incident where StatusID=1 And " & SQLWHERE & ";"
                '----------- GET Total Calls 
                SQL += "select 'CALLS' AS Type, 'Total Calls' AS Description,COUNT(*) as Value,23 as SummType," & ClientRefID & " as ClientID," & PropertyID & " as PropertyID," & UnitID & " as UnitID From tbl_incident where " & SQLWHERE & ";"

                '----------- GET Total Cost This Year (QUERY TO UPDATE)
                SQL += "select 'CALLS' AS Type, 'Total Cost This Year' AS Description,COUNT(*) AS Value,46 as SummType," & ClientRefID & " as ClientID," & PropertyID & " as PropertyID," & UnitID & " as UnitID From tbl_incident where StatusID=1 And " & SQLWHERE & ";"
                '----------- GET Total No. of Hours This Year 
                SQL += "select 'CALLS' AS Type, 'Total No. of Hours This Year' AS Description,COUNT(*) AS Value,46 as SummType," & ClientRefID & " as ClientID," & PropertyID & " as PropertyID," & UnitID & " as UnitID From tbl_incident where " & SQLWHERE & ";"

                '--------------------------------------------------------------------------
                ' Populate DataTable with Results
                'DR = DB.ExecuteDataReader(Conn, SQL)
                Dim oSqlCommand As New SqlCommand(SQL, Conn)
                DR = oSqlCommand.ExecuteReader()

                i = 0
                While DR.Read()
                    DBTable_Summary.LoadDataRow(TableSummarySchema, True)
                    DBTable_Summary.Rows(i)(0) = DR.Item("Type")
                    DBTable_Summary.Rows(i)(1) = DR.Item("Description")
                    If Not IsDBNull(DR.Item("Value")) Then
                        DBTable_Summary.Rows(i)(2) = DR.Item("Value")
                    Else
                        DBTable_Summary.Rows(i)(2) = 0
                    End If
                    DBTable_Summary.Rows(i)(3) = DR.Item("SummType")
                    DBTable_Summary.Rows(i)(4) = DR.Item("ClientID")
                    DBTable_Summary.Rows(i)(5) = DR.Item("PropertyID")
                    DBTable_Summary.Rows(i)(6) = DR.Item("UnitID")

                    i += 1
                    DR.NextResult()
                End While
                DR.Close()
                Conn.Close()

                dstSummary.Tables.Add(DBTable_Summary)     'Convert To DataSet
                '--------------------------------------------------------------------------
                ' Pass DataSet for XSL Transform
                xmlProducts.Document = New XmlDataDocument(dstSummary)
                xmlProducts.TransformSource = "SystemSummary.xsl"
            Case 6
                '-----------------------------------------------------------------------
                '// SQL QUERIES TO RETRIEVE SUMMARY DATA
                '// SOME QUERIES ARE YET TO BE CHANGED
                SQLWHERE = "EngineerID=" & EngineerID
                '----------- GET Open Call Summary
                SQL = "select 'CALLS' AS Type, 'Open Calls' AS Description,COUNT(*) AS Value,1 as SummType," & ClientRefID & " as ClientID," & PropertyID & " as PropertyID," & UnitID & " as UnitID From tbl_incident where StatusID=1 And " & SQLWHERE & ";"
                '----------- GET Scheduled Calls (QUERY TO UPDATE)
                SQL += "select 'CALLS' AS Type, 'Scheduled Calls' AS Description,COUNT(*) AS Value,4 as SummType," & ClientRefID & " as ClientID," & PropertyID & " as PropertyID," & UnitID & " as UnitID From tbl_incident where StatusID=1 And " & SQLWHERE & ";"

                '----------- GET In Last 12 Months (QUERY TO UPDATE)
                SQL += "select 'CALLS' AS Type, 'Calls In Last 12 Months' AS Description,COUNT(*) as Value,45 as SummType," & ClientRefID & " as ClientID," & PropertyID & " as PropertyID," & UnitID & " as UnitID From tbl_incident where StatusID=1 And " & SQLWHERE & ";"
                '----------- GET Total Calls 
                SQL += "select 'CALLS' AS Type, 'Total Calls' AS Description,COUNT(*) as Value,23 as SummType," & ClientRefID & " as ClientID," & PropertyID & " as PropertyID," & UnitID & " as UnitID From tbl_incident where " & SQLWHERE & ";"

                '----------- GET Total Cost This Year (QUERY TO UPDATE)
                SQL += "select 'CALLS' AS Type, 'Total Cost This Year' AS Description,COUNT(*) AS Value,46 as SummType," & ClientRefID & " as ClientID," & PropertyID & " as PropertyID," & UnitID & " as UnitID From tbl_incident where StatusID=1 And " & SQLWHERE & ";"
                '----------- GET Total No. of Hours This Year 
                SQL += "select 'CALLS' AS Type, 'Total No. of Hours This Year' AS Description,COUNT(*) AS Value,46 as SummType," & ClientRefID & " as ClientID," & PropertyID & " as PropertyID," & UnitID & " as UnitID From tbl_incident where " & SQLWHERE & ";"

                '--------------------------------------------------------------------------
                ' Populate DataTable with Results
                'DR = DB.ExecuteDataReader(Conn, SQL)
                Dim oSqlCommand As New SqlCommand(SQL, Conn)
                DR = oSqlCommand.ExecuteReader()

                i = 0
                While DR.Read()
                    DBTable_Summary.LoadDataRow(TableSummarySchema, True)
                    DBTable_Summary.Rows(i)(0) = DR.Item("Type")
                    DBTable_Summary.Rows(i)(1) = DR.Item("Description")
                    If Not IsDBNull(DR.Item("Value")) Then
                        DBTable_Summary.Rows(i)(2) = DR.Item("Value")
                    Else
                        DBTable_Summary.Rows(i)(2) = 0
                    End If
                    DBTable_Summary.Rows(i)(3) = DR.Item("SummType")
                    DBTable_Summary.Rows(i)(4) = DR.Item("ClientID")
                    DBTable_Summary.Rows(i)(5) = DR.Item("PropertyID")
                    DBTable_Summary.Rows(i)(6) = DR.Item("UnitID")

                    i += 1
                    DR.NextResult()
                End While
                DR.Close()
                Conn.Close()

                dstSummary.Tables.Add(DBTable_Summary)     'Convert To DataSet
                '--------------------------------------------------------------------------
                ' Pass DataSet for XSL Transform
                xmlProducts.Document = New XmlDataDocument(dstSummary)
                xmlProducts.TransformSource = "SystemSummary.xsl"
        End Select

    End Sub
End Class
