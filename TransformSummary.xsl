<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  >
  <xsl:template match="SummaryDetails">
  <center>
    <table width="70%" border="0"
      cellpadding="1" cellspacing="0" class="normal">
      <tr>
        <td width="80%">
          <xsl:value-of select="Description" /> 
        </td>

        <td width="20%">
          <b><xsl:value-of select="Value" /> </b>
        </td>
      </tr>
   </table>
   </center>	
  </xsl:template>
 </xsl:stylesheet>
