<%@ Page Language="vb" AutoEventWireup="false" Codebehind="default.aspx.vb" Inherits="HelpdeskWeb._default"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Hexagon Helpdesk Web</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="hexagon.css">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<IMG src="Hexagon.gif">
			<TABLE id="Table1" align="center" cellSpacing="1" cellPadding="1" width="600" height="600"
				border="0">
				<TR>
					<TD style="HEIGHT: 524px"></TD>
					<TD align="center" style="BACKGROUND-POSITION:center 50%; BACKGROUND-IMAGE:url(HexagonTable.jpg); BACKGROUND-REPEAT:no-repeat; HEIGHT:524px">
						<P>
							<asp:Label id="lblWarning" runat="server" ForeColor="Red"></asp:Label></P>
						<P>
							<asp:Label id="Label1" runat="server" Font-Names="Verdana">Welcome To Hexagon Helpdesk</asp:Label>
							<br>
						</P>
						<P>
							<asp:Label id="LabelVersion" runat="server" Font-Names="Verdana" Font-Size="XX-Small">Version 1.02</asp:Label></P>
						<P>
							<asp:Label id="lblLicencee" runat="server" ForeColor="DimGray" Font-Names="Verdana" Font-Size="X-Small"></asp:Label><br/>
							<asp:Label id="lblExpires" runat="server" ForeColor="Gray" Font-Names="Verdana" Font-Size="XX-Small"></asp:Label></P>
					</TD>
					<TD style="HEIGHT: 524px"></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD align="center">
						<asp:HyperLink id="HyperLink1" runat="server" Font-Names="Verdana" Font-Size="XX-Small" NavigateUrl="http://www.hexagonhelpdesk.co.uk">Helpdesk On The Web</asp:HyperLink>
						<br>
						<asp:HyperLink id="HyperLink2" runat="server" Font-Names="Verdana" Font-Size="XX-Small" NavigateUrl="http://www.hexagonsoftware.co.uk"
							Target="_blank">Hexagon Software</asp:HyperLink></TD>
					<TD></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
