CREATE TABLE [dbo].[About_ThisDB] (
	[CommentDate] [datetime] NOT NULL ,
	[Comment] [text] COLLATE Latin1_General_CI_AS NOT NULL ,
	[Author] [varchar] (50) COLLATE Latin1_General_CI_AS NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
;

CREATE TABLE [dbo].[App_Config] (
	[SetupID] [int] IDENTITY (1, 1) NOT NULL ,
	[Status] [int] NOT NULL ,
	[CallClosureDays] [int] NOT NULL ,
	[CallClosureStatus] [int] NOT NULL ,
	[RootPath] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[DreamServer] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[DreamDB] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[DreamCompany] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[DreamUsername] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[DreamPassword] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[DreamTimeDocType] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[DreamStockDocType] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[DreamInvoiceDocType] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[DreamTimeTransType] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[DreamStockTransType] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[DreamInvoiceTransType] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[DreamBudgetDocType] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[DreamBudgetTransType] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[DesktopURL] [varchar] (200) COLLATE Latin1_General_CI_AS NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[App_ErrorLog] (
	[ErrorID] [int] NOT NULL ,
	[ErrorDate] [datetime] NOT NULL ,
	[Type] [int] NULL ,
	[ErrorText] [varchar] (500) COLLATE Latin1_General_CI_AS NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[App_Registered_Machines] (
	[MachineName] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL ,
	[LicenceKey] [varchar] (500) COLLATE Latin1_General_CI_AS NOT NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[App_Sessions] (
	[SessionID] [int] IDENTITY (1, 1) NOT NULL ,
	[UserID] [int] NOT NULL ,
	[LoggedIn] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL ,
	[Type] [int] NOT NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[App_UserDetails] (
	[UserID] [int] IDENTITY (1, 1) NOT NULL ,
	[UserName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL ,
	[UserLoginID] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL ,
	[UserPassword] [varchar] (12) COLLATE Latin1_General_CI_AS NOT NULL ,
	[WindowsAccount] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[GroupID] [int] NOT NULL ,
	[RoleID] [int] NOT NULL ,
	[Status] [int] NOT NULL ,
	[AuthorisationLimit] [money] NOT NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[App_UserGroups] (
	[GroupID] [int] IDENTITY (1, 1) NOT NULL ,
	[GroupName] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL ,
	[Clients] [varchar] (500) COLLATE Latin1_General_CI_AS NOT NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[App_UserRoles] (
	[RoleID] [int] NOT NULL ,
	[UserRole] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL ,
	[UserGroupID] [int] NULL ,
	[CallTab] [int] NOT NULL ,
	[ActionTab] [int] NOT NULL ,
	[CostsTab] [int] NOT NULL ,
	[PartsTab] [int] NOT NULL ,
	[ChargesTab] [int] NOT NULL ,
	[InfoTab] [int] NOT NULL ,
	[WebTab] [int] NOT NULL ,
	[CallListTab] [int] NOT NULL ,
	[NotesTab] [int] NOT NULL ,
	[BPTab] [int] NOT NULL ,
	[AuditTab] [int] NOT NULL ,
	[BudgetTab] [int] NOT NULL ,
	[POTab] [int] NOT NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[Config_ActionStatus] (
	[ActionStatusID] [smallint] NOT NULL ,
	[ActionStatus] [varchar] (30) COLLATE Latin1_General_CI_AS NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[Config_ActionTitle] (
	[ActionID] [int] IDENTITY (1, 1) NOT NULL ,
	[ActionTitle] [varchar] (50) COLLATE Latin1_General_CI_AS NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[Config_AppointmentTypes] (
	[AppointmentTypeID] [int] IDENTITY (1, 1) NOT NULL ,
	[AppointmentType] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[Config_BudgetCate;ries] (
	[BudgetCate;ryID] [int] IDENTITY (1, 1) NOT NULL ,
	[Cate;ry] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL ,
	[Description] [varchar] (200) COLLATE Latin1_General_CI_AS NULL ,
	[ServiceCharge] [int] NOT NULL ,
	[DebitCode] [varchar] (20) COLLATE Latin1_General_CI_AS NULL ,
	[CreditCode] [varchar] (20) COLLATE Latin1_General_CI_AS NULL ,
	[ConCat] [tinyint] NOT NULL ,
	[TransType] [varchar] (20) COLLATE Latin1_General_CI_AS NOT NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[Config_CallReports] (
	[CallReportID] [int] IDENTITY (1, 1) NOT NULL ,
	[ReportName] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL ,
	[ReportPath] [varchar] (500) COLLATE Latin1_General_CI_AS NOT NULL ,
	[ReportSQL] [varchar] (500) COLLATE Latin1_General_CI_AS NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[Config_CallType] (
	[CallTypeID] [int] NOT NULL ,
	[CallTypePrefix] [varchar] (10) COLLATE Latin1_General_CI_AS NULL ,
	[CallTypeDesc] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[DefaultWebPage] [varchar] (500) COLLATE Latin1_General_CI_AS NULL ,
	[CallTab] [tinyint] NOT NULL ,
	[CostsTab] [tinyint] NOT NULL ,
	[ChargesTab] [tinyint] NOT NULL ,
	[PartsTab] [tinyint] NOT NULL ,
	[ActionsTab] [tinyint] NOT NULL ,
	[InfoTab] [tinyint] NOT NULL ,
	[NotesTab] [tinyint] NOT NULL ,
	[AuditTab] [tinyint] NOT NULL ,
	[CallListTab] [tinyint] NOT NULL ,
	[WebTab] [tinyint] NOT NULL ,
	[BPTab] [tinyint] NOT NULL ,
	[BudgetTab] [tinyint] NOT NULL ,
	[POTab] [tinyint] NOT NULL ,
	[ReportsTab] [tinyint] NOT NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[Config_CompanyStatus] (
	[StatusID] [int] NOT NULL ,
	[Description] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[Config_EventTypes] (
	[EventTypeID] [int] IDENTITY (1, 1) NOT NULL ,
	[EventName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL ,
	[EventDescription] [text] COLLATE Latin1_General_CI_AS NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
;

CREATE TABLE [dbo].[Config_IncidentStatus] (
	[StatusID] [int] IDENTITY (0, 1) NOT NULL ,
	[StatusDesc] [varchar] (30) COLLATE Latin1_General_CI_AS NOT NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[Config_InfoFields] (
	[CallTypeID] [int] NOT NULL ,
	[Label1] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Label2] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Label3] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Label4] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Label5] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Label6] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Label7] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Label8] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Label9] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Label10] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Label11] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Label12] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Label13] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Label14] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Label15] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Label16] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Label17] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Label18] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Label19] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Label20] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Field1] [int] NOT NULL ,
	[Field2] [int] NOT NULL ,
	[Field3] [int] NOT NULL ,
	[Field4] [int] NOT NULL ,
	[Field5] [int] NOT NULL ,
	[Field6] [int] NOT NULL ,
	[Field7] [int] NOT NULL ,
	[Field8] [int] NOT NULL ,
	[Field9] [int] NOT NULL ,
	[Field10] [int] NOT NULL ,
	[Field11] [int] NOT NULL ,
	[Field12] [int] NOT NULL ,
	[Field13] [int] NOT NULL ,
	[Field14] [int] NOT NULL ,
	[Field15] [int] NOT NULL ,
	[Field16] [int] NOT NULL ,
	[Field17] [int] NOT NULL ,
	[Field18] [int] NOT NULL ,
	[Field19] [int] NOT NULL ,
	[Field20] [int] NOT NULL ,
	[Value1] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[Value10] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[Value2] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[Value3] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[Value4] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[Value5] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[Value6] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[Value7] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[Value8] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[Value9] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[Value11] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[Value12] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[Value13] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[Value14] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[Value15] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[Value16] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[Value17] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[Value18] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[Value19] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[Value20] [varchar] (100) COLLATE Latin1_General_CI_AS NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[Config_OrderStatus] (
	[OrderStatusID] [int] NOT NULL ,
	[OrderStatusDesc] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[Config_Priority] (
	[PriorityId] [int] NOT NULL ,
	[PriorityDesc] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL ,
	[Affects] [char] (10) COLLATE Latin1_General_CI_AS NOT NULL ,
	[BusinessDays] [int] NOT NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[Config_ReportMaster] (
	[Status] [int] NOT NULL ,
	[DreamServer] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[DreamDB] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[DreamLo;nUser] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[DreamLo;nPassword] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[LandmarkServer] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[LandmarkDB] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[LandmarkLo;n] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[LandmarkPassword] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[BenchmarkServer] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[BenchmarkDB] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[BenchmarkLo;n] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[BenchmarkPassword] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[HelpdeskServer] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[HelpdeskDB] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[HelpdeskLo;n] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[HelpdeskPassword] [varchar] (50) COLLATE Latin1_General_CI_AS NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[Config_Status] (
	[StatusID] [int] NOT NULL ,
	[StatusDesc] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL ,
	[Affects] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[Config_SupplierType] (
	[TypeID] [int] IDENTITY (0, 1) NOT NULL ,
	[Description] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[Config_TaxCodes] (
	[TaxLineID] [int] IDENTITY (1, 1) NOT NULL ,
	[TaxName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL ,
	[TaxDescription] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[TaxRate] [varchar] (53) COLLATE Latin1_General_CI_AS NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[TBL_INCIDENCE] (
	[IncidentID] [int] IDENTITY (1, 1) NOT NULL ,
	[OpenSince] [datetime] NULL ,
	[ClosedAt] [datetime] NULL ,
	[CallTypeID] [int] NOT NULL ,
	[StatusID] [int] NOT NULL ,
	[CallID] [int] NOT NULL ,
	[CostsID] [int] NOT NULL ,
	[ChargesID] [int] NOT NULL ,
	[PartsID] [int] NOT NULL ,
	[ActionsID] [int] NOT NULL ,
	[InfoID] [int] NOT NULL ,
	[NotesID] [int] NOT NULL ,
	[AuditID] [int] NOT NULL ,
	[IncidentName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[CallListID] [int] NOT NULL ,
	[WebID] [int] NOT NULL ,
	[BudgetID] [int] NOT NULL ,
	[POID] [int] NOT NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[TBL_INCIDENCE_ACTION] (
	[ActionID] [int] IDENTITY (1, 1) NOT NULL ,
	[TotalTime] [int] NOT NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[TBL_INCIDENCE_ACTIONITEM] (
	[ActionItemID] [int] IDENTITY (1, 1) NOT NULL ,
	[ActionID] [int] NOT NULL ,
	[ActionTitle] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[ActionByDate] [datetime] NULL ,
	[AssignedSupplier] [int] NOT NULL ,
	[StatusID] [int] NOT NULL ,
	[PriorityID] [int] NOT NULL ,
	[Escalate] [tinyint] NOT NULL ,
	[Multiday] [tinyint] NOT NULL ,
	[TimeSpent] [float] NOT NULL ,
	[ActionRequiredNoteID] [int] NOT NULL ,
	[NoteID] [int] NOT NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[TBL_INCIDENCE_BUDGET] (
	[BudgetID] [int] IDENTITY (1, 1) NOT NULL ,
	[TotalBudget] [money] NOT NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[TBL_INCIDENCE_BUDGETITEM] (
	[BudgetItemID] [int] IDENTITY (1, 1) NOT NULL ,
	[BudgetID] [int] NOT NULL ,
	[ServiceCharge] [tinyint] NOT NULL ,
	[Approver] [int] NULL ,
	[Cate;ry] [int] NOT NULL ,
	[Reference] [varchar] (20) COLLATE Latin1_General_CI_AS NOT NULL ,
	[NetTotal] [float] NOT NULL ,
	[Schedule] [varchar] (20) COLLATE Latin1_General_CI_AS NULL ,
	[Period] [varchar] (20) COLLATE Latin1_General_CI_AS NULL ,
	[Expense] [varchar] (20) COLLATE Latin1_General_CI_AS NULL ,
	[NoteID] [int] NOT NULL ,
	[BudgetDate] [datetime] NULL ,
	[SentToDream] [tinyint] NOT NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[TBL_INCIDENCE_CALL] (
	[CallID] [int] IDENTITY (1, 1) NOT NULL ,
	[ClientID] [int] NULL ,
	[PropertyID] [int] NULL ,
	[UnitID] [int] NULL ,
	[AssetID] [int] NULL ,
	[LoggedBy] [int] NULL ,
	[PriorityID] [int] NULL ,
	[RespondByDate] [datetime] NULL ,
	[SupplierID] [int] NOT NULL ,
	[CallerPosition] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[ContactID] [int] NULL ,
	[ChargeTo] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[NoteID] [int] NOT NULL ,
	[Subject] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[EXT_Tenant] [int] NULL ,
	[CallerName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[CallerTel] [varchar] (50) COLLATE Latin1_General_CI_AS NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[TBL_INCIDENCE_CALLFILE] (
	[CallFileID] [int] IDENTITY (1, 1) NOT NULL ,
	[CallFileListID] [int] NOT NULL ,
	[UploaderUserID] [int] NOT NULL ,
	[FileID] [int] NOT NULL ,
	[Subject] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL ,
	[Note] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL ,
	[UploadDate] [datetime] NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[TBL_INCIDENCE_CALLFILELIST] (
	[CallFileListID] [int] IDENTITY (1, 1) NOT NULL ,
	[LastUpdated] [datetime] NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[TBL_INCIDENCE_CHARGE] (
	[ChargeID] [int] IDENTITY (1, 1) NOT NULL ,
	[TotalValue] [int] NOT NULL ,
	[ChargeText] [varchar] (200) COLLATE Latin1_General_CI_AS NULL ,
	[InvoiceID] [int] NOT NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[TBL_INCIDENCE_CHARGEITEM] (
	[ChargeItemID] [int] IDENTITY (1, 1) NOT NULL ,
	[ChargeID] [int] NOT NULL ,
	[RateID] [int] NOT NULL ,
	[Quantity] [int] NOT NULL ,
	[UnitPrice] [float] NOT NULL ,
	[LineTotal] [float] NOT NULL ,
	[VATRate] [float] NOT NULL ,
	[Nominal] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Control] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[LineDescription] [varchar] (200) COLLATE Latin1_General_CI_AS NULL ,
	[VatNominal] [varchar] (50) COLLATE Latin1_General_CI_AS NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[TBL_INCIDENCE_COST] (
	[CostID] [int] IDENTITY (1, 1) NOT NULL ,
	[TotalValue] [int] NOT NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[TBL_INCIDENCE_COSTITEM] (
	[CostItemID] [int] IDENTITY (1, 1) NOT NULL ,
	[CostID] [int] NOT NULL ,
	[SupplierID] [int] NOT NULL ,
	[ChargeTypeID] [int] NOT NULL ,
	[Quantity] [float] NOT NULL ,
	[TotalCost] [float] NOT NULL ,
	[NoteID] [int] NOT NULL ,
	[Rechargable] [int] NOT NULL ,
	[UpdateAccounts] [int] NOT NULL ,
	[TimesheetID] [int] NOT NULL ,
	[NominalCode] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[AccountCode] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[CostDate] [datetime] NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[TBL_INCIDENCE_NOTES] (
	[IncidenceNotesID] [int] IDENTITY (1, 1) NOT NULL ,
	[MultipleNotesID] [int] NOT NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[TBL_INCIDENCE_PART] (
	[PartsID] [int] IDENTITY (1, 1) NOT NULL ,
	[TotalCostValue] [int] NOT NULL ,
	[TotalSalesValue] [int] NOT NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[TBL_INCIDENCE_PARTITEM] (
	[PartsItemID] [int] IDENTITY (1, 1) NOT NULL ,
	[PartsID] [int] NOT NULL ,
	[OrderedPartID] [int] NOT NULL ,
	[PurchaseNoteID] [int] NOT NULL ,
	[QuantityInStock] [int] NOT NULL ,
	[QuantityOrdered] [int] NOT NULL ,
	[CostPerUnit] [money] NOT NULL ,
	[SalesTotal] [money] NULL ,
	[DateOrdered] [datetime] NULL ,
	[OrderStatusID] [int] NOT NULL ,
	[Installed] [tinyint] NOT NULL ,
	[DateInstalled] [datetime] NULL ,
	[PartDescription] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[TotalCost] [money] NOT NULL ,
	[Rechargable] [int] NOT NULL ,
	[SendToDream] [int] NOT NULL ,
	[PurchaseOrderID] [int] NOT NULL ,
	[NominalCode] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[AccountCode] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[PostedDate] [datetime] NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[TBL_INCIDENCE_WEB] (
	[WebID] [int] IDENTITY (1, 1) NOT NULL ,
	[WebAddress] [varchar] (150) COLLATE Latin1_General_CI_AS NOT NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[tbl_Action] (
	[ActionID] [int] IDENTITY (1, 1) NOT NULL ,
	[IncidentID] [int] NOT NULL ,
	[EngineerID] [int] NOT NULL ,
	[LoggedBy] [int] NOT NULL ,
	[LoggedDate] [datetime] NOT NULL ,
	[CompleteBy] [datetime] NULL ,
	[PriorityID] [smallint] NOT NULL ,
	[StatusID] [smallint] NOT NULL ,
	[ActionTitle] [varchar] (200) COLLATE Latin1_General_CI_AS NULL ,
	[ActionRequired] [varchar] (500) COLLATE Latin1_General_CI_AS NULL ,
	[CompletedBy] [datetime] NULL ,
	[ActionComments] [varchar] (500) COLLATE Latin1_General_CI_AS NULL ,
	[TimeTaken] [int] NOT NULL ,
	[Multiday] [int] NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[tbl_ActionTimes] (
	[ActionLogID] [int] IDENTITY (1, 1) NOT NULL ,
	[ActionID] [int] NOT NULL ,
	[ActionDate] [datetime] NOT NULL ,
	[StartTime] [datetime] NOT NULL ,
	[EndTime] [datetime] NOT NULL ,
	[Breaks] [float] NOT NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[tbl_Address] (
	[AddressID] [int] IDENTITY (1, 1) NOT NULL ,
	[Address1] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Address2] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Address3] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Address4] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[City] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[County] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Postcode] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Country] [varchar] (50) COLLATE Latin1_General_CI_AS NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[tbl_Alarm] (
	[AlarmID] [int] IDENTITY (1, 1) NOT NULL ,
	[UserID] [int] NOT NULL ,
	[NoteID] [int] NOT NULL ,
	[AlarmDate] [datetime] NOT NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[tbl_Appointment] (
	[ID] [int] IDENTITY (1, 1) NOT NULL ,
	[EventStartDate] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[EventEndDate] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[RecursionStartDate] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[RecursionEndDate] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[RecursionPattern] [varchar] (10) COLLATE Latin1_General_CI_AS NULL ,
	[RecursionFreq] [int] NULL ,
	[Subject] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[Type] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[SupplierID] [int] NULL ,
	[NoteID] [int] NOT NULL ,
	[AllDay] [int] NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[tbl_Asset] (
	[AssetID] [int] IDENTITY (1, 1) NOT NULL ,
	[Description] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Cate;ryID] [int] NULL ,
	[PropertyID] [int] NULL ,
	[ClientID] [int] NULL ,
	[UnitID] [int] NULL ,
	[DefaultPriority] [int] NULL ,
	[ModelNo] [char] (20) COLLATE Latin1_General_CI_AS NULL ,
	[SerialNo] [char] (50) COLLATE Latin1_General_CI_AS NULL ,
	[AssetValue] [money] NULL ,
	[DateInstalled] [datetime] NULL ,
	[LastServiced] [datetime] NULL ,
	[ServiceDue] [datetime] NULL ,
	[Warrenty] [bit] NULL ,
	[PersonResponsible] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[SupplierResponsible] [int] NULL ,
	[Manufacturer] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[WarrentyExpires] [datetime] NULL ,
	[MultiNoteID] [int] NOT NULL ,
	[InvoiceContactID] [int] NOT NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[tbl_AssetCate;ry] (
	[AssetCate;ryId] [int] IDENTITY (1, 1) NOT NULL ,
	[Cate;ry] [text] COLLATE Latin1_General_CI_AS NOT NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
;

CREATE TABLE [dbo].[tbl_AssetTemplate] (
	[TemplateID] [int] IDENTITY (1, 1) NOT NULL ,
	[TemplateName] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL ,
	[Comment] [varchar] (500) COLLATE Latin1_General_CI_AS NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[tbl_AssetTemplateLink] (
	[AssetTemplateID] [int] IDENTITY (1, 1) NOT NULL ,
	[TemplateID] [int] NOT NULL ,
	[AssetID] [int] NOT NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[tbl_Audit] (
	[IncidentID] [int] NOT NULL ,
	[AuditDate] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL ,
	[UserName] [int] NOT NULL ,
	[AuditType] [int] NOT NULL ,
	[Message] [varchar] (500) COLLATE Latin1_General_CI_AS NOT NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[tbl_Charge] (
	[ChargeID] [int] IDENTITY (1, 1) NOT NULL ,
	[IncidentID] [int] NOT NULL ,
	[ChargeName] [varchar] (30) COLLATE Latin1_General_CI_AS NOT NULL ,
	[ChargeDescription] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL ,
	[Qty] [int] NOT NULL ,
	[UnitPrice] [money] NULL ,
	[LineTotal] [money] NOT NULL ,
	[Nominal] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Control] [varchar] (50) COLLATE Latin1_General_CI_AS NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[tbl_ChargeType] (
	[ChargeTypeID] [int] IDENTITY (1, 1) NOT NULL ,
	[SupplierID] [int] NOT NULL ,
	[UOM] [varchar] (10) COLLATE Latin1_General_CI_AS NULL ,
	[Charge] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[ChargeDesc] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL ,
	[UnitPrice] [money] NULL ,
	[AccountCode] [varchar] (30) COLLATE Latin1_General_CI_AS NULL ,
	[NominalCode] [varchar] (30) COLLATE Latin1_General_CI_AS NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[tbl_Client] (
	[ClientID] [int] NOT NULL ,
	[ClientName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL ,
	[VatNumber] [varchar] (14) COLLATE Latin1_General_CI_AS NULL ,
	[CompanyNo] [varchar] (20) COLLATE Latin1_General_CI_AS NULL ,
	[ClientComments] [varchar] (500) COLLATE Latin1_General_CI_AS NULL ,
	[Status] [int] NOT NULL ,
	[ContactID] [int] NOT NULL ,
	[AddressID] [int] NOT NULL ,
	[EXTREF] [varchar] (30) COLLATE Latin1_General_CI_AS NULL ,
	[MultiNoteID] [int] NOT NULL ,
	[SalesLedgerAccountCode] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[VATRATE] [varchar] (53) COLLATE Latin1_General_CI_AS NULL ,
	[NominalCoCode] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[NominalDebtCode] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[NominalCredCode] [varchar] (50) COLLATE Latin1_General_CI_AS NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[tbl_ClientVatNominals] (
	[ClientID] [int] NOT NULL ,
	[VatRate] [float] NOT NULL ,
	[VatNominal] [varchar] (20) COLLATE Latin1_General_CI_AS NOT NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[tbl_Comment] (
	[CommentID] [int] NOT NULL ,
	[CallNotesSubject] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL ,
	[Comment] [varchar] (500) COLLATE Latin1_General_CI_AS NOT NULL ,
	[DateStamp] [datetime] NOT NULL ,
	[UserID] [int] NOT NULL ,
	[IncidentID] [int] NOT NULL ,
	[CommentCtgID] [int] NOT NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[tbl_CommentCate;ry] (
	[CommentCtgID] [int] NOT NULL ,
	[CommentCate;ry] [varchar] (30) COLLATE Latin1_General_CI_AS NOT NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[tbl_CompanyFile] (
	[CompanyFileID] [int] IDENTITY (1, 1) NOT NULL ,
	[ClientID] [int] NOT NULL ,
	[PropertyID] [int] NOT NULL ,
	[UnitID] [int] NOT NULL ,
	[AssetID] [int] NOT NULL ,
	[FileID] [int] NOT NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[tbl_Contact] (
	[ContactID] [int] IDENTITY (1, 1) NOT NULL ,
	[Title] [varchar] (10) COLLATE Latin1_General_CI_AS NULL ,
	[FirstName] [varchar] (20) COLLATE Latin1_General_CI_AS NULL ,
	[LastName] [varchar] (30) COLLATE Latin1_General_CI_AS NULL ,
	[Company] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Position] [varchar] (30) COLLATE Latin1_General_CI_AS NULL ,
	[Telephone] [varchar] (20) COLLATE Latin1_General_CI_AS NULL ,
	[Fax] [varchar] (20) COLLATE Latin1_General_CI_AS NULL ,
	[Email] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Website] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[NoteID] [int] NOT NULL ,
	[AddressID] [int] NOT NULL ,
	[Cate;ryID] [int] NOT NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[tbl_ContactLink] (
	[LinkID] [int] IDENTITY (1, 1) NOT NULL ,
	[ContactID] [int] NOT NULL ,
	[ClientID] [int] NOT NULL ,
	[PropertyID] [int] NOT NULL ,
	[UnitID] [int] NOT NULL ,
	[AssetID] [int] NOT NULL ,
	[SupplierID] [int] NOT NULL ,
	[TenantID] [int] NOT NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[tbl_Cost] (
	[ChargeID] [int] IDENTITY (1, 1) NOT NULL ,
	[IncidentID] [int] NOT NULL ,
	[ActionID] [int] NOT NULL ,
	[SupplierChargeID] [int] NOT NULL ,
	[ChargeTypeID] [int] NOT NULL ,
	[Units] [int] NOT NULL ,
	[Rate] [numeric](18, 0) NULL ,
	[TotalAmount] [money] NOT NULL ,
	[Comment] [varchar] (500) COLLATE Latin1_General_CI_AS NOT NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[tbl_Events] (
	[EventId] [int] IDENTITY (1, 1) NOT NULL ,
	[AllProperties] [binary] (50) NULL ,
	[StartDateTime] [datetime] NOT NULL ,
	[EndDateTime] [datetime] NOT NULL ,
	[Duration] [int] NULL ,
	[Subject] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Location] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Type] [int] NOT NULL ,
	[AllDayEvent] [bit] NOT NULL ,
	[Owner] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Description] [text] COLLATE Latin1_General_CI_AS NULL ,
	[ClientID] [int] NOT NULL ,
	[PropertyID] [int] NOT NULL ,
	[UnitID] [int] NOT NULL ,
	[AssetID] [int] NOT NULL ,
	[SupplierID] [int] NOT NULL ,
	[TenantID] [int] NOT NULL ,
	[UserID] [int] NOT NULL ,
	[ContactID] [int] NOT NULL ,
	[AppointmentRefID] [int] NULL ,
	[IncidentRefID] [int] NOT NULL ,
	[MultiNoteID] [int] NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
;

CREATE TABLE [dbo].[tbl_ExtendedNote] (
	[NoteID] [int] IDENTITY (1, 1) NOT NULL ,
	[NoteText] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL ,
	[UserID] [int] NOT NULL ,
	[Subject] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL ,
	[MultiNoteID] [int] NOT NULL ,
	[LastUpdatedAt] [datetime] NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[tbl_File] (
	[FileID] [int] IDENTITY (1, 1) NOT NULL ,
	[Filename] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL ,
	[FileData] [image] NOT NULL ,
	[FileSize] [int] NOT NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
;

CREATE TABLE [dbo].[tbl_INCIDENCE_PurchaseOrder] (
	[PurchaseOrderID] [int] IDENTITY (1, 1) NOT NULL ,
	[POID] [int] NOT NULL ,
	[TotalOrder] [money] NOT NULL ,
	[PONumber] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[PODate] [datetime] NULL ,
	[Supplier] [varchar] (10) COLLATE Latin1_General_CI_AS NOT NULL ,
	[Description] [varchar] (500) COLLATE Latin1_General_CI_AS NULL ,
	[RequestedBy] [int] NOT NULL ,
	[DateReq] [datetime] NULL ,
	[Status] [int] NOT NULL ,
	[InvAddress] [text] COLLATE Latin1_General_CI_AS NULL ,
	[DelAddress] [text] COLLATE Latin1_General_CI_AS NULL ,
	[Delivery] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[SentToDream] [tinyint] NOT NULL ,
	[Comment] [varchar] (500) COLLATE Latin1_General_CI_AS NULL ,
	[DeliveryType] [varchar] (100) COLLATE Latin1_General_CI_AS NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
;

CREATE TABLE [dbo].[tbl_INCIDENCE_PurchaseOrderItem] (
	[POItemID] [int] IDENTITY (1, 1) NOT NULL ,
	[TotalOrder] [money] NOT NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[tbl_Incidence_Info] (
	[InfoID] [int] IDENTITY (1, 1) NOT NULL ,
	[Info1] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Info10] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Info2] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Info3] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Info4] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Info5] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Info6] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Info7] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Info8] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Info9] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Info11] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Info12] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Info13] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Info14] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Info15] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Info16] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Info17] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Info18] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Info19] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Info20] [varchar] (50) COLLATE Latin1_General_CI_AS NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[tbl_Incidence_Invoices] (
	[CallID] [int] NOT NULL ,
	[IncidentName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[InvoiceID] [int] NULL ,
	[InvoiceNo] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[FilePath] [varchar] (50) COLLATE Latin1_General_CI_AS NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[tbl_Invoice] (
	[InvoiceID] [int] IDENTITY (1, 1) NOT NULL ,
	[InvType] [int] NULL ,
	[InvNo] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL ,
	[InvRef] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[InvDate] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Address1] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Address2] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Address3] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[City] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[PostCode] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Text] [varchar] (500) COLLATE Latin1_General_CI_AS NULL ,
	[Total] [float] NULL ,
	[Status] [int] NULL ,
	[Nominal] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Control] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[TotalNet] [float] NOT NULL ,
	[TotalVat] [float] NOT NULL ,
	[TotalGross] [float] NOT NULL ,
	[VatNominal] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[CallRef] [int] NOT NULL ,
	[Discount] [float] NOT NULL ,
	[Invoicee] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[PostedDate] [datetime] NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[tbl_InvoiceLines] (
	[InvLineID] [int] IDENTITY (1, 1) NOT NULL ,
	[InvLineNo] [int] NULL ,
	[InvoiceNo] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL ,
	[ChargeLineID] [int] NOT NULL ,
	[Nominal] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Control] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Qty] [float] NULL ,
	[Price] [float] NULL ,
	[Description] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[Narrative] [varchar] (500) COLLATE Latin1_General_CI_AS NULL ,
	[Status] [int] NULL ,
	[InvID] [int] NOT NULL ,
	[NetAmount] [float] NULL ,
	[Vat] [float] NULL ,
	[GrossAmount] [float] NULL ,
	[VatNominal] [varchar] (50) COLLATE Latin1_General_CI_AS NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[tbl_MultiNote] (
	[MultiNoteID] [int] IDENTITY (1, 1) NOT NULL ,
	[LastEdited] [datetime] NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[tbl_Note] (
	[NoteID] [int] IDENTITY (1, 1) NOT NULL ,
	[NoteText] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL ,
	[UserID] [int] NOT NULL ,
	[Subject] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL ,
	[MultiNoteID] [int] NOT NULL ,
	[LastUpdatedAt] [datetime] NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[tbl_POLineItem] (
	[POLineItemID] [int] IDENTITY (1, 1) NOT NULL ,
	[POItemID] [int] NOT NULL ,
	[Item] [varchar] (20) COLLATE Latin1_General_CI_AS NOT NULL ,
	[Description] [varchar] (500) COLLATE Latin1_General_CI_AS NOT NULL ,
	[Qty] [int] NOT NULL ,
	[UOM] [varchar] (10) COLLATE Latin1_General_CI_AS NULL ,
	[Price] [money] NOT NULL ,
	[Discount] [money] NULL ,
	[LineTotal] [money] NOT NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[tbl_POrder] (
	[OrderID] [int] IDENTITY (1, 1) NOT NULL ,
	[IncidentID] [int] NOT NULL ,
	[PartID] [int] NOT NULL ,
	[Quantity] [int] NOT NULL ,
	[PONumber] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[OrderStatusID] [smallint] NOT NULL ,
	[PartText] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL ,
	[UnitPrice] [money] NOT NULL ,
	[LineTotal] [money] NOT NULL ,
	[DateOrdered] [datetime] NOT NULL ,
	[DateRequired] [datetime] NULL ,
	[Comment] [varchar] (200) COLLATE Latin1_General_CI_AS NULL ,
	[PartDescription] [varchar] (100) COLLATE Latin1_General_CI_AS NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[tbl_Part] (
	[PartID] [int] IDENTITY (1, 1) NOT NULL ,
	[CatalogueID] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[PartDesc] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL ,
	[Price] [money] NOT NULL ,
	[EXTREF] [varchar] (30) COLLATE Latin1_General_CI_AS NULL ,
	[Comments] [varchar] (500) COLLATE Latin1_General_CI_AS NULL ,
	[Description] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Nominal] [varchar] (20) COLLATE Latin1_General_CI_AS NULL ,
	[NominalCr] [varchar] (20) COLLATE Latin1_General_CI_AS NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[tbl_Property] (
	[PropertyID] [int] NOT NULL ,
	[ClientID] [int] NOT NULL ,
	[PropertyName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[PropertyComments] [text] COLLATE Latin1_General_CI_AS NULL ,
	[Status] [int] NOT NULL ,
	[LastInspection] [datetime] NULL ,
	[NextInspection] [datetime] NULL ,
	[ManagerContactID] [int] NOT NULL ,
	[ContactID] [int] NOT NULL ,
	[AddressID] [int] NOT NULL ,
	[EXTREF] [varchar] (30) COLLATE Latin1_General_CI_AS NULL ,
	[MultiNoteID] [int] NOT NULL ,
	[SingleTenant] [int] NOT NULL ,
	[LedgerCode] [varchar] (30) COLLATE Latin1_General_CI_AS NULL ,
	[PropertyCode] [varchar] (30) COLLATE Latin1_General_CI_AS NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
;

CREATE TABLE [dbo].[tbl_Rates] (
	[RateID] [int] IDENTITY (1, 1) NOT NULL ,
	[Key] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL ,
	[ChargeName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL ,
	[Description] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL ,
	[Charge] [float] NOT NULL ,
	[Repeatable] [int] NOT NULL ,
	[Editable] [int] NOT NULL ,
	[Nominal] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Control] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[VatRate] [float] NOT NULL ,
	[PropertyNominal] [int] NOT NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[tbl_ReportMaster] (
	[ReportID] [int] IDENTITY (1, 1) NOT NULL ,
	[ReportName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[ReportTitle] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[ReportSource] [varchar] (250) COLLATE Latin1_General_CI_AS NULL ,
	[TableSQL] [varchar] (500) COLLATE Latin1_General_CI_AS NULL ,
	[Param1SQL] [varchar] (250) COLLATE Latin1_General_CI_AS NULL ,
	[Param1Key] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Param1Value] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Label1] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Param1] [int] NOT NULL ,
	[Param2SQL] [varchar] (250) COLLATE Latin1_General_CI_AS NULL ,
	[Param2Key] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Param2Value] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Label2] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Param2] [int] NOT NULL ,
	[Param3SQL] [varchar] (250) COLLATE Latin1_General_CI_AS NULL ,
	[Param3Key] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Param3Value] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Label3] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Param3] [int] NOT NULL ,
	[Param4SQL] [varchar] (250) COLLATE Latin1_General_CI_AS NULL ,
	[Param4Key] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Param4Value] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Label4] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Param4] [int] NOT NULL ,
	[Param5SQL] [varchar] (250) COLLATE Latin1_General_CI_AS NULL ,
	[Param5Key] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Param5Value] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Label5] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Param5] [int] NOT NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[tbl_ReportSource] (
	[ReportID] [int] IDENTITY (1, 1) NOT NULL ,
	[ReportName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL ,
	[ReportTitle] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[ReportSource] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL ,
	[ReportSQL] [varchar] (300) COLLATE Latin1_General_CI_AS NULL ,
	[ReportParameter] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[AssetID] [bit] NOT NULL ,
	[IncidentID] [bit] NOT NULL ,
	[PropertyID] [bit] NOT NULL ,
	[StartDate] [bit] NOT NULL ,
	[EngineerID] [bit] NOT NULL ,
	[UnitID] [bit] NOT NULL ,
	[ClientID] [bit] NOT NULL ,
	[StatusID] [bit] NOT NULL ,
	[EndDate] [bit] NOT NULL ,
	[ExactDate] [bit] NOT NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[tbl_Schedule] (
	[ID] [int] IDENTITY (1, 1) NOT NULL ,
	[ClientID] [int] NOT NULL ,
	[PropertyID] [int] NOT NULL ,
	[UnitID] [int] NULL ,
	[AssetID] [int] NULL ,
	[AppointmentID] [int] NOT NULL ,
	[SupplierID] [int] NOT NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[tbl_Supplier] (
	[SupplierID] [int] IDENTITY (1, 1) NOT NULL ,
	[SupplierName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL ,
	[SupplierTypeID] [smallint] NOT NULL ,
	[Status] [int] NOT NULL ,
	[SupplierComments] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[ContactID] [int] NOT NULL ,
	[AddressID] [int] NOT NULL ,
	[AccountLedger] [varchar] (30) COLLATE Latin1_General_CI_AS NULL ,
	[AccountCode] [varchar] (30) COLLATE Latin1_General_CI_AS NULL ,
	[MultiNoteID] [int] NOT NULL ,
	[SupplierTelephone] [varchar] (50) COLLATE Latin1_General_CI_AS NULL 
) ON [PRIMARY]
;

CREATE TABLE [dbo].[tbl_Unit] (
	[UnitID] [int] NOT NULL ,
	[UnitName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL ,
	[ClientID] [numeric](18, 0) NOT NULL ,
	[PropertyID] [int] NOT NULL ,
	[UnitComments] [text] COLLATE Latin1_General_CI_AS NULL ,
	[Status] [int] NOT NULL ,
	[ContactID] [int] NOT NULL ,
	[ManagerContactID] [int] NOT NULL ,
	[TenantRef] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[AddressID] [int] NOT NULL ,
	[EXTREF] [varchar] (20) COLLATE Latin1_General_CI_AS NULL ,
	[MultiNoteID] [int] NOT NULL ,
	[TenantContactID] [int] NOT NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
;

CREATE TABLE [dbo].[tbl_Web_Fields] (
	[InfoLineID] [int] IDENTITY (1, 1) NOT NULL ,
	[WebID] [int] NOT NULL ,
	[CallID] [int] NOT NULL ,
	[Field10] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[Field11] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[Field12] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[Field13] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[Field14] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[Field15] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[Field16] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[Field17] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[Field18] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[Field19] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[Field20] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[Field1] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[Field2] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[Field3] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[Field4] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[Field5] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[Field6] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[Field7] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[Field8] [varchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[Field9] [varchar] (100) COLLATE Latin1_General_CI_AS NULL 
) ON [PRIMARY]
;

