
CREATE VIEW [dbo].[DreamServiceCharges]
AS
SELECT     LandMark.dbo.tblServiceBudget.PropertyReference AS PROPERTY, LEFT(LandMark.dbo.tblServiceBudget.BudgetPeriod, 4) 
                      + LandMark.dbo.tblServiceBudget.ScheduleReference + LandMark.dbo.tblServiceBudget.ExpenseReference AS L3ACCOUNT, 
                      LandMark.dbo.tblServiceBudget.BudgetNetValue, LandMark.dbo.tblServiceBudget.BudgetPeriod AS YEAR, 
                      LandMark.dbo.tblServiceBudget.ScheduleReference AS SCHEDULE, LandMark.dbo.tblServiceBudget.ExpenseReference AS EXPENSE, 
                      dbo.View_Property_EXT.PropertyID
FROM         LandMark.dbo.tblServiceBudget INNER JOIN
                      dbo.View_Property_EXT ON LandMark.dbo.tblServiceBudget.PropertyReference = dbo.View_Property_EXT.EXTREF

;

CREATE VIEW dbo.DrmPOview
AS
SELECT     OrderID AS POrderID, IncidentID, PartID, Quantity, PartText, PartDescription, UnitPrice, LineTotal, DateOrdered
FROM         dbo.tbl_POrder

;

CREATE VIEW dbo.DrmView_Invoice
AS
SELECT     InvoiceID, InvNo, InvType, '100001' AS DocNumber, InvDate AS DocDate, 'SINV' AS DocType, Nominal, Control, TotalNet, TotalVat, TotalGross
FROM         dbo.tbl_Invoice

;

CREATE VIEW dbo.DrmView_PartUsed
AS
SELECT     TOP 100 PERCENT DocType, DocNo AS DocNum, RowNo, DocDate, Nominal AS NominalCode, Account AS AccountCode, Description, QuantityOrdered, 
                      CostPerUnit, [value], IncidentName, CallNo, OrderStatusID, SendToDream, PartsID, PostedDate
FROM         dbo.DrmView_PartUsed1
ORDER BY DocNo, RowNo

;

CREATE VIEW dbo.DrmView_PartUsed1
AS
SELECT     'MPAR' AS DocType, '1' AS RowNo, dbo.TBL_INCIDENCE.IncidentID AS DocNo, dbo.TBL_INCIDENCE_PARTITEM.DateOrdered AS DocDate, 
                      dbo.tbl_Part.Nominal AS Nominal, dbo.View_Property_EXT.PropertyCode AS Account, dbo.TBL_INCIDENCE_PARTITEM.PartDescription AS Description, 
                      dbo.TBL_INCIDENCE_PARTITEM.QuantityOrdered, dbo.TBL_INCIDENCE_PARTITEM.TotalCost AS value, dbo.TBL_INCIDENCE.IncidentName, 
                      dbo.TBL_INCIDENCE.IncidentID AS CallNo, dbo.TBL_INCIDENCE_PARTITEM.OrderStatusID, dbo.tbl_Part.CatalogueID, dbo.tbl_Part.PartDesc, 
                      dbo.tbl_Part.Comments, dbo.TBL_INCIDENCE_PARTITEM.SendToDream, dbo.TBL_INCIDENCE_PARTITEM.CostPerUnit, dbo.TBL_INCIDENCE.PartsID, dbo.TBL_INCIDENCE_PARTITEM.PostedDate
FROM         dbo.TBL_INCIDENCE_CALL INNER JOIN
                      dbo.View_Property_EXT ON dbo.TBL_INCIDENCE_CALL.PropertyID = dbo.View_Property_EXT.PropertyID RIGHT OUTER JOIN
                      dbo.TBL_INCIDENCE INNER JOIN
                      dbo.TBL_INCIDENCE_PARTITEM ON dbo.TBL_INCIDENCE.PartsID = dbo.TBL_INCIDENCE_PARTITEM.PartsID INNER JOIN
                      dbo.tbl_Part ON dbo.TBL_INCIDENCE_PARTITEM.OrderedPartID = dbo.tbl_Part.PartID ON 
                      dbo.TBL_INCIDENCE_CALL.CallID = dbo.TBL_INCIDENCE.CallID
UNION ALL
SELECT     'MPAR' AS DocType, '2' AS RowNo, dbo.TBL_INCIDENCE.IncidentID AS DocNo, dbo.TBL_INCIDENCE_PARTITEM.DateOrdered AS DocDate, 
                      dbo.tbl_Part.Nominalcr AS Nominal, 'ZZZ99' as Account, dbo.TBL_INCIDENCE_PARTITEM.PartDescription AS Description, 
                      dbo.TBL_INCIDENCE_PARTITEM.QuantityOrdered, dbo.TBL_INCIDENCE_PARTITEM.TotalCost * - 1 AS [value], dbo.TBL_INCIDENCE.IncidentName, 
                      dbo.TBL_INCIDENCE.IncidentID AS CallNo, dbo.TBL_INCIDENCE_PARTITEM.OrderStatusID, dbo.tbl_Part.CatalogueID, dbo.tbl_Part.PartDesc, 
                      dbo.tbl_Part.Comments, dbo.TBL_INCIDENCE_PARTITEM.SendToDream, dbo.TBL_INCIDENCE_PARTITEM.CostPerUnit, dbo.TBL_INCIDENCE.PartsID, dbo.TBL_INCIDENCE_PARTITEM.PostedDate
FROM         dbo.TBL_INCIDENCE_CALL INNER JOIN
                      dbo.tbl_Client ON dbo.TBL_INCIDENCE_CALL.ClientID = dbo.tbl_Client.ClientID LEFT OUTER JOIN
                      dbo.tbl_Property ON dbo.TBL_INCIDENCE_CALL.PropertyID = dbo.tbl_Property.PropertyID RIGHT OUTER JOIN
                      dbo.TBL_INCIDENCE INNER JOIN
                      dbo.TBL_INCIDENCE_PARTITEM ON dbo.TBL_INCIDENCE.PartsID = dbo.TBL_INCIDENCE_PARTITEM.PartsID INNER JOIN
                      dbo.tbl_Part ON dbo.TBL_INCIDENCE_PARTITEM.OrderedPartID = dbo.tbl_Part.PartID ON 
                      dbo.TBL_INCIDENCE_CALL.CallID = dbo.TBL_INCIDENCE.CallID





;
CREATE VIEW dbo.DrmView_PurchaseRequest
AS
SELECT     'PREQ' AS OrderType, dbo.TBL_INCIDENCE_PARTITEM.DateOrdered, 'W' AS OrderStatus, 'SQSDBA' AS Authorisor, 'Suppliers' AS Ledger, 
                      'ZZ001' AS SupplierAccount, dbo.tbl_Part.CatalogueID AS OrderItem, dbo.tbl_Client.CompanyNo + dbo.tbl_Part.Nominal AS Nominal, 
                      dbo.View_Property_EXT.PropertyCode AS Account, dbo.tbl_Part.PartDesc + ' - ' + dbo.tbl_Note.NoteText AS Description, 
                      dbo.TBL_INCIDENCE_PARTITEM.CostPerUnit AS UnitPrice, dbo.TBL_INCIDENCE_PARTITEM.QuantityOrdered AS Quantity, 
                      dbo.TBL_INCIDENCE_PARTITEM.TotalCost AS TotalPrice, dbo.TBL_INCIDENCE_CALL.CallID AS CallNo, dbo.TBL_INCIDENCE.IncidentName, 
                      dbo.TBL_INCIDENCE_CALL.LoggedBy, dbo.TBL_INCIDENCE_PARTITEM.PurchaseNoteID, dbo.TBL_INCIDENCE_PARTITEM.OrderStatusID, 
                      dbo.TBL_INCIDENCE.IncidentID
FROM         dbo.TBL_INCIDENCE INNER JOIN
                      dbo.TBL_INCIDENCE_CALL ON dbo.TBL_INCIDENCE.CallID = dbo.TBL_INCIDENCE_CALL.CallID INNER JOIN
                      dbo.TBL_INCIDENCE_PARTITEM ON dbo.TBL_INCIDENCE.PartsID = dbo.TBL_INCIDENCE_PARTITEM.PartsID INNER JOIN
                      dbo.tbl_Client ON dbo.TBL_INCIDENCE_CALL.ClientID = dbo.tbl_Client.ClientID INNER JOIN
                      dbo.View_Property_EXT ON dbo.TBL_INCIDENCE_CALL.PropertyID = dbo.View_Property_EXT.PropertyID LEFT OUTER JOIN
                      dbo.tbl_Part ON dbo.TBL_INCIDENCE_PARTITEM.OrderedPartID = dbo.tbl_Part.PartID LEFT OUTER JOIN
                      dbo.tbl_Note ON dbo.TBL_INCIDENCE_PARTITEM.PurchaseNoteID = dbo.tbl_Note.NoteID


;

CREATE VIEW dbo.DrmView_SalInv2
AS
SELECT     'SINV' AS DocType, dbo.tbl_Invoice.InvoiceID AS DocNum, dbo.tbl_Invoice.InvDate AS DocDate, '3' AS RowNo, dbo.tbl_InvoiceLines.Nominal, 
                      dbo.tbl_InvoiceLines.Control AS Account, dbo.tbl_InvoiceLines.Description, dbo.tbl_InvoiceLines.NetAmount * - 1 AS [Value], 
                      'TransactionType' AS Actual, dbo.tbl_Invoice.InvRef AS Userfield1, ' ' AS Userfield2, 0 AS UserValue
FROM         dbo.tbl_Invoice INNER JOIN
                      dbo.tbl_InvoiceLines ON dbo.tbl_Invoice.InvNo = dbo.tbl_InvoiceLines.InvoiceNo


;
CREATE VIEW dbo.DrmView_SalesInv
AS
SELECT     TOP 100 PERCENT DocType, DocNum, DocDate, RowNo, Nominal, Account, Description, [Value], TransactionType, Userfield1, Userfield2, Uservalue
FROM         dbo.DrmView_SalesInv1
ORDER BY DocNum, RowNo



;


CREATE VIEW dbo.DrmView_SalesInv1
AS
SELECT     'SINV' AS DocType, InvoiceID AS DocNum, InvDate AS DocDate, '' AS RowNo, Nominal, Control AS Account, 'Helpdesk call ref-' + InvRef AS Description, 
                      TotalGross AS [Value], 'Actual' AS TransactionType, InvRef AS Userfield1, ' ' AS Userfield2, '0' AS Uservalue, '1' AS Line
FROM         dbo.tbl_Invoice
UNION ALL
SELECT     'SINV' AS DocType, InvoiceID AS DocNum, InvDate AS DocDate, '' AS RowNo, VatNominal, NULL AS Account, 
                      'Helpdesk call ref-' + InvRef AS Description, TotalVat * - 1 AS [Value], 'Actual' AS TransactionType, InvRef AS Userfield1, ' ' AS Userfield2, 
                      TotalNet * - 1 AS USERValue, '2' AS line
FROM         dbo.tbl_Invoice
UNION ALL
SELECT     'SINV' AS DocType, dbo.tbl_Invoice.InvoiceID AS DocNum, dbo.tbl_Invoice.InvDate AS DocDate, '' AS RowNo, dbo.tbl_InvoiceLines.Nominal, 
                      dbo.tbl_InvoiceLines.Control AS Account, dbo.tbl_InvoiceLines.Description, dbo.tbl_InvoiceLines.NetAmount * - 1 AS [Value], 'Actual' AS Actual, 
                      dbo.tbl_Invoice.InvRef AS Userfield1, ' ' AS Userfield2, 0 AS UserValue, '3' AS Line
FROM         dbo.tbl_Invoice INNER JOIN
                      dbo.tbl_InvoiceLines ON dbo.tbl_Invoice.InvNo = dbo.tbl_InvoiceLines.InvoiceNo


;


CREATE VIEW dbo.Drm_PODeliveryAddress
AS
SELECT     ACCOUNT, PRENAME, ADDRESSNAME, ADDRESS1, ADDRESS2, ADDRESS3, POSTTOWN, COUNTY, POSTCODE1, POSTCODE2, TELEPHONE, 
                      FAXNO
FROM         DREAMDB.SQSDBA.M_NAMEACCOUNT
WHERE     (LEDGER = '     COMPLED')


;


CREATE VIEW dbo.Drm_PoItems
AS
SELECT     PRODUCT, DESCRIPTION, NOMINAL, UNITPRICE, UOM
FROM         DREAMDB.SQSDBA.M_PRODUCTS


;


CREATE VIEW dbo.Drmview_Invoice1
AS
SELECT    dbo.tbl_InvoiceLines.InvoiceNo, 'SINV' AS DocType, '1' AS RowNo, dbo.tbl_Invoice.InvDate AS DocDate, dbo.tbl_InvoiceLines.Nominal, dbo.tbl_InvoiceLines.Control AS Account, 
                      dbo.tbl_InvoiceLines.Narrative AS Description, dbo.tbl_InvoiceLines.GrossAmount AS Value, 'Actual' AS TransactionType, 
                      dbo.tbl_InvoiceLines.InvoiceNo AS UserField1, dbo.tbl_InvoiceLines.InvID AS UserField2
FROM         dbo.tbl_InvoiceLines inner Join dbo.tbl_Invoice on dbo.tbl_Invoice.InvNo =  dbo.tbl_InvoiceLines.InvoiceNo
UNION ALL
SELECT    dbo.tbl_InvoiceLines.InvoiceNo,  'SINV' AS DocType, '2' AS RowNo, dbo.tbl_Invoice.InvDate AS DocDate, dbo.tbl_InvoiceLines.Nominal, dbo.tbl_InvoiceLines.Control AS Account, 
                      dbo.tbl_InvoiceLines.Narrative AS Description, dbo.tbl_InvoiceLines.GrossAmount * - 1 AS Value, 'Actual' AS TransactionType, 
                      dbo.tbl_InvoiceLines.InvoiceNo AS UserField1, dbo.tbl_InvoiceLines.InvID AS UserField2
FROM         dbo.tbl_InvoiceLines inner Join dbo.tbl_Invoice on dbo.tbl_Invoice.InvNo =  dbo.tbl_InvoiceLines.InvoiceNo



;

CREATE VIEW dbo.Drmview_Timesheet
AS
SELECT     TOP 100 PERCENT DocType, DocNum, RowNo, DocDate, NominalCode, NominalCompanyCode, AccountCode, Subject, NoteText, ChargeDesc, NCC, 
                      Nominal2, Quantity, UnitPrice, TotalCost, IncidentName, CallNo, costid AS CostID, updateaccounts, TimesheetID
FROM         dbo.Drmview_Timesheet1
ORDER BY DocNum, RowNo
;

CREATE VIEW dbo.Drmview_Timesheet1
AS
SELECT     'MTS' AS DocType, dbo.TBL_INCIDENCE.IncidentID AS DocNum, '1' AS RowNo, dbo.TBL_INCIDENCE_COSTITEM.costdate AS DocDate, 
                      dbo.tbl_ChargeType.accountCode AS NominalCode, dbo.View_Client_EXT.NominalCompanyCode, dbo.View_Supplier_EXT.AccountCode as PropAccount, 
                      dbo.View_Property_EXT.PropertyCode AS AccountCode, dbo.TBL_INCIDENCE_CALL.Subject, dbo.tbl_Note.NoteText, dbo.tbl_ChargeType.ChargeDesc, 
                      dbo.View_Client_EXT.NominalCompanyCode AS NCC, dbo.tbl_ChargeType.NominalCode AS Nominal2, dbo.TBL_INCIDENCE_COSTITEM.Quantity, 
                      dbo.tbl_ChargeType.UnitPrice, dbo.TBL_INCIDENCE_COSTITEM.TotalCost * - 1 AS TotalCost, dbo.TBL_INCIDENCE.IncidentName, 
                      dbo.TBL_INCIDENCE_COSTITEM.CostItemID, dbo.TBL_INCIDENCE.IncidentID AS CallNo, dbo.TBL_INCIDENCE_COSTITEM.costid, 
                      dbo.TBL_INCIDENCE_COSTITEM.updateaccounts, dbo.TBL_INCIDENCE_COSTITEM.TimesheetID
FROM         dbo.TBL_INCIDENCE INNER JOIN
                      dbo.TBL_INCIDENCE_COSTITEM ON dbo.TBL_INCIDENCE.CostsID = dbo.TBL_INCIDENCE_COSTITEM.CostID INNER JOIN
                      dbo.TBL_INCIDENCE_CALL ON dbo.TBL_INCIDENCE.CallID = dbo.TBL_INCIDENCE_CALL.CallID INNER JOIN
                      dbo.tbl_Note ON dbo.TBL_INCIDENCE_COSTITEM.NoteID = dbo.tbl_Note.NoteID INNER JOIN
                      dbo.tbl_ChargeType ON dbo.TBL_INCIDENCE_COSTITEM.ChargeTypeID = dbo.tbl_ChargeType.ChargeTypeID AND 
                      dbo.TBL_INCIDENCE_COSTITEM.SupplierID = dbo.tbl_ChargeType.SupplierID INNER JOIN
                      dbo.View_Supplier_EXT ON dbo.tbl_ChargeType.SupplierID = dbo.View_Supplier_EXT.SupplierID LEFT OUTER JOIN
                      dbo.View_Client_EXT ON dbo.TBL_INCIDENCE_CALL.ClientID = dbo.View_Client_EXT.ClientID LEFT OUTER JOIN
                      dbo.View_Property_EXT ON dbo.TBL_INCIDENCE_CALL.PropertyID = dbo.View_Property_EXT.PropertyID
UNION ALL
SELECT     'MTS' AS DocType, dbo.TBL_INCIDENCE.IncidentID AS DocNum, '2' AS RowNo, dbo.TBL_INCIDENCE_COSTITEM.costdate AS DocDate, 
                      dbo.tbl_ChargeType.NominalCode AS NominalCode, dbo.View_Client_EXT.NominalCompanyCode, 
                      dbo.View_Property_EXT.PropertyCode AS PropAccount, dbo.View_Supplier_EXT.AccountCode, dbo.TBL_INCIDENCE_CALL.Subject, 
                      dbo.tbl_Note.NoteText, dbo.tbl_ChargeType.ChargeDesc, dbo.View_Client_EXT.NominalCompanyCode AS NCC, 
                      dbo.tbl_ChargeType.NominalCode AS Expr1, dbo.TBL_INCIDENCE_COSTITEM.Quantity, dbo.tbl_ChargeType.UnitPrice, 
                      dbo.TBL_INCIDENCE_COSTITEM.TotalCost AS Cost, dbo.TBL_INCIDENCE.IncidentName, dbo.TBL_INCIDENCE_COSTITEM.CostItemID, 
                      dbo.TBL_INCIDENCE.IncidentID AS CallNo, dbo.TBL_INCIDENCE_COSTITEM.costid, dbo.TBL_INCIDENCE_COSTITEM.updateaccounts, 
                      dbo.TBL_INCIDENCE_COSTITEM.TimesheetID
FROM         dbo.TBL_INCIDENCE INNER JOIN
                      dbo.TBL_INCIDENCE_COSTITEM ON dbo.TBL_INCIDENCE.CostsID = dbo.TBL_INCIDENCE_COSTITEM.CostID INNER JOIN
                      dbo.TBL_INCIDENCE_CALL ON dbo.TBL_INCIDENCE.CallID = dbo.TBL_INCIDENCE_CALL.CallID INNER JOIN
                      dbo.tbl_Note ON dbo.TBL_INCIDENCE_COSTITEM.NoteID = dbo.tbl_Note.NoteID INNER JOIN
                      dbo.tbl_ChargeType ON dbo.TBL_INCIDENCE_COSTITEM.ChargeTypeID = dbo.tbl_ChargeType.ChargeTypeID AND 
                      dbo.TBL_INCIDENCE_COSTITEM.SupplierID = dbo.tbl_ChargeType.SupplierID INNER JOIN
                      dbo.View_Supplier_EXT ON dbo.tbl_ChargeType.SupplierID = dbo.View_Supplier_EXT.SupplierID LEFT OUTER JOIN
                      dbo.View_Client_EXT ON dbo.TBL_INCIDENCE_CALL.ClientID = dbo.View_Client_EXT.ClientID LEFT OUTER JOIN
                      dbo.View_Property_EXT ON dbo.TBL_INCIDENCE_CALL.PropertyID = dbo.View_Property_EXT.PropertyID





;

CREATE VIEW dbo.Report_Incidence
AS
SELECT     dbo.TBL_INCIDENCE.IncidentName, dbo.TBL_INCIDENCE_CALL.ClientID, dbo.TBL_INCIDENCE_CALL.PropertyID, dbo.TBL_INCIDENCE_CALL.UnitID, 
                      dbo.TBL_INCIDENCE_CALL.AssetID, dbo.TBL_INCIDENCE.IncidentID, dbo.tbl_Supplier.SupplierName, dbo.TBL_INCIDENCE_CALL.CallerName, 
                      dbo.TBL_INCIDENCE_CALL.CallerTel, dbo.TBL_INCIDENCE_CALL.CallerPosition, dbo.Config_IncidentStatus.StatusDesc, 
                      dbo.Config_Priority.PriorityDesc, dbo.View_Client_EXT.ClientName, dbo.App_UserDetails.UserName, dbo.TBL_INCIDENCE.OpenSince, 
                      dbo.TBL_INCIDENCE_CALL.Subject, dbo.TBL_INCIDENCE_CALL.ChargeTo, dbo.TBL_INCIDENCE_CALL.RespondByDate, dbo.View_Unit_EXT.UnitName,
                       dbo.tbl_Asset.Description, dbo.Config_CallType.CallTypeDesc, dbo.TBL_INCIDENCE.StatusID, dbo.TBL_INCIDENCE.CallTypeID, 
                      dbo.tbl_Supplier.SupplierID, dbo.TBL_INCIDENCE.ClosedAt, dbo.View_Property_EXT.PropertyName, dbo.View_Unit_EXT.EXTREF AS UnitReference, 
                      dbo.View_Property_EXT.EXTREF AS PropertyReference, dbo.View_Client_EXT.EXTREF AS CompanyReference, 
                      dbo.tbl_Supplier.SupplierID AS EngineerID, dbo.Config_Status.StatusDesc AS CallStatus, dbo.Config_Status.Affects, dbo.tbl_Note.NoteText, 
                      dbo.TBL_INCIDENCE_CALL.CallID
FROM         dbo.View_Property_EXT RIGHT OUTER JOIN
                      dbo.Config_Priority INNER JOIN
                      dbo.tbl_Supplier INNER JOIN
                      dbo.TBL_INCIDENCE_CALL ON dbo.tbl_Supplier.SupplierID = dbo.TBL_INCIDENCE_CALL.SupplierID ON 
                      dbo.Config_Priority.PriorityId = dbo.TBL_INCIDENCE_CALL.PriorityID INNER JOIN
                      dbo.View_Client_EXT ON dbo.TBL_INCIDENCE_CALL.ClientID = dbo.View_Client_EXT.ClientID INNER JOIN
                      dbo.App_UserDetails ON dbo.TBL_INCIDENCE_CALL.LoggedBy = dbo.App_UserDetails.UserID LEFT OUTER JOIN
                      dbo.tbl_Note ON dbo.TBL_INCIDENCE_CALL.NoteID = dbo.tbl_Note.NoteID LEFT OUTER JOIN
                      dbo.tbl_Asset ON dbo.TBL_INCIDENCE_CALL.AssetID = dbo.tbl_Asset.AssetID LEFT OUTER JOIN
                      dbo.View_Unit_EXT ON dbo.TBL_INCIDENCE_CALL.UnitID = dbo.View_Unit_EXT.UnitID ON 
                      dbo.View_Property_EXT.PropertyID = dbo.TBL_INCIDENCE_CALL.PropertyID RIGHT OUTER JOIN
                      dbo.Config_CallType INNER JOIN
                      dbo.Config_IncidentStatus INNER JOIN
                      dbo.TBL_INCIDENCE ON dbo.Config_IncidentStatus.StatusID = dbo.TBL_INCIDENCE.StatusID ON 
                      dbo.Config_CallType.CallTypeID = dbo.TBL_INCIDENCE.CallTypeID LEFT OUTER JOIN
                      dbo.Config_Status ON dbo.TBL_INCIDENCE.StatusID = dbo.Config_Status.StatusID ON 
                      dbo.TBL_INCIDENCE_CALL.CallID = dbo.TBL_INCIDENCE.CallID
WHERE     (dbo.Config_Status.Affects = 'call')


;

CREATE VIEW dbo.Report_Incidence_Actions
AS
SELECT     dbo.TBL_INCIDENCE.IncidentID, dbo.TBL_INCIDENCE_ACTIONITEM.ActionID, dbo.TBL_INCIDENCE_ACTIONITEM.ActionTitle, 
                      dbo.TBL_INCIDENCE_ACTIONITEM.ActionByDate, dbo.TBL_INCIDENCE_ACTIONITEM.TimeSpent, dbo.tbl_Supplier.SupplierName, 
                      dbo.Config_Status.StatusDesc, dbo.Config_Status.Affects, dbo.tbl_Note.NoteText
FROM         dbo.TBL_INCIDENCE_ACTIONITEM INNER JOIN
                      dbo.tbl_Supplier ON dbo.TBL_INCIDENCE_ACTIONITEM.AssignedSupplier = dbo.tbl_Supplier.SupplierID INNER JOIN
                      dbo.TBL_INCIDENCE ON dbo.TBL_INCIDENCE_ACTIONITEM.ActionID = dbo.TBL_INCIDENCE.ActionsID LEFT OUTER JOIN
                      dbo.tbl_Note ON dbo.TBL_INCIDENCE_ACTIONITEM.ActionRequiredNoteID = dbo.tbl_Note.NoteID LEFT OUTER JOIN
                      dbo.Config_Status ON dbo.TBL_INCIDENCE_ACTIONITEM.StatusID = dbo.Config_Status.StatusID
WHERE     (dbo.Config_Status.Affects = 'ACTION')

;
CREATE VIEW dbo.Report_Incidence_Costs
AS
SELECT     dbo.TBL_INCIDENCE.IncidentID, dbo.TBL_INCIDENCE_COSTITEM.CostID, dbo.tbl_Supplier.SupplierName, dbo.tbl_ChargeType.ChargeDesc, 
                      dbo.TBL_INCIDENCE_COSTITEM.Quantity, dbo.TBL_INCIDENCE_COSTITEM.TotalCost, dbo.TBL_INCIDENCE_COSTITEM.Rechargable
FROM         dbo.TBL_INCIDENCE_COSTITEM INNER JOIN
                      dbo.TBL_INCIDENCE ON dbo.TBL_INCIDENCE_COSTITEM.CostID = dbo.TBL_INCIDENCE.CostsID INNER JOIN
                      dbo.tbl_ChargeType ON dbo.TBL_INCIDENCE_COSTITEM.ChargeTypeID = dbo.tbl_ChargeType.ChargeTypeID INNER JOIN
                      dbo.tbl_Supplier ON dbo.TBL_INCIDENCE_COSTITEM.SupplierID = dbo.tbl_Supplier.SupplierID

;
CREATE VIEW dbo.Report_Incidence_Parts
AS
SELECT     dbo.TBL_INCIDENCE.IncidentID, dbo.TBL_INCIDENCE_PARTITEM.PartsID, dbo.TBL_INCIDENCE_PARTITEM.QuantityOrdered, 
                      dbo.TBL_INCIDENCE_PARTITEM.CostPerUnit, dbo.TBL_INCIDENCE_PARTITEM.SalesTotal, dbo.TBL_INCIDENCE_PARTITEM.DateOrdered, 
                      dbo.TBL_INCIDENCE_PARTITEM.DateInstalled, dbo.Config_OrderStatus.OrderStatusDesc, 
                      dbo.TBL_INCIDENCE_PARTITEM.CostPerUnit * dbo.TBL_INCIDENCE_PARTITEM.QuantityOrdered AS TotalCost, 
                      dbo.TBL_INCIDENCE_PARTITEM.SalesTotal * dbo.TBL_INCIDENCE_PARTITEM.QuantityOrdered AS TotalSales, 
                      dbo.TBL_INCIDENCE_PARTITEM.Rechargable, dbo.TBL_INCIDENCE_PARTITEM.PartDescription, dbo.tbl_Part.PartDesc, dbo.tbl_Part.CatalogueID, 
                      dbo.TBL_INCIDENCE_PARTITEM.SalesTotal AS SalesperUnit, dbo.tbl_Note.NoteText
FROM         dbo.TBL_INCIDENCE INNER JOIN
                      dbo.TBL_INCIDENCE_PARTITEM ON dbo.TBL_INCIDENCE.PartsID = dbo.TBL_INCIDENCE_PARTITEM.PartsID INNER JOIN
                      dbo.Config_OrderStatus ON dbo.TBL_INCIDENCE_PARTITEM.OrderStatusID = dbo.Config_OrderStatus.OrderStatusID LEFT OUTER JOIN
                      dbo.tbl_Part ON dbo.TBL_INCIDENCE_PARTITEM.OrderedPartID = dbo.tbl_Part.PartID LEFT OUTER JOIN
                      dbo.tbl_Note ON dbo.TBL_INCIDENCE_PARTITEM.PurchaseNoteID = dbo.tbl_Note.NoteID

;
CREATE VIEW dbo.Report_PPMPlanner
AS
SELECT     TOP 100 PERCENT dbo.tbl_Events.StartDateTime, dbo.tbl_Events.Description, dbo.tbl_Supplier.SupplierName, 
                      dbo.tbl_Asset.Description AS AssetName, dbo.View_Client_EXT.ClientName, dbo.View_Property_EXT.PropertyName, dbo.View_Unit_EXT.UnitName, 
                      dbo.Config_EventTypes.EventName, dbo.tbl_Events.EventId, dbo.tbl_Appointment.RecursionPattern, dbo.tbl_Appointment.RecursionFreq, 
                      dbo.tbl_Appointment.Subject, dbo.tbl_Appointment.Type
FROM         dbo.tbl_Events INNER JOIN
                      dbo.Config_EventTypes ON dbo.tbl_Events.Type = dbo.Config_EventTypes.EventTypeID INNER JOIN
                      dbo.tbl_Appointment ON dbo.tbl_Events.AppointmentRefID = dbo.tbl_Appointment.ID LEFT OUTER JOIN
                      dbo.View_Client_EXT ON dbo.tbl_Events.ClientID = dbo.View_Client_EXT.ClientID LEFT OUTER JOIN
                      dbo.View_Property_EXT ON dbo.tbl_Events.PropertyID = dbo.View_Property_EXT.PropertyID LEFT OUTER JOIN
                      dbo.View_Unit_EXT ON dbo.tbl_Events.UnitID = dbo.View_Unit_EXT.UnitID LEFT OUTER JOIN
                      dbo.tbl_Asset ON dbo.tbl_Events.AssetID = dbo.tbl_Asset.AssetID LEFT OUTER JOIN
                      dbo.tbl_Supplier ON dbo.tbl_Events.SupplierID = dbo.tbl_Supplier.SupplierID
ORDER BY dbo.tbl_Events.StartDateTime DESC

;

CREATE VIEW dbo.TreeView_CallsByStatus
AS
SELECT     TOP 100 PERCENT dbo.TBL_INCIDENCE.IncidentID, dbo.TBL_INCIDENCE_CALL.ClientID, dbo.Config_IncidentStatus.StatusDesc, 
                      dbo.Config_CallType.CallTypePrefix, dbo.TBL_INCIDENCE.OpenSince, dbo.Config_IncidentStatus.StatusID
FROM         dbo.TBL_INCIDENCE INNER JOIN
                      dbo.TBL_INCIDENCE_CALL ON dbo.TBL_INCIDENCE.CallID = dbo.TBL_INCIDENCE_CALL.CallID INNER JOIN
                      dbo.Config_IncidentStatus ON dbo.TBL_INCIDENCE.StatusID = dbo.Config_IncidentStatus.StatusID INNER JOIN
                      dbo.Config_CallType ON dbo.TBL_INCIDENCE.CallTypeID = dbo.Config_CallType.CallTypeID
ORDER BY dbo.TBL_INCIDENCE.IncidentID

;
CREATE VIEW dbo.VIEW1
AS
SELECT     'MTS' AS DocType, dbo.TBL_INCIDENCE.IncidentID AS DocNum, '1' AS RowNo, '02/03/06' AS DocDate, 
                      'GRP' + dbo.View_Supplier_EXT.AccountLedger AS NominalCode, dbo.View_Client_EXT.NominalCompanyCode, dbo.View_Supplier_EXT.AccountCode, 
                      dbo.View_Property_EXT.PropertyCode AS PropAccount, dbo.TBL_INCIDENCE_CALL.Subject, dbo.tbl_Note.NoteText, dbo.tbl_ChargeType.ChargeDesc, 
                      dbo.View_Client_EXT.NominalCompanyCode AS NCC, dbo.tbl_ChargeType.NominalCode AS Nominal2, dbo.TBL_INCIDENCE_COSTITEM.Quantity, 
                      dbo.tbl_ChargeType.UnitPrice, dbo.TBL_INCIDENCE_COSTITEM.TotalCost, dbo.TBL_INCIDENCE.IncidentName, 
                      dbo.TBL_INCIDENCE_COSTITEM.CostItemID, dbo.TBL_INCIDENCE.IncidentID AS CallNo, dbo.TBL_INCIDENCE_COSTITEM.CostID
FROM         dbo.TBL_INCIDENCE INNER JOIN
                      dbo.TBL_INCIDENCE_COSTITEM ON dbo.TBL_INCIDENCE.CostsID = dbo.TBL_INCIDENCE_COSTITEM.CostID INNER JOIN
                      dbo.TBL_INCIDENCE_CALL ON dbo.TBL_INCIDENCE.CallID = dbo.TBL_INCIDENCE_CALL.CallID INNER JOIN
                      dbo.tbl_Note ON dbo.TBL_INCIDENCE_COSTITEM.NoteID = dbo.tbl_Note.NoteID INNER JOIN
                      dbo.tbl_ChargeType ON dbo.TBL_INCIDENCE_COSTITEM.ChargeTypeID = dbo.tbl_ChargeType.ChargeTypeID AND 
                      dbo.TBL_INCIDENCE_COSTITEM.SupplierID = dbo.tbl_ChargeType.SupplierID INNER JOIN
                      dbo.View_Supplier_EXT ON dbo.tbl_ChargeType.SupplierID = dbo.View_Supplier_EXT.SupplierID LEFT OUTER JOIN
                      dbo.View_Client_EXT ON dbo.TBL_INCIDENCE_CALL.ClientID = dbo.View_Client_EXT.ClientID LEFT OUTER JOIN
                      dbo.View_Property_EXT ON dbo.TBL_INCIDENCE_CALL.PropertyID = dbo.View_Property_EXT.PropertyID

;

CREATE VIEW dbo.View_AssetUnit
AS
SELECT     dbo.tbl_Asset.*, dbo.tbl_Unit.UnitName AS UnitName
FROM         dbo.tbl_Asset LEFT OUTER JOIN
                      dbo.tbl_Unit ON dbo.tbl_Asset.UnitID = dbo.tbl_Unit.UnitID

;
CREATE VIEW dbo.View_Asset_EXT
AS
SELECT     dbo.tbl_Asset.AssetID, dbo.tbl_Asset.Description, dbo.tbl_AssetCate;ry.Cate;ry, dbo.tbl_Asset.DefaultPriority, dbo.tbl_Asset.ModelNo, 
                      dbo.tbl_Asset.SerialNo, dbo.tbl_Asset.AssetValue, dbo.tbl_Asset.DateInstalled, dbo.tbl_Asset.LastServiced, dbo.tbl_Asset.ServiceDue, 
                      dbo.tbl_Asset.Warrenty, dbo.tbl_Asset.PersonResponsible, dbo.tbl_Asset.SupplierResponsible, dbo.tbl_Asset.Manufacturer, 
                      dbo.tbl_Asset.WarrentyExpires, dbo.tbl_Asset.MultiNoteID, dbo.View_Unit_EXT.UnitName, dbo.View_Unit_EXT.EXTREF, dbo.tbl_Asset.UnitID, 
                      dbo.tbl_Asset.ClientID, dbo.tbl_Asset.PropertyID, dbo.View_Property_EXT.PropertyName, dbo.View_Property_EXT.ClientName, 
                      dbo.View_Unit_EXT.ContactID, dbo.View_Supplier_EXT.SupplierName, 'COMP:' + CONVERT(varchar(5), dbo.tbl_Asset.ClientID) 
                      + '-' + CONVERT(varchar(5), dbo.tbl_Asset.PropertyID) + '-' + CONVERT(varchar(5), dbo.tbl_Asset.UnitID) + '-' + CONVERT(varchar(5), 
                      dbo.tbl_Asset.AssetID) + '-0' AS tag
FROM         dbo.tbl_Asset INNER JOIN
                      dbo.tbl_AssetCate;ry ON dbo.tbl_Asset.Cate;ryID = dbo.tbl_AssetCate;ry.AssetCate;ryId INNER JOIN
                      dbo.View_Supplier_EXT ON dbo.tbl_Asset.SupplierResponsible = dbo.View_Supplier_EXT.SupplierID LEFT OUTER JOIN
                      dbo.View_Unit_EXT ON dbo.tbl_Asset.UnitID = dbo.View_Unit_EXT.UnitID LEFT OUTER JOIN
                      dbo.View_Property_EXT ON dbo.tbl_Asset.PropertyID = dbo.View_Property_EXT.PropertyID

;
CREATE VIEW dbo.View_Asset_Management_Assets
AS
SELECT     dbo.tbl_Asset.AssetID, dbo.tbl_Asset.Description, dbo.tbl_AssetCate;ry.Cate;ry, dbo.Config_Priority.PriorityDesc AS Priority, 
                      dbo.View_Unit_EXT.UnitName, dbo.View_Property_EXT.PropertyName, dbo.View_Client_EXT.ClientName AS Company, dbo.tbl_Asset.ModelNo, 
                      dbo.tbl_Asset.SerialNo, dbo.tbl_Asset.AssetValue, dbo.tbl_Asset.DateInstalled, dbo.tbl_Asset.Warrenty, dbo.tbl_Asset.LastServiced, 
                      dbo.tbl_Asset.ServiceDue, dbo.tbl_Asset.PersonResponsible, dbo.tbl_Supplier.SupplierName, dbo.View_Unit_EXT.UnitID AS Expr3, 
                      dbo.View_Property_EXT.PropertyID, dbo.View_Client_EXT.ClientID, dbo.tbl_Asset.PropertyID AS Expr1, dbo.tbl_Asset.ClientID AS CompanyID, 
                      dbo.tbl_Asset.UnitID, dbo.tbl_Asset.Manufacturer, dbo.tbl_Asset.WarrentyExpires, dbo.tbl_Asset.Description AS AssetName
FROM         dbo.tbl_Asset INNER JOIN
                      dbo.tbl_AssetCate;ry ON dbo.tbl_Asset.Cate;ryID = dbo.tbl_AssetCate;ry.AssetCate;ryId INNER JOIN
                      dbo.Config_Priority ON dbo.tbl_Asset.DefaultPriority = dbo.Config_Priority.PriorityId LEFT OUTER JOIN
                      dbo.View_Property_EXT ON dbo.tbl_Asset.PropertyID = dbo.View_Property_EXT.PropertyID LEFT OUTER JOIN
                      dbo.View_Unit_EXT ON dbo.tbl_Asset.UnitID = dbo.View_Unit_EXT.UnitID LEFT OUTER JOIN
                      dbo.View_Client_EXT ON dbo.tbl_Asset.ClientID = dbo.View_Client_EXT.ClientID LEFT OUTER JOIN
                      dbo.tbl_Supplier ON dbo.tbl_Asset.SupplierResponsible = dbo.tbl_Supplier.SupplierID

;
CREATE VIEW dbo.View_Asset_Management_Properties
AS
SELECT     landmark.dbo.qryUnionProperty.SeqID AS PropertyID, landmark.dbo.qryUnionProperty.Name AS PropertyName, 
                      landmark.dbo.qryUnionProperty.Reference AS EXTREF, landmark.dbo.qryUnionProperty.Address1, landmark.dbo.qryUnionProperty.Address2, 
                      landmark.dbo.qryUnionProperty.City, landmark.dbo.qryUnionProperty.Postcode, landmark.dbo.qryUnionProperty.Manager, 
                      dbo.View_Client_EXT.ClientID AS CompanyID, dbo.View_Client_EXT.ClientName AS Company, ISNULL(dbo.tbl_Property.Status, 1) AS HelpdeskStatus, 
                      dbo.tbl_Property.ManagerContactID, dbo.tbl_Property.ContactID, dbo.tbl_Property.AddressID, dbo.tbl_Property.MultiNoteID, 
                      dbo.tbl_Property.SingleTenant, landmark.dbo.qryUnionProperty.SectorReference, dbo.tbl_Property.NextInspection, dbo.tbl_Property.LastInspection, 
                      landmark.dbo.qryUnionProperty.CommentsMemo, dbo.tbl_Property.LedgerCode, dbo.tbl_Property.PropertyCode, 
                      dbo.View_Client_EXT.ClientID AS ClientID
FROM         landmark.dbo.qryUnionProperty INNER JOIN
                      dbo.View_Client_EXT ON landmark.dbo.qryUnionProperty.CompanyReference = dbo.View_Client_EXT.EXTREF LEFT OUTER JOIN
                      dbo.tbl_Property ON landmark.dbo.qryUnionProperty.SeqID = dbo.tbl_Property.PropertyID

;
CREATE VIEW dbo.View_Asset_Management_Schedules
AS
SELECT     dbo.tbl_Schedule.ID, dbo.tbl_Appointment.Subject, dbo.tbl_Asset.AssetID, dbo.tbl_Asset.Description, dbo.tbl_Supplier.SupplierName, 
                      dbo.tbl_Appointment.RecursionEndDate, dbo.tbl_Appointment.RecursionFreq, dbo.tbl_Appointment.RecursionPattern, 
                      dbo.tbl_Appointment.RecursionStartDate, dbo.tbl_Schedule.AppointmentID, dbo.tbl_Schedule.SupplierID, dbo.tbl_Appointment.EventStartDate, 
                      dbo.tbl_Appointment.EventEndDate, dbo.tbl_Appointment.Type, dbo.tbl_Appointment.NoteID, 
                      dbo.Config_AppointmentTypes.AppointmentType AS Eventname, dbo.tbl_Appointment.AllDay
FROM         dbo.Config_AppointmentTypes INNER JOIN
                      dbo.tbl_Appointment ON dbo.Config_AppointmentTypes.AppointmentTypeID = dbo.tbl_Appointment.Type RIGHT OUTER JOIN
                      dbo.View_Property_EXT RIGHT OUTER JOIN
                      dbo.View_Unit_EXT RIGHT OUTER JOIN
                      dbo.View_Client_EXT INNER JOIN
                      dbo.tbl_Schedule ON dbo.View_Client_EXT.ClientID = dbo.tbl_Schedule.ClientID ON dbo.View_Unit_EXT.UnitID = dbo.tbl_Schedule.UnitID ON 
                      dbo.View_Property_EXT.PropertyID = dbo.tbl_Schedule.PropertyID ON dbo.tbl_Appointment.ID = dbo.tbl_Schedule.AppointmentID LEFT OUTER JOIN
                      dbo.tbl_Asset ON dbo.tbl_Schedule.AssetID = dbo.tbl_Asset.AssetID LEFT OUTER JOIN
                      dbo.tbl_Supplier ON dbo.tbl_Schedule.SupplierID = dbo.tbl_Supplier.SupplierID

;
CREATE VIEW dbo.View_Asset_Management_Units
AS
SELECT     landmark.dbo.qryUnionUnit.Name AS UnitName, landmark.dbo.qryUnionUnit.Address1, landmark.dbo.qryUnionUnit.Address2, 
                      landmark.dbo.qryUnionUnit.City, landmark.dbo.qryUnionUnit.Postcode, landmark.dbo.qryUnionUnit.Reference AS EXTREF, 
                      landmark.dbo.qryUnionUnit.PropertyReference, dbo.View_Property_EXT.PropertyName, dbo.View_Property_EXT.ClientName AS Company, 
                      ISNULL(dbo.tbl_Unit.Status, 1) AS HelpdeskStatus, dbo.tbl_Unit.ContactID, dbo.tbl_Unit.ManagerContactID, dbo.tbl_Unit.TenantContactID, 
                      dbo.tbl_Unit.AddressID, dbo.tbl_Unit.MultiNoteID, landmark.dbo.qryUnionUnit.TenancyReference, landmark.dbo.tblTenancy.TenantName, 
                      landmark.dbo.tblTenancy.SalesLedgerAccountCode, landmark.dbo.qryUnionUnit.UnitManager, landmark.dbo.qryUnionUnit.SeqID AS UnitID, 
                      landmark.dbo.qryUnionUnit.UnitComments, landmark.dbo.qryUnionUnit.Address3, landmark.dbo.qryUnionUnit.Address4, 
                      landmark.dbo.qryUnionUnit.Country, dbo.View_Property_EXT.ClientID AS CompanyID, dbo.View_Property_EXT.PropertyID, 
                      dbo.View_Client_EXT.NominalDebtorControl, landmark.dbo.qryUnionUnit.County
FROM         dbo.View_Property_EXT INNER JOIN
                      landmark.dbo.qryUnionUnit ON dbo.View_Property_EXT.EXTREF = landmark.dbo.qryUnionUnit.PropertyReference INNER JOIN
                      dbo.View_Client_EXT ON dbo.View_Property_EXT.ClientID = dbo.View_Client_EXT.ClientID LEFT OUTER JOIN
                      landmark.dbo.tblTenancy ON landmark.dbo.qryUnionUnit.TenancyReference = landmark.dbo.tblTenancy.Reference LEFT OUTER JOIN
                      dbo.tbl_Unit ON landmark.dbo.qryUnionUnit.SeqID = dbo.tbl_Unit.UnitID

;
CREATE VIEW dbo.View_Build_InvoiceHeader
AS
SELECT     dbo.TBL_INCIDENCE.IncidentID, dbo.TBL_INCIDENCE.IncidentName, dbo.TBL_INCIDENCE_CALL.Subject, dbo.TBL_INCIDENCE_CALL.ChargeTo, 
                      dbo.TBL_INCIDENCE_CALL.CallerName, dbo.TBL_INCIDENCE_CALL.CallerPosition, dbo.View_Client_EXT.ClientName, dbo.View_Client_EXT.EXTREF, 
                      dbo.View_Client_EXT.Address1, dbo.View_Client_EXT.Address2, dbo.View_Client_EXT.City, dbo.View_Client_EXT.Postcode, 
                      dbo.View_Client_EXT.VatNumber, 'VAT001' AS VATNominal
FROM         dbo.TBL_INCIDENCE INNER JOIN
                      dbo.TBL_INCIDENCE_CALL ON dbo.TBL_INCIDENCE.CallID = dbo.TBL_INCIDENCE_CALL.CallID LEFT OUTER JOIN
                      dbo.View_Client_EXT ON dbo.TBL_INCIDENCE_CALL.ClientID = dbo.View_Client_EXT.ClientID

;
CREATE VIEW dbo.View_Build_InvoiceLine
AS
SELECT     dbo.TBL_INCIDENCE_CHARGEITEM.ChargeItemID, dbo.tbl_Rates.ChargeName, dbo.tbl_Rates.Description, 
                      dbo.TBL_INCIDENCE_CHARGEITEM.Quantity, dbo.TBL_INCIDENCE_CHARGEITEM.UnitPrice, dbo.TBL_INCIDENCE_CHARGEITEM.LineTotal, 
                      dbo.TBL_INCIDENCE_CHARGEITEM.Nominal AS ChargeNominal, dbo.TBL_INCIDENCE_CHARGEITEM.Control AS ChargeControl, 
                      dbo.TBL_INCIDENCE_CHARGEITEM.LineDescription, dbo.tbl_Rates.Nominal AS RateNominal, dbo.tbl_Rates.Control AS RateControl, 
                      dbo.TBL_INCIDENCE_CHARGEITEM.VATRate, dbo.TBL_INCIDENCE_CHARGEITEM.VatNominal
FROM         dbo.TBL_INCIDENCE_CHARGEITEM INNER JOIN
                      dbo.tbl_Rates ON dbo.TBL_INCIDENCE_CHARGEITEM.RateID = dbo.tbl_Rates.RateID

;
CREATE VIEW dbo.View_Build_PO_Lines
AS
SELECT     dbo.TBL_INCIDENCE_PARTITEM.PartsItemID, dbo.TBL_INCIDENCE_PARTITEM.OrderedPartID, dbo.tbl_Part.CatalogueID, dbo.tbl_Part.PartDesc, 
                      dbo.tbl_Part.Nominal, dbo.TBL_INCIDENCE_PARTITEM.QuantityOrdered, dbo.TBL_INCIDENCE_PARTITEM.CostPerUnit, 
                      dbo.TBL_INCIDENCE_PARTITEM.SalesTotal, dbo.TBL_INCIDENCE_PARTITEM.DateOrdered, dbo.TBL_INCIDENCE_PARTITEM.OrderStatusID, 
                      dbo.TBL_INCIDENCE_PARTITEM.Installed, dbo.TBL_INCIDENCE_PARTITEM.DateInstalled, dbo.TBL_INCIDENCE_PARTITEM.PartDescription, 
                      dbo.TBL_INCIDENCE_PARTITEM.TotalCost, dbo.TBL_INCIDENCE_PARTITEM.Rechargable, dbo.TBL_INCIDENCE_PARTITEM.SendToDream, 
                      dbo.TBL_INCIDENCE_PARTITEM.PurchaseOrderID, dbo.TBL_INCIDENCE_PARTITEM.PartsID, dbo.TBL_INCIDENCE.IncidentID
FROM         dbo.TBL_INCIDENCE_PARTITEM INNER JOIN
                      dbo.TBL_INCIDENCE ON dbo.TBL_INCIDENCE_PARTITEM.PartsID = dbo.TBL_INCIDENCE.PartsID INNER JOIN
                      dbo.tbl_Part ON dbo.TBL_INCIDENCE_PARTITEM.OrderedPartID = dbo.tbl_Part.PartID

;
CREATE VIEW dbo.View_Client
AS
SELECT     landmark.dbo.tblCompany.*, landmark.dbo.tblCompany.SeqID AS ClientID, landmark.dbo.tblCompany.Name AS ClientName, 
                      dbo.tbl_Client.Status AS HelpDeskStatus, dbo.tbl_Client.ContactID AS Expr1, dbo.tbl_Client.AddressID AS Expr2, dbo.tbl_Client.MultiNoteID AS Expr3, 
                      dbo.tbl_Client.EXTREF AS Expr4, dbo.tbl_Client.ClientComments AS Expr5, dbo.tbl_Client.ClientName AS Expr6
FROM         landmark.dbo.tblCompany FULL OUTER JOIN
                      dbo.tbl_Client ON landmark.dbo.tblCompany.SeqID = dbo.tbl_Client.ClientID

;

CREATE VIEW dbo.View_Client_EXT
AS
SELECT     landmark.dbo.tblCompany.SeqID AS ClientID, landmark.dbo.tblCompany.Name AS ClientName, landmark.dbo.tblCompany.Reference AS EXTREF, 
                      landmark.dbo.tblCompany.Address1, landmark.dbo.tblCompany.Address2, landmark.dbo.tblCompany.Address3, landmark.dbo.tblCompany.Address4, 
                      landmark.dbo.tblCompany.City, landmark.dbo.tblCompany.Postcode, landmark.dbo.tblCompany.County, landmark.dbo.tblCompany.Telephone, 
                      landmark.dbo.tblCompany.VatNumber, landmark.dbo.tblCompany.NominalCompanyCode, landmark.dbo.tblCompany.NominalDebtorControl, 
                      landmark.dbo.tblCompany.NominalCreditorControl, dbo.tbl_Client.ClientComments, ISNULL(dbo.tbl_Client.Status, 1) AS Status, dbo.tbl_Client.ContactID, 
                      dbo.tbl_Client.AddressID, dbo.tbl_Client.MultiNoteID, landmark.dbo.tblCompany.Fax, landmark.dbo.tblCompany.CommentsMemo, 
                      dbo.tbl_Client.SalesLedgerAccountCode, landmark.dbo.tblCompany.Country, dbo.tbl_Client.Status AS HelpdeskStatus, dbo.tbl_Client.VATRATE, 
                      dbo.tbl_Client.NominalCoCode, dbo.tbl_Client.NominalDebtCode, dbo.tbl_Client.NominalCredCode, 
                      dbo.tbl_Contact.Title + ' ' + dbo.tbl_Contact.FirstName + ' ' + dbo.tbl_Contact.LastName AS Contact, 'COMP:' + CONVERT(varchar(5), 
                      landmark.dbo.tblCompany.SeqID) + '-0-0-0-0' AS tag
FROM         dbo.tbl_Client LEFT OUTER JOIN
                      dbo.tbl_Contact ON dbo.tbl_Client.ContactID = dbo.tbl_Contact.ContactID RIGHT OUTER JOIN
                      landmark.dbo.tblCompany ON dbo.tbl_Client.ClientID = landmark.dbo.tblCompany.SeqID

;


CREATE VIEW dbo.View_Contact_EXT
AS
SELECT     dbo.tbl_Contact.Title COLLATE database_default AS Title, dbo.tbl_Contact.FirstName COLLATE database_default AS FirstName, 
                      dbo.tbl_Contact.LastName COLLATE database_default AS LastName, dbo.tbl_Contact.Company COLLATE database_default AS Company, 
                      dbo.tbl_Contact.[Position] COLLATE database_default AS [Position], dbo.View_Client_EXT.ClientName COLLATE database_default AS ClientName, 
                      dbo.View_Property_EXT.PropertyName COLLATE database_default AS PropertyName, 
                      dbo.View_Unit_EXT.UnitName COLLATE database_default UnitName, dbo.View_Unit_EXT.TenantName COLLATE database_default AS TenantName, 
                      dbo.tbl_Contact.ContactID AS ContactID, dbo.tbl_Contact.Telephone COLLATE database_default AS Telephone, 
                      dbo.tbl_Contact.Email COLLATE database_default Email, dbo.Tbl_ContactLink.ClientID, dbo.Tbl_ContactLink.PropertyID, dbo.Tbl_ContactLink.UnitID, 
                      dbo.Tbl_ContactLink.AssetID, '' AS Mobile
FROM         dbo.View_Unit_EXT RIGHT OUTER JOIN
                      dbo.tbl_ContactLink ON dbo.View_Unit_EXT.UnitID = dbo.tbl_ContactLink.UnitID LEFT OUTER JOIN
                      dbo.View_Property_EXT ON dbo.tbl_ContactLink.PropertyID = dbo.View_Property_EXT.PropertyID LEFT OUTER JOIN
                      dbo.View_Client_EXT ON dbo.tbl_ContactLink.ClientID = dbo.View_Client_EXT.ClientID LEFT OUTER JOIN
                      dbo.tbl_Contact ON dbo.tbl_ContactLink.ContactID = dbo.tbl_Contact.ContactID
UNION ALL
SELECT     LANDMARK.dbo.qryUnionTenancy.TenantContactSalutation AS Title, LANDMARK.dbo.qryUnionTenancy.TenantContactInitials AS FirstName, 
                      LANDMARK.dbo.qryUnionTenancy.TenantContactSurname AS LastName, LANDMARK.dbo.qryUnionTenancy.TenantName AS Company, '' AS [Position], 
                      dbo.View_Client_EXT.ClientName AS ClientName, LANDMARK.dbo.qryUnionProperty.Name AS PropertyName, dbo.View_Unit_EXT.UnitName, 
                      LANDMARK.dbo.qryUnionTenancy.TenantName, LANDMARK.dbo.qryUnionTenancy.SeqID AS ContactID, 
                      LANDMARK.dbo.qryUnionTenancy.Telephone AS Telephone, LANDMARK.dbo.qryUnionTenancy.TenantEMail AS Email, dbo.View_Client_EXT.ClientID, 
                      LANDMARK.dbo.qryUnionProperty.SeqID AS PropertyID, dbo.View_Unit_EXT.UnitID, - 1 AS AssetID, '' AS Mobile
FROM         LANDMARK.dbo.qryUnionTenancy INNER JOIN
                      LANDMARK.dbo.qryUnionProperty ON LANDMARK.dbo.qryUnionTenancy.PropertyReference = LANDMARK.dbo.qryUnionProperty.Reference INNER JOIN
                      dbo.View_Client_EXT ON LANDMARK.dbo.qryUnionProperty.CompanyReference = dbo.View_Client_EXT.EXTREF LEFT OUTER JOIN
                      dbo.View_Unit_EXT ON LANDMARK.dbo.qryUnionTenancy.Reference = dbo.View_Unit_EXT.TenancyReference AND 
                      LANDMARK.dbo.qryUnionTenancy.PropertyReference = dbo.View_Unit_EXT.PropertyReference
;
CREATE VIEW dbo.View_Contact_EXT
AS
SELECT     dbo.tbl_Contact.Title, dbo.tbl_Contact.FirstName, dbo.tbl_Contact.LastName, dbo.tbl_Contact.Company, dbo.tbl_Contact.[Position], 
                      dbo.View_Client_EXT.ClientName, dbo.View_Property_EXT.PropertyName, dbo.View_Unit_EXT.UnitName, dbo.View_Unit_EXT.TenantName, 
                      dbo.tbl_Contact.ContactID, dbo.tbl_Contact.Telephone, dbo.tbl_Contact.Email
FROM         dbo.View_Unit_EXT RIGHT OUTER JOIN
                      dbo.tbl_ContactLink ON dbo.View_Unit_EXT.UnitID = dbo.tbl_ContactLink.UnitID LEFT OUTER JOIN
                      dbo.View_Property_EXT ON dbo.tbl_ContactLink.PropertyID = dbo.View_Property_EXT.PropertyID LEFT OUTER JOIN
                      dbo.View_Client_EXT ON dbo.tbl_ContactLink.ClientID = dbo.View_Client_EXT.ClientID LEFT OUTER JOIN
                      dbo.tbl_Contact ON dbo.tbl_ContactLink.ContactID = dbo.tbl_Contact.ContactID

;
CREATE VIEW dbo.View_DreamIndex
AS
SELECT     'JOB' AS Ledger, dbo.TBL_INCIDENCE.IncidentName, 'PROPERTY' AS [Index], dbo.View_Property_EXT.EXTREF AS IndexItem
FROM         dbo.TBL_INCIDENCE INNER JOIN
                      dbo.TBL_INCIDENCE_CALL ON dbo.TBL_INCIDENCE.CallID = dbo.TBL_INCIDENCE_CALL.CallID INNER JOIN
                      dbo.TBL_INCIDENCE_BUDGET ON dbo.TBL_INCIDENCE.BudgetID = dbo.TBL_INCIDENCE_BUDGET.BudgetID INNER JOIN
                      dbo.View_Property_EXT ON dbo.TBL_INCIDENCE_CALL.PropertyID = dbo.View_Property_EXT.PropertyID
UNION ALL
SELECT     'JOB' AS Ledger, dbo.TBL_INCIDENCE.IncidentName, 'SERVICECHG' AS [Index], '2007AREPAIRS' AS Indexitem
FROM         dbo.TBL_INCIDENCE INNER JOIN
                      dbo.TBL_INCIDENCE_CALL ON dbo.TBL_INCIDENCE.CallID = dbo.TBL_INCIDENCE_CALL.CallID INNER JOIN
                      dbo.TBL_INCIDENCE_BUDGET ON dbo.TBL_INCIDENCE.BudgetID = dbo.TBL_INCIDENCE_BUDGET.BudgetID INNER JOIN
                      dbo.View_Property_EXT ON dbo.TBL_INCIDENCE_CALL.PropertyID = dbo.View_Property_EXT.PropertyID INNER JOIN
                      dbo.TBL_INCIDENCE_BUDGETITEM ON dbo.TBL_INCIDENCE_BUDGET.BudgetID = dbo.TBL_INCIDENCE_BUDGETITEM.BudgetID
GROUP BY dbo.TBL_INCIDENCE.IncidentName, dbo.View_Property_EXT.EXTREF, dbo.TBL_INCIDENCE_BUDGETITEM.Period, 
                      dbo.TBL_INCIDENCE_BUDGETITEM.Schedule, dbo.TBL_INCIDENCE_BUDGETITEM.Expense
HAVING      (dbo.TBL_INCIDENCE_BUDGETITEM.Period + dbo.TBL_INCIDENCE_BUDGETITEM.Schedule + dbo.TBL_INCIDENCE_BUDGETITEM.Expense <> '')


;
CREATE VIEW dbo.View_DreamSuppliersw
AS
SELECT     *, LEDGER AS Expr1
FROM         DREAMDB.SQSDBA.M_NAMEACCOUNT
WHERE     (LEDGER = 'suppliers')


;
CREATE view dbo.view_Dream_budgetbuilder1 as

SELECT     dbo.TBL_INCIDENCE_BUDGETITEM.BudgetItemID, dbo.App_Config.DreamBudgetDocType, dbo.Config_BudgetCate;ries.DebitCode, 
                      dbo.TBL_INCIDENCE.IncidentName, dbo.TBL_INCIDENCE_BUDGETITEM.NetTotal, dbo.TBL_INCIDENCE_BUDGETITEM.BudgetDate, 
                      dbo.TBL_INCIDENCE_CALL.Subject, dbo.App_Config.DreamBudgetTransType, dbo.Config_BudgetCate;ries.CreditCode, 
                      dbo.View_Property_EXT.PropertyCode, 
                      dbo.TBL_INCIDENCE_BUDGETITEM.Period + dbo.TBL_INCIDENCE_BUDGETITEM.Schedule + dbo.TBL_INCIDENCE_BUDGETITEM.Expense AS ServiceChg,
                       dbo.TBL_INCIDENCE_BUDGETITEM.NetTotal * - 1 AS ControlValue, dbo.TBL_INCIDENCE.IncidentName AS UserField1, 
                      dbo.TBL_INCIDENCE_BUDGETITEM.BudgetID, case dbo.TBL_INCIDENCE_BUDGETITEM.ServiceCharge when 0 then '' else dbo.View_Property_EXT.PropertyCode end as 'Account', 
case dbo.TBL_INCIDENCE_BUDGETITEM.ServiceCharge when 0 then '' else dbo.TBL_INCIDENCE_BUDGETITEM.Period + dbo.TBL_INCIDENCE_BUDGETITEM.Schedule + dbo.TBL_INCIDENCE_BUDGETITEM.Expense end as 'L3Account', dbo.TBL_INCIDENCE_BUDGETITEM.ServiceCharge AS ServiceChgFlag
FROM         dbo.TBL_INCIDENCE_CALL INNER JOIN
                      dbo.TBL_INCIDENCE_BUDGETITEM INNER JOIN
                      dbo.Config_BudgetCate;ries ON dbo.TBL_INCIDENCE_BUDGETITEM.Cate;ry = dbo.Config_BudgetCate;ries.BudgetCate;ryID INNER JOIN
                      dbo.TBL_INCIDENCE ON dbo.TBL_INCIDENCE_BUDGETITEM.BudgetID = dbo.TBL_INCIDENCE.BudgetID ON 
                      dbo.TBL_INCIDENCE_CALL.CallID = dbo.TBL_INCIDENCE.CallID LEFT OUTER JOIN
                      dbo.View_Property_EXT ON dbo.TBL_INCIDENCE_CALL.PropertyID = dbo.View_Property_EXT.PropertyID CROSS JOIN
                      dbo.App_Config

Where	dbo.TBL_INCIDENCE_BUDGETITEM.SentToDream = 0
;

CREATE VIEW dbo.View_Dream_JobSummary
AS
SELECT     TOP 100 PERCENT dbo.View_Dream_JobSummary2.L3LEDGER, dbo.View_Dream_JobSummary2.L3ACCOUNT, 
                      SUM(dbo.View_Dream_JobSummary2.BUDGET) AS BUDGETVALUE, SUM(dbo.View_Dream_JobSummary2.ACTUAL) AS ACTUALVALUE, 
                      SUM(dbo.View_Dream_JobSummary2.COMMITMENT) AS COMMITMENTVALUE, dbo.TBL_INCIDENCE.IncidentID, 
                      dbo.View_Dream_JobSummary2.TOTALCOSTS
FROM         dbo.View_Dream_JobSummary2 INNER JOIN
                      dbo.TBL_INCIDENCE ON 
                      dbo.View_Dream_JobSummary2.L3ACCOUNT = dbo.TBL_INCIDENCE.IncidentName COLLATE SQL_Latin1_General_CP1_CI_AS
GROUP BY dbo.View_Dream_JobSummary2.L3LEDGER, dbo.View_Dream_JobSummary2.L3ACCOUNT, dbo.TBL_INCIDENCE.IncidentID, 
                      dbo.View_Dream_JobSummary2.TOTALCOSTS
ORDER BY dbo.View_Dream_JobSummary2.L3ACCOUNT


;

CREATE VIEW dbo.View_Dream_JobSummary2
AS
SELECT     10000 AS BUDGET, 0 AS ACTUAL, 0 AS COMMITMENT, 'TST' AS L3ACCOUNT, 'LEG' AS L3LEDGER, 5000 AS TOTALCOSTS


;

CREATE  VIEW dbo.View_Dream_PurchaseOrderBuilder
AS
SELECT     dbo.TBL_INCIDENCE.IncidentName, dbo.tbl_INCIDENCE_PurchaseOrder.POID, 'HINV' AS DocType, dbo.tbl_POLineItem.POItemID, 
                      dbo.tbl_INCIDENCE_PurchaseOrder.PODate AS DocDate, '99999' AS Nominal, dbo.tbl_INCIDENCE_PurchaseOrder.Supplier AS Account, '' AS L3Account,
                       dbo.tbl_INCIDENCE_PurchaseOrder.Description, '' AS Quantity, SUM(dbo.tbl_POLineItem.LineTotal)*-1 AS Value, 
                      dbo.tbl_INCIDENCE_PurchaseOrder.Status, 'REG' AS Year, 'PINV' AS Period, 'COMMITMENT' as TranType, '1' as RowNo
FROM         dbo.TBL_INCIDENCE INNER JOIN
                      dbo.tbl_INCIDENCE_PurchaseOrder ON dbo.TBL_INCIDENCE.POID = dbo.tbl_INCIDENCE_PurchaseOrder.POID INNER JOIN
                      dbo.tbl_POLineItem ON dbo.tbl_INCIDENCE_PurchaseOrder.PurchaseOrderID = dbo.tbl_POLineItem.POItemID INNER JOIN
                      dbo.TBL_INCIDENCE_CALL ON dbo.TBL_INCIDENCE.CallID = dbo.TBL_INCIDENCE_CALL.CallID INNER JOIN
                      dbo.View_Property_EXT ON dbo.TBL_INCIDENCE_CALL.PropertyID = dbo.View_Property_EXT.PropertyID LEFT OUTER JOIN
                      dbo.Drm_PoItems ON dbo.tbl_POLineItem.Item = dbo.Drm_PoItems. PRODUCT COLLATE Latin1_General_CI_AS
GROUP BY dbo.TBL_INCIDENCE.IncidentName, dbo.tbl_INCIDENCE_PurchaseOrder.POID, dbo.tbl_POLineItem.POItemID, 
                      dbo.tbl_INCIDENCE_PurchaseOrder.PODate, dbo.tbl_INCIDENCE_PurchaseOrder.Supplier, dbo.tbl_INCIDENCE_PurchaseOrder.Description, 
                      dbo.tbl_INCIDENCE_PurchaseOrder.Status
HAVING      (dbo.tbl_INCIDENCE_PurchaseOrder.Status = 2)
UNION ALL
SELECT     dbo.TBL_INCIDENCE.IncidentName, dbo.tbl_INCIDENCE_PurchaseOrder.POID, 'HINV' AS DocType, dbo.tbl_POLineItem.POItemID, 
                      dbo.tbl_INCIDENCE_PurchaseOrder.PODate AS DocDate, dbo.Drm_PoItems.NOMINAL AS Nominal, dbo.View_Property_EXT.EXTREF AS Account, 
                      dbo.TBL_INCIDENCE.IncidentName AS L3Account, dbo.tbl_POLineItem.Description, dbo.tbl_POLineItem.Qty AS Quantity, 
                      dbo.tbl_POLineItem.LineTotal AS Value, dbo.tbl_INCIDENCE_PurchaseOrder.Status, 'REG' AS Year, 'PINV' AS Period, 'COMMITMENT' as TranType, '2' as RowNo
FROM         dbo.TBL_INCIDENCE INNER JOIN
                      dbo.tbl_INCIDENCE_PurchaseOrder ON dbo.TBL_INCIDENCE.POID = dbo.tbl_INCIDENCE_PurchaseOrder.POID INNER JOIN
                      dbo.tbl_POLineItem ON dbo.tbl_INCIDENCE_PurchaseOrder.PurchaseOrderID = dbo.tbl_POLineItem.POItemID INNER JOIN
                      dbo.TBL_INCIDENCE_CALL ON dbo.TBL_INCIDENCE.CallID = dbo.TBL_INCIDENCE_CALL.CallID INNER JOIN
                      dbo.View_Property_EXT ON dbo.TBL_INCIDENCE_CALL.PropertyID = dbo.View_Property_EXT.PropertyID LEFT OUTER JOIN
                      dbo.Drm_PoItems ON dbo.tbl_POLineItem.Item = dbo.Drm_PoItems. PRODUCT COLLATE Latin1_General_CI_AS
WHERE     (dbo.tbl_INCIDENCE_PurchaseOrder.Status = 2 AND dbo.tbl_Incidence_PurchaseOrder.SentToDream = 0)



;
CREATE VIEW dbo.View_Events
AS
SELECT     TOP 100 PERCENT dbo.tbl_Events.StartDateTime, dbo.tbl_Events.Description, dbo.tbl_Supplier.SupplierName, 
                      dbo.tbl_Asset.Description AS AssetName, dbo.View_Client_EXT.ClientName, dbo.View_Property_EXT.PropertyName, dbo.View_Unit_EXT.UnitName, 
                      dbo.Config_EventTypes.EventName, dbo.tbl_Events.EventId, dbo.tbl_Events.AppointmentRefID, dbo.tbl_Events.SupplierID, dbo.tbl_Events.ClientID, 
                      dbo.tbl_Events.PropertyID, dbo.tbl_Events.Subject, dbo.tbl_Events.TenantID, dbo.tbl_Events.EndDateTime, dbo.tbl_Events.AllDayEvent, 
                      dbo.tbl_Events.Type, dbo.tbl_Events.Location, dbo.tbl_Events.Owner
FROM         dbo.tbl_Events INNER JOIN
                      dbo.Config_EventTypes ON dbo.tbl_Events.Type = dbo.Config_EventTypes.EventTypeID LEFT OUTER JOIN
                      dbo.View_Client_EXT ON dbo.tbl_Events.ClientID = dbo.View_Client_EXT.ClientID LEFT OUTER JOIN
                      dbo.View_Property_EXT ON dbo.tbl_Events.PropertyID = dbo.View_Property_EXT.PropertyID LEFT OUTER JOIN
                      dbo.View_Unit_EXT ON dbo.tbl_Events.UnitID = dbo.View_Unit_EXT.UnitID LEFT OUTER JOIN
                      dbo.tbl_Asset ON dbo.tbl_Events.AssetID = dbo.tbl_Asset.AssetID LEFT OUTER JOIN
                      dbo.tbl_Supplier ON dbo.tbl_Events.SupplierID = dbo.tbl_Supplier.SupplierID
ORDER BY dbo.tbl_Events.StartDateTime DESC

;
CREATE VIEW dbo.View_Incidence
AS
SELECT     TOP 100 PERCENT dbo.TBL_INCIDENCE.IncidentName, dbo.TBL_INCIDENCE_CALL.ClientID, dbo.TBL_INCIDENCE_CALL.PropertyID, 
                      dbo.TBL_INCIDENCE_CALL.UnitID, dbo.TBL_INCIDENCE_CALL.AssetID, dbo.TBL_INCIDENCE.IncidentID, dbo.tbl_Supplier.SupplierName, 
                      dbo.TBL_INCIDENCE_CALL.CallerName, dbo.TBL_INCIDENCE_CALL.CallerTel, dbo.TBL_INCIDENCE_CALL.CallerPosition, 
                      dbo.Config_IncidentStatus.StatusDesc, dbo.Config_Priority.PriorityDesc, dbo.View_Client_EXT.ClientName, dbo.App_UserDetails.UserName, 
                      dbo.TBL_INCIDENCE.OpenSince, dbo.TBL_INCIDENCE_CALL.Subject, dbo.TBL_INCIDENCE_CALL.ChargeTo, 
                      dbo.TBL_INCIDENCE_CALL.RespondByDate, dbo.View_Unit_EXT.UnitName, dbo.tbl_Asset.Description, dbo.Config_CallType.CallTypeDesc, 
                      dbo.TBL_INCIDENCE.StatusID, dbo.TBL_INCIDENCE.CallTypeID, dbo.tbl_Supplier.SupplierID, dbo.TBL_INCIDENCE.ClosedAt, 
                      dbo.View_Property_EXT.PropertyName, dbo.View_Unit_EXT.EXTREF AS UnitReference, dbo.View_Property_EXT.EXTREF AS PropertyReference, 
                      dbo.View_Client_EXT.EXTREF AS CompanyReference, dbo.tbl_Supplier.SupplierID AS EngineerID, dbo.tbl_Note.NoteText, 
                      dbo.TBL_INCIDENCE_CALL.CallID, dbo.View_Tenant_EXT.TenantName, dbo.View_Tenant_EXT.TenantEMail, 
                      dbo.View_Tenant_EXT.Telephone AS TenantTel
FROM         dbo.tbl_Asset RIGHT OUTER JOIN
                      dbo.tbl_Supplier RIGHT OUTER JOIN
                      dbo.tbl_Contact RIGHT OUTER JOIN
                      dbo.App_UserDetails INNER JOIN
                      dbo.View_Client_EXT INNER JOIN
                      dbo.Config_Priority INNER JOIN
                      dbo.TBL_INCIDENCE_CALL ON dbo.Config_Priority.PriorityId = dbo.TBL_INCIDENCE_CALL.PriorityID ON 
                      dbo.View_Client_EXT.ClientID = dbo.TBL_INCIDENCE_CALL.ClientID INNER JOIN
                      dbo.Config_IncidentStatus INNER JOIN
                      dbo.TBL_INCIDENCE ON dbo.Config_IncidentStatus.StatusID = dbo.TBL_INCIDENCE.StatusID ON 
                      dbo.TBL_INCIDENCE_CALL.CallID = dbo.TBL_INCIDENCE.CallID INNER JOIN
                      dbo.Config_CallType ON dbo.TBL_INCIDENCE.CallTypeID = dbo.Config_CallType.CallTypeID ON 
                      dbo.App_UserDetails.UserID = dbo.TBL_INCIDENCE_CALL.LoggedBy LEFT OUTER JOIN
                      dbo.View_Tenant_EXT ON dbo.TBL_INCIDENCE_CALL.PropertyID = dbo.View_Tenant_EXT.PropertyID LEFT OUTER JOIN
                      dbo.tbl_Note ON dbo.TBL_INCIDENCE_CALL.NoteID = dbo.tbl_Note.NoteID ON dbo.tbl_Contact.ContactID = dbo.TBL_INCIDENCE_CALL.ContactID ON 
                      dbo.tbl_Supplier.SupplierID = dbo.TBL_INCIDENCE_CALL.SupplierID ON dbo.tbl_Asset.AssetID = dbo.TBL_INCIDENCE_CALL.AssetID LEFT OUTER JOIN
                      dbo.View_Unit_EXT ON dbo.TBL_INCIDENCE_CALL.UnitID = dbo.View_Unit_EXT.UnitID LEFT OUTER JOIN
                      dbo.View_Property_EXT ON dbo.TBL_INCIDENCE_CALL.PropertyID = dbo.View_Property_EXT.PropertyID
ORDER BY dbo.TBL_INCIDENCE_CALL.CallID

;
CREATE VIEW dbo.View_Incidence_Actions
AS
SELECT     dbo.TBL_INCIDENCE.IncidentID, dbo.TBL_INCIDENCE_ACTIONITEM.ActionID, dbo.TBL_INCIDENCE_ACTIONITEM.ActionTitle, 
                      dbo.TBL_INCIDENCE_ACTIONITEM.ActionByDate, dbo.TBL_INCIDENCE_ACTIONITEM.TimeSpent, dbo.tbl_Supplier.SupplierName, 
                      dbo.Config_Status.StatusDesc, dbo.Config_Status.Affects, dbo.tbl_Note.NoteText
FROM         dbo.TBL_INCIDENCE_ACTIONITEM INNER JOIN
                      dbo.tbl_Supplier ON dbo.TBL_INCIDENCE_ACTIONITEM.AssignedSupplier = dbo.tbl_Supplier.SupplierID INNER JOIN
                      dbo.TBL_INCIDENCE ON dbo.TBL_INCIDENCE_ACTIONITEM.ActionID = dbo.TBL_INCIDENCE.ActionsID LEFT OUTER JOIN
                      dbo.tbl_Note ON dbo.TBL_INCIDENCE_ACTIONITEM.ActionRequiredNoteID = dbo.tbl_Note.NoteID LEFT OUTER JOIN
                      dbo.Config_Status ON dbo.TBL_INCIDENCE_ACTIONITEM.StatusID = dbo.Config_Status.StatusID
WHERE     (dbo.Config_Status.Affects = 'ACTION')

;
CREATE VIEW dbo.View_Incidence_Charges
AS
SELECT     dbo.TBL_INCIDENCE.IncidentID, dbo.TBL_INCIDENCE_CHARGEITEM.ChargeID, dbo.tbl_Rates.ChargeName, 
                      dbo.TBL_INCIDENCE_CHARGEITEM.Quantity, dbo.TBL_INCIDENCE_CHARGEITEM.UnitPrice, dbo.TBL_INCIDENCE_CHARGEITEM.LineTotal, 
                      dbo.TBL_INCIDENCE_CALL.ClientID, dbo.TBL_INCIDENCE_CALL.PropertyID, dbo.TBL_INCIDENCE.CallTypeID
FROM         dbo.TBL_INCIDENCE INNER JOIN
                      dbo.TBL_INCIDENCE_CHARGEITEM ON dbo.TBL_INCIDENCE.ChargesID = dbo.TBL_INCIDENCE_CHARGEITEM.ChargeID INNER JOIN
                      dbo.tbl_Rates ON dbo.TBL_INCIDENCE_CHARGEITEM.RateID = dbo.tbl_Rates.RateID INNER JOIN
                      dbo.TBL_INCIDENCE_CALL ON dbo.TBL_INCIDENCE.CallID = dbo.TBL_INCIDENCE_CALL.CallID

;
CREATE VIEW dbo.View_Incidence_Costs
AS
SELECT     dbo.TBL_INCIDENCE.IncidentID, dbo.TBL_INCIDENCE_COSTITEM.CostID, dbo.tbl_Supplier.SupplierName, dbo.tbl_ChargeType.ChargeDesc, 
                      dbo.TBL_INCIDENCE_COSTITEM.Quantity, dbo.TBL_INCIDENCE_COSTITEM.TotalCost, dbo.TBL_INCIDENCE_COSTITEM.Rechargable
FROM         dbo.TBL_INCIDENCE_COSTITEM INNER JOIN
                      dbo.TBL_INCIDENCE ON dbo.TBL_INCIDENCE_COSTITEM.CostID = dbo.TBL_INCIDENCE.CostsID INNER JOIN
                      dbo.tbl_ChargeType ON dbo.TBL_INCIDENCE_COSTITEM.ChargeTypeID = dbo.tbl_ChargeType.ChargeTypeID INNER JOIN
                      dbo.tbl_Supplier ON dbo.TBL_INCIDENCE_COSTITEM.SupplierID = dbo.tbl_Supplier.SupplierID

;
CREATE VIEW dbo.View_Incidence_Invoices
AS
SELECT     dbo.tbl_Incidence_Invoices.CallID, dbo.tbl_Incidence_Invoices.InvoiceID, dbo.tbl_Incidence_Invoices.InvoiceNo, dbo.tbl_InvoiceLines.InvLineID, 
                      dbo.tbl_InvoiceLines.ChargeLineID, dbo.tbl_InvoiceLines.Qty, dbo.tbl_InvoiceLines.Price, 
                      dbo.tbl_InvoiceLines.Qty * dbo.tbl_InvoiceLines.Price AS CalcTotal, dbo.tbl_InvoiceLines.GrossAmount AS InvoicedTotal
FROM         dbo.tbl_Incidence_Invoices INNER JOIN
                      dbo.tbl_InvoiceLines ON dbo.tbl_Incidence_Invoices.InvoiceNo = dbo.tbl_InvoiceLines.InvoiceNo

;
CREATE VIEW dbo.View_Incidence_Parts
AS
SELECT     dbo.TBL_INCIDENCE.IncidentID, dbo.TBL_INCIDENCE_PARTITEM.PartsID, dbo.TBL_INCIDENCE_PARTITEM.QuantityOrdered, 
                      dbo.TBL_INCIDENCE_PARTITEM.CostPerUnit, dbo.TBL_INCIDENCE_PARTITEM.SalesTotal, dbo.TBL_INCIDENCE_PARTITEM.DateOrdered, 
                      dbo.TBL_INCIDENCE_PARTITEM.DateInstalled, dbo.Config_OrderStatus.OrderStatusDesc, 
                      dbo.TBL_INCIDENCE_PARTITEM.CostPerUnit * dbo.TBL_INCIDENCE_PARTITEM.QuantityOrdered AS TotalCost, 
                      dbo.TBL_INCIDENCE_PARTITEM.SalesTotal * dbo.TBL_INCIDENCE_PARTITEM.QuantityOrdered AS TotalSales, 
                      dbo.TBL_INCIDENCE_PARTITEM.Rechargable, dbo.TBL_INCIDENCE_PARTITEM.PartDescription, dbo.tbl_Part.PartDesc
FROM         dbo.TBL_INCIDENCE INNER JOIN
                      dbo.TBL_INCIDENCE_PARTITEM ON dbo.TBL_INCIDENCE.PartsID = dbo.TBL_INCIDENCE_PARTITEM.PartsID INNER JOIN
                      dbo.Config_OrderStatus ON dbo.TBL_INCIDENCE_PARTITEM.OrderStatusID = dbo.Config_OrderStatus.OrderStatusID INNER JOIN
                      dbo.tbl_Part ON dbo.TBL_INCIDENCE_PARTITEM.OrderedPartID = dbo.tbl_Part.PartID
;
CREATE VIEW dbo.View_IncidentAction
AS
SELECT     dbo.tbl_Action.*, dbo.tbl_Supplier.SupplierName AS Engineer, dbo.Config_ActionStatus.ActionStatus AS ActionStatus
FROM         dbo.tbl_Action LEFT OUTER JOIN
                      dbo.Config_ActionStatus ON dbo.tbl_Action.StatusID = dbo.Config_ActionStatus.ActionStatusID LEFT OUTER JOIN
                      dbo.tbl_Supplier ON dbo.tbl_Action.EngineerID = dbo.tbl_Supplier.SupplierID LEFT OUTER JOIN
                      dbo.tbl_Incident ON dbo.tbl_Action.IncidentID = dbo.tbl_Incident.IncidentID



;
CREATE VIEW dbo.View_IncidetNotes
AS
SELECT     dbo.tbl_Comment.*, dbo.tbl_CommentCate;ry.CommentCate;ry AS CommentCate;ry, dbo.App_UserDetails.UserName AS UserName
FROM         dbo.tbl_Comment INNER JOIN
                      dbo.tbl_CommentCate;ry ON dbo.tbl_Comment.CommentCtgID = dbo.tbl_CommentCate;ry.CommentCtgID INNER JOIN
                      dbo.App_UserDetails ON dbo.tbl_Comment.UserID = dbo.App_UserDetails.UserID



;

CREATE VIEW dbo.View_IncidentPart
AS
SELECT     dbo.tbl_POrder.*, dbo.tbl_Part.PartDesc AS PartDesc, dbo.Config_OrderStatus.OrderStatusDesc AS OrderStatusDesc
FROM         dbo.tbl_POrder LEFT OUTER JOIN
                      dbo.Config_OrderStatus ON dbo.tbl_POrder.OrderStatusID = dbo.Config_OrderStatus.OrderStatusID LEFT OUTER JOIN
                      dbo.tbl_Part ON dbo.tbl_POrder.PartID = dbo.tbl_Part.PartID



;
CREATE VIEW dbo.View_Invoice_Charges
AS
SELECT     dbo.TBL_INCIDENCE_CHARGEITEM.ChargeItemID, dbo.TBL_INCIDENCE_CHARGEITEM.Quantity, dbo.TBL_INCIDENCE_CHARGEITEM.UnitPrice, 
                      dbo.TBL_INCIDENCE_CHARGEITEM.LineTotal, dbo.TBL_INCIDENCE.IncidentName, dbo.TBL_INCIDENCE.IncidentID, dbo.tbl_Rates.ChargeName, 
                      dbo.tbl_Rates.Description, dbo.tbl_Rates.Charge, dbo.View_Client_EXT.ClientName, dbo.View_Unit_EXT.UnitName, 
                      dbo.View_Asset_EXT.Description AS AssetName, dbo.View_Property_EXT.PropertyName
FROM         dbo.TBL_INCIDENCE_CALL INNER JOIN
                      dbo.TBL_INCIDENCE ON dbo.TBL_INCIDENCE_CALL.CallID = dbo.TBL_INCIDENCE.IncidentID LEFT OUTER JOIN
                      dbo.View_Property_EXT ON dbo.TBL_INCIDENCE_CALL.PropertyID = dbo.View_Property_EXT.PropertyID LEFT OUTER JOIN
                      dbo.View_Client_EXT ON dbo.TBL_INCIDENCE_CALL.ClientID = dbo.View_Client_EXT.ClientID LEFT OUTER JOIN
                      dbo.View_Unit_EXT ON dbo.TBL_INCIDENCE_CALL.UnitID = dbo.View_Unit_EXT.UnitID LEFT OUTER JOIN
                      dbo.View_Asset_EXT ON dbo.TBL_INCIDENCE_CALL.AssetID = dbo.View_Asset_EXT.AssetID RIGHT OUTER JOIN
                      dbo.tbl_Rates INNER JOIN
                      dbo.TBL_INCIDENCE_CHARGEITEM ON dbo.tbl_Rates.RateID = dbo.TBL_INCIDENCE_CHARGEITEM.RateID ON 
                      dbo.TBL_INCIDENCE.ChargesID = dbo.TBL_INCIDENCE_CHARGEITEM.ChargeID

;
CREATE VIEW dbo.View_Invoice_InvoiceLines
AS
SELECT     Qty, Narrative, Description, Price, Vat, GrossAmount, NetAmount, Status, Nominal, Control, InvID, VatNominal, InvoiceNo
FROM         dbo.tbl_InvoiceLines

;
CREATE VIEW dbo.View_Part_EXT
AS
SELECT     dbo.tbl_Part.CatalogueID, dbo.tbl_Part.PartDesc, dbo.tbl_Part.Price, Dream.dbo.DRM_PARTS.SalesPrice, Dream.dbo.DRM_PARTS.UnitPrice, 
                      dbo.tbl_Part.Description, Dream.dbo.DRM_PARTS.NominalCode, Dream.dbo.DRM_PARTS.ExpenseCode, Dream.dbo.DRM_PARTS.InStock, 
                      dbo.tbl_Part.PartID
FROM         Dream.dbo.DRM_PARTS RIGHT OUTER JOIN
                      dbo.tbl_Part ON Dream.dbo.DRM_PARTS.PartID = dbo.tbl_Part.EXTREF

;

CREATE VIEW dbo.View_Property
AS
SELECT     Landmark.dbo.qryUnionProperty.*, Landmark.dbo.tblCompany.SeqID AS ClientID, Landmark.dbo.qryUnionProperty.SeqID AS PropertyID, 
                      Landmark.dbo.qryUnionProperty.Name AS PropertyName, dbo.tbl_Property.Status AS HelpDeskStatus, 
                      dbo.tbl_Property.ManagerContactID AS ManagerContactID, dbo.tbl_Property.ContactID AS ContactID, dbo.tbl_Property.AddressID AS AddressID, 
                      dbo.tbl_Property.MultiNoteID AS MultiNoteID, dbo.tbl_Property.EXTREF AS EXTREF, dbo.tbl_Property.PropertyComments AS PropertyComments, 
                      dbo.tbl_Property.LastInspection AS LastInspection, dbo.tbl_Property.NextInspection AS NextInspectionHD, dbo.tbl_Property.SingleTenant AS Expr1, 
                      dbo.tbl_Property.LedgerCode AS Expr2, dbo.tbl_Property.PropertyCode AS Expr3
FROM         Landmark.dbo.tblCompany INNER JOIN
                      Landmark.dbo.qryUnionProperty ON Landmark.dbo.tblCompany.Reference = Landmark.dbo.qryUnionProperty.CompanyReference LEFT OUTER JOIN
                      dbo.tbl_Property ON Landmark.dbo.qryUnionProperty.SeqID = dbo.tbl_Property.PropertyID


;
CREATE VIEW dbo.View_Property_EXT
AS
SELECT     landmark.dbo.qryUnionProperty.SeqID AS PropertyID, landmark.dbo.qryUnionProperty.Name AS PropertyName, 
                      landmark.dbo.qryUnionProperty.Reference AS EXTREF, landmark.dbo.qryUnionProperty.Address1, landmark.dbo.qryUnionProperty.Address2, 
                      landmark.dbo.qryUnionProperty.City, landmark.dbo.qryUnionProperty.Postcode, landmark.dbo.qryUnionProperty.Manager, 
                      dbo.View_Client_EXT.ClientID, dbo.View_Client_EXT.ClientName, ISNULL(dbo.tbl_Property.Status, 1) AS HelpdeskStatus, 
                      dbo.tbl_Property.ManagerContactID, dbo.tbl_Property.ContactID, dbo.tbl_Property.AddressID, dbo.tbl_Property.MultiNoteID, 
                      dbo.tbl_Property.SingleTenant, landmark.dbo.qryUnionProperty.SectorReference, dbo.tbl_Property.NextInspection, dbo.tbl_Property.LastInspection, 
                      landmark.dbo.qryUnionProperty.CommentsMemo, dbo.tbl_Property.LedgerCode, dbo.tbl_Property.PropertyCode, 
                      landmark.dbo.qryUnionProperty.Address3, landmark.dbo.qryUnionProperty.Address4, landmark.dbo.qryUnionProperty.County, 
                      landmark.dbo.qryUnionProperty.Country, 'COMP:' + CONVERT(varchar(5), dbo.View_Client_EXT.ClientID) + '-' + CONVERT(varchar(5), 
                      landmark.dbo.qryUnionProperty.SeqID) + '-0-0-0' AS tag, dbo.View_Tenant_EXT.TenantName, dbo.View_Tenant_EXT.TenantEMail, 
                      dbo.View_Tenant_EXT.Telephone AS TenantTelephone
FROM         dbo.View_Tenant_EXT RIGHT OUTER JOIN
                      dbo.tbl_Property ON dbo.View_Tenant_EXT.PropertyID = dbo.tbl_Property.PropertyID RIGHT OUTER JOIN
                      landmark.dbo.qryUnionProperty INNER JOIN
                      dbo.View_Client_EXT ON landmark.dbo.qryUnionProperty.CompanyReference = dbo.View_Client_EXT.EXTREF ON 
                      dbo.tbl_Property.PropertyID = landmark.dbo.qryUnionProperty.SeqID

;

CREATE VIEW dbo.View_PurchaseOrder
AS
SELECT     dbo.tbl_INCIDENCE_PurchaseOrder.PurchaseOrderID, dbo.tbl_INCIDENCE_PurchaseOrder.Supplier, dbo.tbl_INCIDENCE_PurchaseOrder.PONumber, 
                      dbo.tbl_INCIDENCE_PurchaseOrder.Description, dbo.tbl_INCIDENCE_PurchaseOrder.RequestedBy, dbo.tbl_INCIDENCE_PurchaseOrder.DateReq, 
                      dbo.tbl_INCIDENCE_PurchaseOrder.Status, dbo.tbl_INCIDENCE_PurchaseOrder.DelAddress, dbo.tbl_INCIDENCE_PurchaseOrder.InvAddress, 
                      dbo.tbl_INCIDENCE_PurchaseOrder.Delivery, dbo.tbl_POLineItem.POLineItemID, dbo.tbl_POLineItem.Item, 
                      dbo.tbl_POLineItem.Description AS LineDescription, dbo.tbl_POLineItem.Qty, dbo.tbl_POLineItem.UOM, dbo.tbl_POLineItem.Discount, 
                      dbo.tbl_POLineItem.Price, dbo.tbl_POLineItem.LineTotal
FROM         dbo.tbl_INCIDENCE_PurchaseOrder INNER JOIN
                      dbo.tbl_POLineItem ON dbo.tbl_INCIDENCE_PurchaseOrder.PurchaseOrderID = dbo.tbl_POLineItem.POItemID


;
CREATE VIEW dbo.View_Schedule
AS
SELECT     dbo.tbl_Schedule.ID, dbo.tbl_Appointment.Subject, dbo.tbl_Asset.AssetID, dbo.tbl_Asset.Description, dbo.tbl_Property.PropertyName, 
                      dbo.tbl_Unit.UnitName, dbo.tbl_Supplier.SupplierName, dbo.tbl_Appointment.RecursionEndDate, dbo.tbl_Appointment.RecursionFreq, 
                      dbo.tbl_Appointment.RecursionPattern, dbo.tbl_Client.ClientName, dbo.tbl_Appointment.RecursionStartDate, dbo.tbl_Schedule.AppointmentID, 
                      dbo.tbl_Schedule.SupplierID, dbo.Config_EventTypes.EventName, dbo.tbl_Appointment.EventStartDate, dbo.tbl_Appointment.EventEndDate, 
                      dbo.tbl_Appointment.Type
FROM         dbo.tbl_Schedule INNER JOIN
                      dbo.tbl_Asset ON dbo.tbl_Schedule.AssetID = dbo.tbl_Asset.AssetID INNER JOIN
                      dbo.tbl_Appointment ON dbo.tbl_Schedule.AppointmentID = dbo.tbl_Appointment.ID INNER JOIN
                      dbo.Config_EventTypes ON dbo.tbl_Appointment.Type = dbo.Config_EventTypes.EventTypeID LEFT OUTER JOIN
                      dbo.tbl_Supplier ON dbo.tbl_Schedule.SupplierID = dbo.tbl_Supplier.SupplierID LEFT OUTER JOIN
                      dbo.tbl_Client ON dbo.tbl_Schedule.ClientID = dbo.tbl_Client.ClientID LEFT OUTER JOIN
                      dbo.tbl_Unit ON dbo.tbl_Schedule.UnitID = dbo.tbl_Unit.UnitID LEFT OUTER JOIN
                      dbo.tbl_Property ON dbo.tbl_Schedule.PropertyID = dbo.tbl_Property.PropertyID

;

CREATE VIEW dbo.View_Search_Actions
AS
SELECT     dbo.TBL_INCIDENCE_ACTIONITEM.ActionByDate, dbo.View_Incidence.IncidentName, dbo.View_Supplier_EXT.SupplierName, 
                      dbo.Config_SupplierType.Description AS SupplierType, dbo.TBL_INCIDENCE_ACTIONITEM.ActionTitle, dbo.TBL_INCIDENCE_ACTIONITEM.TimeSpent, 
                      dbo.TBL_INCIDENCE_ACTIONITEM.Escalate, dbo.Config_ActionStatus.ActionStatus, dbo.TBL_INCIDENCE.IncidentID
FROM         dbo.TBL_INCIDENCE INNER JOIN
                      dbo.TBL_INCIDENCE_ACTIONITEM ON dbo.TBL_INCIDENCE.ActionsID = dbo.TBL_INCIDENCE_ACTIONITEM.ActionID INNER JOIN
                      dbo.View_Incidence ON dbo.TBL_INCIDENCE.IncidentID = dbo.View_Incidence.IncidentID INNER JOIN
                      dbo.Config_ActionStatus ON dbo.TBL_INCIDENCE_ACTIONITEM.StatusID = dbo.Config_ActionStatus.ActionStatusID INNER JOIN
                      dbo.Config_SupplierType INNER JOIN
                      dbo.View_Supplier_EXT ON dbo.Config_SupplierType.TypeID = dbo.View_Supplier_EXT.SupplierTypeID ON 
                      dbo.TBL_INCIDENCE_ACTIONITEM.AssignedSupplier = dbo.View_Supplier_EXT.SupplierID

;
CREATE VIEW dbo.View_Search_Calls
AS
SELECT     dbo.TBL_INCIDENCE.IncidentName, dbo.TBL_INCIDENCE_CALL.Subject, dbo.TBL_INCIDENCE_CALL.CallerName, 
                      dbo.TBL_INCIDENCE_CALL.CallerPosition, dbo.TBL_INCIDENCE_CALL.CallerTel, dbo.tbl_Asset.Description, 
                      dbo.View_Property_EXT.EXTREF AS PropertyReference, dbo.tbl_Supplier.SupplierName, dbo.App_UserDetails.UserName AS LoggedBy, 
                      dbo.TBL_INCIDENCE.OpenSince AS DateLogged, dbo.View_Unit_EXT.EXTREF AS UnitReference, dbo.View_Property_EXT.PropertyName, 
                      dbo.View_Unit_EXT.UnitName, dbo.View_Client_EXT.ClientName, dbo.Config_CallType.CallTypeDesc, dbo.TBL_INCIDENCE_CALL.ClientID, 
                      dbo.TBL_INCIDENCE_CALL.PropertyID, dbo.TBL_INCIDENCE_CALL.UnitID, dbo.TBL_INCIDENCE_CALL.AssetID, dbo.TBL_INCIDENCE.CallTypeID, 
                      dbo.tbl_Supplier.SupplierID, dbo.View_Client_EXT.EXTREF AS CompanyReference, dbo.TBL_INCIDENCE.IncidentID, dbo.tbl_Contact.ContactID, 
                      dbo.tbl_Contact.FirstName, dbo.tbl_Contact.LastName
FROM         dbo.View_Property_EXT RIGHT OUTER JOIN
                      dbo.Config_Priority INNER JOIN
                      dbo.tbl_Supplier INNER JOIN
                      dbo.TBL_INCIDENCE_CALL ON dbo.tbl_Supplier.SupplierID = dbo.TBL_INCIDENCE_CALL.SupplierID ON 
                      dbo.Config_Priority.PriorityId = dbo.TBL_INCIDENCE_CALL.PriorityID INNER JOIN
                      dbo.View_Client_EXT ON dbo.TBL_INCIDENCE_CALL.ClientID = dbo.View_Client_EXT.ClientID INNER JOIN
                      dbo.App_UserDetails ON dbo.TBL_INCIDENCE_CALL.LoggedBy = dbo.App_UserDetails.UserID LEFT OUTER JOIN
                      dbo.tbl_Contact ON dbo.TBL_INCIDENCE_CALL.ContactID = dbo.tbl_Contact.ContactID LEFT OUTER JOIN
                      dbo.tbl_Asset ON dbo.TBL_INCIDENCE_CALL.AssetID = dbo.tbl_Asset.AssetID LEFT OUTER JOIN
                      dbo.View_Unit_EXT ON dbo.TBL_INCIDENCE_CALL.UnitID = dbo.View_Unit_EXT.UnitID ON 
                      dbo.View_Property_EXT.PropertyID = dbo.TBL_INCIDENCE_CALL.PropertyID RIGHT OUTER JOIN
                      dbo.Config_CallType INNER JOIN
                      dbo.Config_IncidentStatus INNER JOIN
                      dbo.TBL_INCIDENCE ON dbo.Config_IncidentStatus.StatusID = dbo.TBL_INCIDENCE.StatusID ON 
                      dbo.Config_CallType.CallTypeID = dbo.TBL_INCIDENCE.CallTypeID ON dbo.TBL_INCIDENCE_CALL.CallID = dbo.TBL_INCIDENCE.CallID
;
CREATE VIEW dbo.View_Search_Contacts
AS
SELECT     dbo.tbl_Contact.Title, dbo.tbl_Contact.FirstName, dbo.tbl_Contact.LastName, dbo.tbl_Contact.Company, dbo.tbl_Contact.[Position], 
                      dbo.tbl_Contact.Telephone, dbo.tbl_Contact.Email, dbo.tbl_Address.Address1, dbo.tbl_Address.Address2, dbo.tbl_Address.City, 
                      dbo.tbl_Address.Postcode, dbo.tbl_Contact.ContactID
FROM         dbo.tbl_Contact LEFT OUTER JOIN
                      dbo.tbl_Address ON dbo.tbl_Contact.AddressID = dbo.tbl_Address.AddressID

;

CREATE VIEW dbo.View_Search_PurchaseOrders
AS
SELECT     dbo.tbl_INCIDENCE_PurchaseOrder.PONumber, dbo.tbl_INCIDENCE_PurchaseOrder.Description, dbo.tbl_INCIDENCE_PurchaseOrder.PODate, 
                      dbo.TBL_INCIDENCE.IncidentName, dbo.View_DreamSuppliersw.ADDRESSNAME, dbo.tbl_INCIDENCE_PurchaseOrder.Supplier,
                          (SELECT     SUM(tbl_POLineItem.LineTotal)
                            FROM          tbl_POLineItem
                            WHERE      tbl_POLineItem.POItemID = PurchaseOrderID) AS TotalOrder, dbo.tbl_INCIDENCE_PurchaseOrder.SentToDream, 
                      dbo.Config_Status.StatusDesc AS Status, dbo.TBL_INCIDENCE.IncidentID
FROM         dbo.tbl_INCIDENCE_PurchaseOrder INNER JOIN
                      dbo.Config_Status ON dbo.tbl_INCIDENCE_PurchaseOrder.Status = dbo.Config_Status.StatusID AND 
                      dbo.Config_Status.Affects = 'PURCHASEORDER' INNER JOIN
                      dbo.TBL_INCIDENCE ON dbo.tbl_INCIDENCE_PurchaseOrder.POID = dbo.TBL_INCIDENCE.POID INNER JOIN
                      dbo.View_DreamSuppliersw ON dbo.tbl_INCIDENCE_PurchaseOrder.Supplier = dbo.View_DreamSuppliersw.ACCOUNT COLLATE Latin1_General_CI_AS

;
CREATE VIEW dbo.View_Search_Suppliers
AS
SELECT     dbo.View_Supplier_EXT.SupplierName, dbo.Config_SupplierType.Description AS Type, dbo.View_Supplier_EXT.AccountLedger, 
                      dbo.View_Supplier_EXT.AccountCode, dbo.View_Supplier_EXT.DreamAddress, dbo.tbl_Address.Address1, dbo.tbl_Address.Address2, 
                      dbo.tbl_Address.City, dbo.tbl_Address.County, dbo.tbl_Address.Postcode, dbo.tbl_Address.Country, dbo.tbl_Contact.Title, dbo.tbl_Contact.FirstName, 
                      dbo.tbl_Contact.LastName, dbo.tbl_Contact.[Position], dbo.tbl_Contact.Telephone, dbo.tbl_Contact.Fax, dbo.tbl_Contact.Email, 
                      dbo.View_Supplier_EXT.SupplierID
FROM         dbo.View_Supplier_EXT INNER JOIN
                      dbo.Config_SupplierType ON dbo.View_Supplier_EXT.SupplierTypeID = dbo.Config_SupplierType.TypeID LEFT OUTER JOIN
                      dbo.tbl_Contact ON dbo.View_Supplier_EXT.ContactID = dbo.tbl_Contact.ContactID LEFT OUTER JOIN
                      dbo.tbl_Address ON dbo.View_Supplier_EXT.AddressID = dbo.tbl_Address.AddressID

;
CREATE VIEW dbo.View_Supplier_EXT
AS
SELECT     dbo.tbl_Supplier.*, ' ' AS DreamAddress, dbo.Config_SupplierType.Description AS Cate;ry, 
                      dbo.tbl_Contact.Title + ' ' + dbo.tbl_Contact.FirstName + ' ' + dbo.tbl_Contact.LastName AS Contact, dbo.tbl_Address.Address1, 
                      dbo.tbl_Address.Address2, dbo.tbl_Address.Address3, dbo.tbl_Address.Address4, dbo.tbl_Address.City, dbo.tbl_Address.County, 
                      dbo.tbl_Address.Postcode, dbo.tbl_Address.Country
FROM         dbo.tbl_Supplier LEFT OUTER JOIN
                      dbo.tbl_Address ON dbo.tbl_Supplier.AddressID = dbo.tbl_Address.AddressID LEFT OUTER JOIN
                      dbo.tbl_Contact ON dbo.tbl_Supplier.ContactID = dbo.tbl_Contact.ContactID LEFT OUTER JOIN
                      dbo.Config_SupplierType ON dbo.tbl_Supplier.SupplierTypeID = dbo.Config_SupplierType.TypeID

;

CREATE VIEW dbo.View_Tenant_EXT
AS
SELECT     TOP 100 PERCENT LANDMARK.dbo.tblTenancy.SeqID, LANDMARK.dbo.tblTenancy.TenantName, LANDMARK.dbo.tblTenancy.TenantAddress1, 
                      LANDMARK.dbo.tblTenancy.TenantAddress2, LANDMARK.dbo.tblTenancy.TenantAddress3, LANDMARK.dbo.tblTenancy.TenantAddress4, 
                      LANDMARK.dbo.tblTenancy.TenantCity, LANDMARK.dbo.tblTenancy.TenantCounty, LANDMARK.dbo.tblTenancy.TenantPostcode, 
                      LANDMARK.dbo.tblTenancy.TenantCountry, LANDMARK.dbo.tblTenancy.TenantEMail, LANDMARK.dbo.tblTenancy.Telephone, 
                      LANDMARK.dbo.tblTenancy.Fax, LANDMARK.dbo.tblProperty.SeqID AS PropertyID, LANDMARK.dbo.tblProperty.Name AS PropertyName, 
                      LANDMARK.dbo.tblUnit.SeqID AS UnitID, LANDMARK.dbo.tblUnit.Name AS UnitName, LANDMARK.dbo.tblTenancy.SalesLedgerAccountCode, 
                      LANDMARK.dbo.tblTenancy.Reference AS EXTREF, LANDMARK.dbo.tblCompany.SeqID AS ClientID, LANDMARK.dbo.tblCompany.NominalDebtorControl, 
                      LANDMARK.dbo.tblTenancy.SeqID AS UniqueID, LANDMARK.dbo.tblUnit.Reference AS UnitRef
FROM         LANDMARK.dbo.tblCompany INNER JOIN
                      LANDMARK.dbo.tblProperty ON LANDMARK.dbo.tblCompany.Reference = LANDMARK.dbo.tblProperty.CompanyReference INNER JOIN
                      LANDMARK.dbo.tblUnit ON LANDMARK.dbo.tblProperty.Reference = LANDMARK.dbo.tblUnit.PropertyReference INNER JOIN
                      LANDMARK.dbo.tblTenancy ON LANDMARK.dbo.tblUnit.PropertyReference = LANDMARK.dbo.tblTenancy.PropertyReference AND 
                      LANDMARK.dbo.tblUnit.TenancyReference = LANDMARK.dbo.tblTenancy.Reference
ORDER BY LANDMARK.dbo.tblTenancy.SeqID
;

CREATE VIEW dbo.View_Timeline_Schedules
AS
SELECT     dbo.tbl_Events.StartDateTime, dbo.tbl_Events.EndDateTime, dbo.tbl_Events.Subject, dbo.tbl_Events.Type, dbo.View_Client_EXT.EXTREF AS ClientRef, 
                      dbo.View_Property_EXT.EXTREF AS PropertyRef, dbo.View_Unit_EXT.EXTREF AS UnitRef, dbo.tbl_Asset.Description, 
                      dbo.tbl_Supplier.SupplierName
FROM         dbo.tbl_Events LEFT OUTER JOIN
                      dbo.tbl_Supplier ON dbo.tbl_Events.SupplierID = dbo.tbl_Supplier.SupplierID LEFT OUTER JOIN
                      dbo.View_Unit_EXT ON dbo.tbl_Events.UnitID = dbo.View_Unit_EXT.UnitID LEFT OUTER JOIN
                      dbo.tbl_Asset ON dbo.tbl_Events.AssetID = dbo.tbl_Asset.AssetID LEFT OUTER JOIN
                      dbo.View_Property_EXT ON dbo.tbl_Events.PropertyID = dbo.View_Property_EXT.PropertyID LEFT OUTER JOIN
                      dbo.View_Client_EXT ON dbo.tbl_Events.ClientID = dbo.View_Client_EXT.ClientID
WHERE     (dbo.tbl_Events.AppointmentRefID <> 0)

;

CREATE VIEW dbo.View_Timesheet_Builder
AS
SELECT     dbo.TBL_INCIDENCE_COSTITEM.UpdateAccounts, dbo.TBL_INCIDENCE_COSTITEM.TimesheetID, dbo.TBL_INCIDENCE.IncidentName, 
                      dbo.TBL_INCIDENCE.IncidentID, dbo.TBL_INCIDENCE.CostsID, dbo.TBL_INCIDENCE_COSTITEM.CostID, dbo.Drmview_Timesheet.DocType, 
                      dbo.Drmview_Timesheet.DocNum, dbo.Drmview_Timesheet.RowNo, dbo.Drmview_Timesheet.DocDate, dbo.Drmview_Timesheet.NominalCode, 
                      dbo.Drmview_Timesheet.NominalCompanyCode, dbo.Drmview_Timesheet.AccountCode, dbo.Drmview_Timesheet.Subject, 
                      dbo.Drmview_Timesheet.NoteText, dbo.Drmview_Timesheet.NCC, dbo.Drmview_Timesheet.Nominal2, dbo.Drmview_Timesheet.Quantity, 
                      dbo.Drmview_Timesheet.UnitPrice, dbo.tbl_ChargeType.Charge, dbo.tbl_ChargeType.ChargeDesc, dbo.TBL_INCIDENCE_COSTITEM.TotalCost
FROM         dbo.TBL_INCIDENCE INNER JOIN
                      dbo.TBL_INCIDENCE_COSTITEM ON dbo.TBL_INCIDENCE.CostsID = dbo.TBL_INCIDENCE_COSTITEM.CostID INNER JOIN
                      dbo.tbl_ChargeType ON dbo.TBL_INCIDENCE_COSTITEM.ChargeTypeID = dbo.tbl_ChargeType.ChargeTypeID LEFT OUTER JOIN
                      dbo.Drmview_Timesheet ON dbo.TBL_INCIDENCE_COSTITEM.CostItemID = dbo.Drmview_Timesheet.CostLineID

;

CREATE VIEW dbo.View_Unit
AS
SELECT     landmark.dbo.qryUnionUnit.*, landmark.dbo.qryUnionUnit.SeqID AS UnitID, landmark.dbo.qryUnionUnit.Name AS UnitName, 
                      dbo.tbl_Unit.Status AS HelpDeskStatus, landmark.dbo.qryUnionProperty.SeqID AS PropertyID, landmark.dbo.tblCompany.SeqID AS ClientID, 
                      dbo.tbl_Unit.ContactID AS ContactID, dbo.tbl_Unit.ManagerContactID AS ManagerContactID, dbo.tbl_Unit.AddressID AS AddressID, 
                      dbo.tbl_Unit.MultiNoteID AS MultiNoteID, landmark.dbo.tblTenancy.DateOccupied AS DateOccupied, 
                      case when landmark.dbo.tblTenancy.TenantName is null then '' else  landmark.dbo.tblTenancy.TenantName end AS TenantName, landmark.dbo.tblTenancy.TenantContact AS TenantContact, 
                      landmark.dbo.tblTenancy.TenantEMail AS TenantEMail, landmark.dbo.tblTenancy.Telephone AS Telephone, 
                      dbo.tbl_Unit.TenantContactID AS TenantContactID, dbo.tbl_Unit.UnitComments AS UnitCommentsHD
FROM         landmark.dbo.qryUnionUnit INNER JOIN
                      landmark.dbo.qryUnionProperty ON landmark.dbo.qryUnionUnit.PropertyReference = landmark.dbo.qryUnionProperty.Reference INNER JOIN
                      landmark.dbo.tblCompany ON landmark.dbo.qryUnionProperty.CompanyReference = landmark.dbo.tblCompany.Reference LEFT OUTER JOIN
                      landmark.dbo.tblTenancy ON landmark.dbo.qryUnionUnit.TenancyReference = landmark.dbo.tblTenancy.Reference LEFT OUTER JOIN
                      dbo.tbl_Unit ON landmark.dbo.qryUnionUnit.SeqID = dbo.tbl_Unit.UnitID


;

CREATE VIEW dbo.View_Unit2
AS
SELECT     landmark.dbo.qryUnionUnit.SeqID AS UnitID, landmark.dbo.qryUnionUnit.Name AS UnitName, dbo.tbl_Unit.Status AS HelpDeskStatus, 
                      landmark.dbo.qryUnionProperty.SeqID AS PropertyID, landmark.dbo.tblCompany.SeqID AS ClientID
FROM         landmark.dbo.qryUnionUnit INNER JOIN
                      landmark.dbo.qryUnionProperty ON landmark.dbo.qryUnionUnit.PropertyReference = landmark.dbo.qryUnionProperty.Reference INNER JOIN
                      landmark.dbo.tblCompany ON landmark.dbo.qryUnionProperty.CompanyReference = landmark.dbo.tblCompany.Reference LEFT OUTER JOIN
                      dbo.tbl_Unit ON landmark.dbo.qryUnionUnit.SeqID = dbo.tbl_Unit.UnitID

;
CREATE VIEW dbo.View_Unit_EXT
AS
SELECT     landmark.dbo.qryUnionUnit.Name AS UnitName, landmark.dbo.qryUnionUnit.Address1, landmark.dbo.qryUnionUnit.Address2, 
                      landmark.dbo.qryUnionUnit.City, landmark.dbo.qryUnionUnit.Postcode, landmark.dbo.qryUnionUnit.Reference AS EXTREF, 
                      landmark.dbo.qryUnionUnit.PropertyReference, dbo.View_Property_EXT.PropertyName, dbo.View_Property_EXT.ClientName, 
                      ISNULL(dbo.tbl_Unit.Status, 1) AS HelpdeskStatus, dbo.tbl_Unit.ContactID, dbo.tbl_Unit.ManagerContactID, dbo.tbl_Unit.TenantContactID, 
                      dbo.tbl_Unit.AddressID, dbo.tbl_Unit.MultiNoteID, landmark.dbo.qryUnionUnit.TenancyReference, landmark.dbo.tblTenancy.TenantName, 
                      landmark.dbo.tblTenancy.SalesLedgerAccountCode, landmark.dbo.qryUnionUnit.UnitManager, landmark.dbo.qryUnionUnit.SeqID AS UnitID, 
                      landmark.dbo.qryUnionUnit.UnitComments, landmark.dbo.qryUnionUnit.Address3, landmark.dbo.qryUnionUnit.Address4, 
                      landmark.dbo.qryUnionUnit.Country, dbo.View_Property_EXT.ClientID, dbo.View_Property_EXT.PropertyID, dbo.View_Client_EXT.NominalDebtorControl,
                       landmark.dbo.qryUnionUnit.County, 'COMP:' + CONVERT(varchar(5), dbo.View_Client_EXT.ClientID) + '-' + CONVERT(varchar(5), 
                      dbo.View_Property_EXT.PropertyID) + '-' + CONVERT(varchar(5), dbo.tbl_Unit.UnitID) + '-0-0' AS tag, landmark.dbo.tblTenancy.TenantEMail, 
                      landmark.dbo.tblTenancy.Telephone AS TenantTelephone
FROM         dbo.View_Property_EXT INNER JOIN
                      landmark.dbo.qryUnionUnit ON dbo.View_Property_EXT.EXTREF = landmark.dbo.qryUnionUnit.PropertyReference INNER JOIN
                      dbo.View_Client_EXT ON dbo.View_Property_EXT.ClientID = dbo.View_Client_EXT.ClientID LEFT OUTER JOIN
                      landmark.dbo.tblTenancy ON landmark.dbo.qryUnionUnit.TenancyReference = landmark.dbo.tblTenancy.Reference LEFT OUTER JOIN
                      dbo.tbl_Unit ON landmark.dbo.qryUnionUnit.SeqID = dbo.tbl_Unit.UnitID

;
CREATE VIEW dbo.View_invoices
AS
SELECT     dbo.tbl_Invoice.InvoiceID, dbo.tbl_Invoice.InvType, dbo.tbl_Invoice.InvNo, dbo.tbl_Invoice.InvRef, dbo.tbl_Invoice.InvDate, dbo.tbl_Invoice.Address1, 
                      dbo.tbl_Invoice.Address2, dbo.tbl_Invoice.Address3, dbo.tbl_Invoice.City, dbo.tbl_Invoice.PostCode, dbo.tbl_Invoice.Text, dbo.tbl_Invoice.Total, 
                      dbo.tbl_Invoice.Status, dbo.tbl_Invoice.Nominal, dbo.tbl_Invoice.Control, dbo.tbl_Invoice.TotalNet, dbo.tbl_Invoice.TotalVat, dbo.tbl_Invoice.TotalGross, 
                      dbo.tbl_Invoice.VatNominal, dbo.tbl_Invoice.CallRef, dbo.tbl_InvoiceLines.InvLineID, dbo.tbl_InvoiceLines.InvLineNo, dbo.tbl_InvoiceLines.InvoiceNo, 
                      dbo.tbl_InvoiceLines.ChargeLineID, dbo.tbl_InvoiceLines.Nominal AS ExpenseNominal, dbo.tbl_InvoiceLines.Control AS ExpenseControl, 
                      dbo.tbl_InvoiceLines.Qty, dbo.tbl_InvoiceLines.Price, dbo.tbl_InvoiceLines.Description, dbo.tbl_InvoiceLines.Narrative, 
                      dbo.tbl_InvoiceLines.Status AS ExpStatus, dbo.tbl_InvoiceLines.InvID, dbo.tbl_InvoiceLines.NetAmount, dbo.tbl_InvoiceLines.Vat, 
                      dbo.tbl_InvoiceLines.GrossAmount, dbo.tbl_Invoice.Discount, dbo.tbl_Invoice.Invoicee, dbo.App_Config.DreamInvoiceDocType
FROM         dbo.tbl_Invoice INNER JOIN
                      dbo.tbl_InvoiceLines ON dbo.tbl_Invoice.InvNo = dbo.tbl_InvoiceLines.InvoiceNo CROSS JOIN
                      dbo.App_Config


;
CREATE VIEW dbo.tblcontrol
AS
SELECT     *
FROM         LANDMARK.dbo.tblControl

;
CREATE VIEW dbo.view_CallStatus
AS
SELECT     dbo.TBL_INCIDENCE_CALL.CallID, dbo.TBL_INCIDENCE.OpenSince, dbo.tbl_Client.ClientName, dbo.tbl_Property.PropertyName, 
                      dbo.tbl_Charge.LineTotal, dbo.tbl_Cost.TotalAmount, dbo.tbl_Action.TimeTaken, dbo.Config_Status.StatusDesc, dbo.Config_Status.StatusID, 
                      dbo.TBL_INCIDENCE.IncidentID
FROM         dbo.Config_Status LEFT OUTER JOIN
                      dbo.TBL_INCIDENCE ON dbo.Config_Status.StatusID = dbo.TBL_INCIDENCE.StatusID LEFT OUTER JOIN
                      dbo.TBL_INCIDENCE_CALL ON dbo.TBL_INCIDENCE.CallID = dbo.TBL_INCIDENCE_CALL.CallID LEFT OUTER JOIN
                      dbo.tbl_Property ON dbo.TBL_INCIDENCE_CALL.PropertyID = dbo.tbl_Property.PropertyID LEFT OUTER JOIN
                      dbo.tbl_Client ON dbo.TBL_INCIDENCE_CALL.ClientID = dbo.tbl_Client.ClientID LEFT OUTER JOIN
                      dbo.tbl_Charge ON dbo.TBL_INCIDENCE.IncidentID = dbo.tbl_Charge.IncidentID LEFT OUTER JOIN
                      dbo.tbl_Cost ON dbo.TBL_INCIDENCE.IncidentID = dbo.tbl_Cost.IncidentID LEFT OUTER JOIN
                      dbo.tbl_Action ON dbo.TBL_INCIDENCE.IncidentID = dbo.tbl_Action.IncidentID
WHERE     (dbo.Config_Status.Affects = 'CALL')


;
CREATE VIEW dbo.view_CallSummary
AS
SELECT     dbo.View_Client_EXT.ClientName, dbo.View_Property_EXT.PropertyName, dbo.View_Unit_EXT.UnitName, dbo.View_Unit_EXT.TenantName, 
                      dbo.View_Supplier_EXT.SupplierName, dbo.TBL_INCIDENCE_CALL.CallerTel, dbo.TBL_INCIDENCE_CALL.CallerName, 
                      dbo.TBL_INCIDENCE_CALL.Subject, dbo.TBL_INCIDENCE_CALL.CallerPosition, dbo.TBL_INCIDENCE_CALL.RespondByDate, dbo.tbl_Note.NoteText, 
                      dbo.TBL_INCIDENCE.IncidentName, dbo.tbl_Asset.Description, dbo.tbl_Asset.ModelNo, dbo.tbl_Asset.SerialNo, dbo.tbl_Asset.AssetValue, 
                      dbo.tbl_Asset.DateInstalled, dbo.tbl_Asset.LastServiced, dbo.tbl_Asset.ServiceDue, dbo.tbl_Asset.Warrenty, dbo.tbl_Asset.Manufacturer, 
                      dbo.tbl_Asset.WarrentyExpires, dbo.tbl_Asset.PersonResponsible, dbo.tbl_Asset.SupplierResponsible, dbo.Config_Priority.PriorityDesc, 
                      dbo.Config_Priority.BusinessDays, dbo.TBL_INCIDENCE.IncidentID, dbo.TBL_INCIDENCE.OpenSince, dbo.Config_CallType.CallTypeDesc, 
                      'Test' AS Test, dbo.View_Property_EXT.Address1, dbo.View_Property_EXT.Address2, dbo.View_Property_EXT.City, dbo.View_Property_EXT.Postcode, 
                      dbo.View_Property_EXT.Manager, dbo.App_UserDetails.UserName, dbo.TBL_INCIDENCE.StatusID, dbo.Config_Status.StatusDesc, 
                      dbo.View_Property_EXT.EXTREF, dbo.Config_Status.Affects, dbo.tbl_Incidence_Info.Info1, dbo.tbl_Incidence_Info.Info2, dbo.tbl_Incidence_Info.Info3, 
                      dbo.tbl_Incidence_Info.Info4, dbo.tbl_Incidence_Info.Info5, dbo.tbl_Incidence_Info.Info6, dbo.tbl_Incidence_Info.Info7, dbo.tbl_Incidence_Info.Info8, 
                      dbo.tbl_Incidence_Info.Info9, dbo.tbl_Incidence_Info.Info10, dbo.tbl_Incidence_Info.Info11, dbo.tbl_Incidence_Info.Info12, dbo.tbl_Incidence_Info.Info13, 
                      dbo.tbl_Incidence_Info.Info14, dbo.tbl_Incidence_Info.Info15, dbo.tbl_Incidence_Info.Info16, dbo.tbl_Incidence_Info.Info17, 
                      dbo.tbl_Incidence_Info.Info18, dbo.tbl_Incidence_Info.Info19, dbo.tbl_Incidence_Info.Info20, dbo.TBL_INCIDENCE.CallID, 
                      tbl_Note_1.NoteText AS ActionNoteText, tbl_Note_1.Subject AS ActionSubject, dbo.TBL_INCIDENCE_ACTIONITEM.ActionID, 
                      dbo.TBL_INCIDENCE_ACTIONITEM.ActionTitle, dbo.TBL_INCIDENCE_ACTIONITEM.ActionByDate, dbo.TBL_INCIDENCE_ACTIONITEM.AssignedSupplier, 
                      dbo.tbl_Supplier.SupplierName AS ActionSupplier
FROM         dbo.tbl_Note tbl_Note_1 RIGHT OUTER JOIN
                      dbo.TBL_INCIDENCE_ACTIONITEM INNER JOIN
                      dbo.TBL_INCIDENCE_ACTION ON dbo.TBL_INCIDENCE_ACTIONITEM.ActionID = dbo.TBL_INCIDENCE_ACTION.ActionID ON 
                      tbl_Note_1.NoteID = dbo.TBL_INCIDENCE_ACTIONITEM.ActionRequiredNoteID LEFT OUTER JOIN
                      dbo.tbl_Supplier ON dbo.TBL_INCIDENCE_ACTIONITEM.AssignedSupplier = dbo.tbl_Supplier.SupplierID RIGHT OUTER JOIN
                      dbo.View_Supplier_EXT INNER JOIN
                      dbo.TBL_INCIDENCE INNER JOIN
                      dbo.TBL_INCIDENCE_CALL ON dbo.TBL_INCIDENCE.CallID = dbo.TBL_INCIDENCE_CALL.CallID ON 
                      dbo.View_Supplier_EXT.SupplierID = dbo.TBL_INCIDENCE_CALL.SupplierID INNER JOIN
                      dbo.tbl_Note ON dbo.TBL_INCIDENCE_CALL.NoteID = dbo.tbl_Note.NoteID INNER JOIN
                      dbo.Config_Priority ON dbo.TBL_INCIDENCE_CALL.PriorityID = dbo.Config_Priority.PriorityId INNER JOIN
                      dbo.Config_CallType ON dbo.TBL_INCIDENCE.CallTypeID = dbo.Config_CallType.CallTypeID INNER JOIN
                      dbo.App_UserDetails ON dbo.TBL_INCIDENCE_CALL.Loggedby = dbo.App_UserDetails.UserID LEFT OUTER JOIN
                      dbo.tbl_Incidence_Info ON dbo.TBL_INCIDENCE.InfoID = dbo.tbl_Incidence_Info.InfoID LEFT OUTER JOIN
                      dbo.Config_Status ON dbo.TBL_INCIDENCE.StatusID = dbo.Config_Status.StatusID ON 
                      dbo.TBL_INCIDENCE_ACTION.ActionID = dbo.TBL_INCIDENCE.ActionsID LEFT OUTER JOIN
                      dbo.tbl_Asset ON dbo.TBL_INCIDENCE_CALL.AssetID = dbo.tbl_Asset.AssetID LEFT OUTER JOIN
                      dbo.View_Client_EXT ON dbo.TBL_INCIDENCE_CALL.ClientID = dbo.View_Client_EXT.ClientID LEFT OUTER JOIN
                      dbo.View_Property_EXT ON dbo.TBL_INCIDENCE_CALL.PropertyID = dbo.View_Property_EXT.PropertyID LEFT OUTER JOIN
                      dbo.View_Unit_EXT ON dbo.TBL_INCIDENCE_CALL.UnitID = dbo.View_Unit_EXT.UnitID
WHERE     (dbo.Config_Status.Affects = 'call')



;

CREATE VIEW dbo.view_EngWorkOrder
AS
SELECT     dbo.View_Client_EXT.ClientName, dbo.View_Property_EXT.PropertyName, dbo.View_Unit_EXT.UnitName, dbo.View_Unit_EXT.TenantName, 
                      dbo.View_Supplier_EXT.SupplierName, dbo.TBL_INCIDENCE_CALL.CallerTel, dbo.TBL_INCIDENCE_CALL.CallerName, 
                      dbo.TBL_INCIDENCE_CALL.Subject, dbo.TBL_INCIDENCE_CALL.CallerPosition, dbo.TBL_INCIDENCE_CALL.RespondByDate, dbo.tbl_Note.NoteText, 
                      dbo.TBL_INCIDENCE.IncidentName, dbo.tbl_Asset.Description, dbo.tbl_Asset.ModelNo, dbo.tbl_Asset.SerialNo, dbo.tbl_Asset.AssetValue, 
                      dbo.tbl_Asset.DateInstalled, dbo.tbl_Asset.LastServiced, dbo.tbl_Asset.ServiceDue, dbo.tbl_Asset.Warrenty, dbo.tbl_Asset.Manufacturer, 
                      dbo.tbl_Asset.WarrentyExpires, dbo.tbl_Asset.PersonResponsible, dbo.tbl_Asset.SupplierResponsible, dbo.Config_Priority.PriorityDesc, 
                      dbo.Config_Priority.BusinessDays, dbo.TBL_INCIDENCE.IncidentID, dbo.TBL_INCIDENCE.OpenSince, dbo.Config_CallType.CallTypeDesc, 
                      'Test' AS Test, dbo.View_Property_EXT.Address1, dbo.View_Property_EXT.Address2, dbo.View_Property_EXT.City, dbo.View_Property_EXT.Postcode, 
                      dbo.View_Property_EXT.Manager, dbo.TBL_INCIDENCE.StatusID, dbo.Config_Status.StatusDesc, dbo.View_Property_EXT.EXTREF, 
                      dbo.Config_Status.Affects, dbo.tbl_Incidence_Info.Info1, dbo.tbl_Incidence_Info.Info2, dbo.tbl_Incidence_Info.Info3, dbo.tbl_Incidence_Info.Info4, 
                      dbo.tbl_Incidence_Info.Info5, dbo.tbl_Incidence_Info.Info6, dbo.tbl_Incidence_Info.Info7, dbo.tbl_Incidence_Info.Info8, dbo.tbl_Incidence_Info.Info9, 
                      dbo.tbl_Incidence_Info.Info10, dbo.tbl_Incidence_Info.Info11, dbo.tbl_Incidence_Info.Info12, dbo.tbl_Incidence_Info.Info13, 
                      dbo.tbl_Incidence_Info.Info14, dbo.tbl_Incidence_Info.Info15, dbo.tbl_Incidence_Info.Info16, dbo.tbl_Incidence_Info.Info17, 
                      dbo.tbl_Incidence_Info.Info18, dbo.tbl_Incidence_Info.Info19, dbo.tbl_Incidence_Info.Info20, dbo.App_UserDetails.UserName, 
                      dbo.TBL_INCIDENCE_CALL.SupplierID
FROM         dbo.View_Supplier_EXT INNER JOIN
                      dbo.TBL_INCIDENCE INNER JOIN
                      dbo.TBL_INCIDENCE_CALL ON dbo.TBL_INCIDENCE.CallID = dbo.TBL_INCIDENCE_CALL.CallID ON 
                      dbo.View_Supplier_EXT.SupplierID = dbo.TBL_INCIDENCE_CALL.SupplierID INNER JOIN
                      dbo.tbl_Note ON dbo.TBL_INCIDENCE_CALL.NoteID = dbo.tbl_Note.NoteID INNER JOIN
                      dbo.Config_Priority ON dbo.TBL_INCIDENCE_CALL.PriorityID = dbo.Config_Priority.PriorityId INNER JOIN
                      dbo.Config_CallType ON dbo.TBL_INCIDENCE.CallTypeID = dbo.Config_CallType.CallTypeID LEFT OUTER JOIN
                      dbo.App_UserDetails ON dbo.TBL_INCIDENCE_CALL.LoggedBy = dbo.App_UserDetails.UserID LEFT OUTER JOIN
                      dbo.tbl_Incidence_Info ON dbo.TBL_INCIDENCE.InfoID = dbo.tbl_Incidence_Info.InfoID LEFT OUTER JOIN
                      dbo.Config_Status ON dbo.TBL_INCIDENCE.StatusID = dbo.Config_Status.StatusID LEFT OUTER JOIN
                      dbo.TBL_INCIDENCE_ACTIONITEM INNER JOIN
                      dbo.TBL_INCIDENCE_ACTION ON dbo.TBL_INCIDENCE_ACTIONITEM.ActionID = dbo.TBL_INCIDENCE_ACTION.ActionID ON 
                      dbo.TBL_INCIDENCE.ActionsID = dbo.TBL_INCIDENCE_ACTION.ActionID LEFT OUTER JOIN
                      dbo.tbl_Asset ON dbo.TBL_INCIDENCE_CALL.AssetID = dbo.tbl_Asset.AssetID LEFT OUTER JOIN
                      dbo.View_Client_EXT ON dbo.TBL_INCIDENCE_CALL.ClientID = dbo.View_Client_EXT.ClientID LEFT OUTER JOIN
                      dbo.View_Property_EXT ON dbo.TBL_INCIDENCE_CALL.PropertyID = dbo.View_Property_EXT.PropertyID LEFT OUTER JOIN
                      dbo.View_Unit_EXT ON dbo.TBL_INCIDENCE_CALL.UnitID = dbo.View_Unit_EXT.UnitID
WHERE     (dbo.Config_Status.Affects = 'call')


;
CREATE VIEW dbo.view_EngWorkOrderAction
AS
SELECT     dbo.View_Client_EXT.ClientName, dbo.View_Property_EXT.PropertyName, dbo.View_Unit_EXT.UnitName, dbo.View_Unit_EXT.TenantName, 
                      dbo.View_Supplier_EXT.SupplierName, dbo.TBL_INCIDENCE_CALL.CallerTel, dbo.TBL_INCIDENCE_CALL.CallerName, 
                      dbo.TBL_INCIDENCE_CALL.Subject, dbo.TBL_INCIDENCE_CALL.CallerPosition, dbo.TBL_INCIDENCE_CALL.RespondByDate, dbo.tbl_Note.NoteText, 
                      dbo.TBL_INCIDENCE.IncidentName, dbo.tbl_Asset.Description, dbo.tbl_Asset.ModelNo, dbo.tbl_Asset.SerialNo, dbo.tbl_Asset.AssetValue, 
                      dbo.tbl_Asset.DateInstalled, dbo.tbl_Asset.LastServiced, dbo.tbl_Asset.ServiceDue, dbo.tbl_Asset.Warrenty, dbo.tbl_Asset.Manufacturer, 
                      dbo.tbl_Asset.WarrentyExpires, dbo.tbl_Asset.PersonResponsible, dbo.tbl_Asset.SupplierResponsible, dbo.Config_Priority.PriorityDesc, 
                      dbo.Config_Priority.BusinessDays, dbo.TBL_INCIDENCE.IncidentID, dbo.TBL_INCIDENCE.OpenSince, dbo.Config_CallType.CallTypeDesc, 
                      'Test' AS Test, dbo.View_Property_EXT.Address1, dbo.View_Property_EXT.Address2, dbo.View_Property_EXT.City, dbo.View_Property_EXT.Postcode, 
                      dbo.View_Property_EXT.Manager, dbo.TBL_INCIDENCE.StatusID, dbo.Config_Status.StatusDesc, dbo.View_Property_EXT.EXTREF, 
                      dbo.Config_Status.Affects, dbo.tbl_Incidence_Info.Info1, dbo.tbl_Incidence_Info.Info2, dbo.tbl_Incidence_Info.Info3, dbo.tbl_Incidence_Info.Info4, 
                      dbo.tbl_Incidence_Info.Info5, dbo.tbl_Incidence_Info.Info6, dbo.tbl_Incidence_Info.Info7, dbo.tbl_Incidence_Info.Info8, dbo.tbl_Incidence_Info.Info9, 
                      dbo.tbl_Incidence_Info.Info10, dbo.tbl_Incidence_Info.Info11, dbo.tbl_Incidence_Info.Info12, dbo.tbl_Incidence_Info.Info13, 
                      dbo.tbl_Incidence_Info.Info14, dbo.tbl_Incidence_Info.Info15, dbo.tbl_Incidence_Info.Info16, dbo.tbl_Incidence_Info.Info17, 
                      dbo.tbl_Incidence_Info.Info18, dbo.tbl_Incidence_Info.Info19, dbo.tbl_Incidence_Info.Info20, dbo.App_UserDetails.UserName, 
                      dbo.TBL_INCIDENCE_CALL.SupplierID
FROM         dbo.View_Supplier_EXT INNER JOIN
                      dbo.TBL_INCIDENCE INNER JOIN
                      dbo.TBL_INCIDENCE_CALL ON dbo.TBL_INCIDENCE.CallID = dbo.TBL_INCIDENCE_CALL.CallID ON 
                      dbo.View_Supplier_EXT.SupplierID = dbo.TBL_INCIDENCE_CALL.SupplierID INNER JOIN
                      dbo.tbl_Note ON dbo.TBL_INCIDENCE_CALL.NoteID = dbo.tbl_Note.NoteID INNER JOIN
                      dbo.Config_Priority ON dbo.TBL_INCIDENCE_CALL.PriorityID = dbo.Config_Priority.PriorityId INNER JOIN
                      dbo.Config_CallType ON dbo.TBL_INCIDENCE.CallTypeID = dbo.Config_CallType.CallTypeID LEFT OUTER JOIN
                      dbo.App_UserDetails ON dbo.TBL_INCIDENCE_CALL.LoggedBy = dbo.App_UserDetails.UserID LEFT OUTER JOIN
                      dbo.tbl_Incidence_Info ON dbo.TBL_INCIDENCE.InfoID = dbo.tbl_Incidence_Info.InfoID LEFT OUTER JOIN
                      dbo.Config_Status ON dbo.TBL_INCIDENCE.StatusID = dbo.Config_Status.StatusID LEFT OUTER JOIN
                      dbo.TBL_INCIDENCE_ACTIONITEM INNER JOIN
                      dbo.TBL_INCIDENCE_ACTION ON dbo.TBL_INCIDENCE_ACTIONITEM.ActionID = dbo.TBL_INCIDENCE_ACTION.ActionID ON 
                      dbo.TBL_INCIDENCE.ActionsID = dbo.TBL_INCIDENCE_ACTION.ActionID LEFT OUTER JOIN
                      dbo.tbl_Asset ON dbo.TBL_INCIDENCE_CALL.AssetID = dbo.tbl_Asset.AssetID LEFT OUTER JOIN
                      dbo.View_Client_EXT ON dbo.TBL_INCIDENCE_CALL.ClientID = dbo.View_Client_EXT.ClientID LEFT OUTER JOIN
                      dbo.View_Property_EXT ON dbo.TBL_INCIDENCE_CALL.PropertyID = dbo.View_Property_EXT.PropertyID LEFT OUTER JOIN
                      dbo.View_Unit_EXT ON dbo.TBL_INCIDENCE_CALL.UnitID = dbo.View_Unit_EXT.UnitID
WHERE     (dbo.Config_Status.Affects = 'call')




;
